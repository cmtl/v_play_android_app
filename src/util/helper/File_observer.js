import { Log_MACHINE_debug, Log_MACHINE_error,Log_MACHINE_info,Log_MACHINE_warn } from "./Log_shortcut";
import * as FileSystem from 'expo-file-system';

/**
 * Author: Ken Lung, Time: 03/04/2024
 * observe_DocumentDirectory and observe_CacheDirectory reveal the files in the respective directory 
 * And log the content into MACHINE LOG
 * 
 * Used in App.js's admin function for debugging 
 */

export async function observe_DocumentDirectory() {
    const dirUri = FileSystem.documentDirectory; 

    const dirInfo = await FileSystem.readDirectoryAsync(dirUri);
    for (const file of dirInfo) {
        Log_MACHINE_info(`The documentDirectory has file ${file}`) //Log the name of the file found in the document directory 
    }
}
export async function observe_CacheDirectory() {
    const dirUri = FileSystem.cacheDirectory;

    const dirInfo = await FileSystem.readDirectoryAsync(dirUri);
    for (const file of dirInfo) {
        Log_MACHINE_info(`The cacheDirectory has file ${file}`) //Log the name of the file found in the cache directory 
    }
}