/**
 * Author: Ken Lung, Time 03/04/2024
 * A helper function for checking if the parentArray contains all elements in subsetArray 
 * Used in Media_player_new.js for checking if the received playlist has all the media downloaded 
 */

export function checkSubset(parentArray, subsetArray) {
    return subsetArray.every((el) => { //Return true if all element in the subArray are in the parentArray
        return parentArray.includes(el) //Check if the parentArray has an element from subsetArray, return true when it does 
    })
}