import { Dimensions } from 'react-native';
/**
 * Author: Ken Lung, Time: 03/04/2024
 * Helper functions for getting the screen/window's width and height for App.js 
 * To determine whether the device is on portrait or landscape
 */
export const calcWidth = () => Dimensions.get('window').width;
export const calcHeight = () => Dimensions.get('window').height;