/**
 * 
 * Author: Ken Lung, Time: 03/04/2024
 * A helper function that checks if the resposne contain any empty playlist jobs
 * Remove the empty playlist job while looping from the end of the array 
 * 
 * Because it is backward counting, removing elements in the array will not change the numbering
 * And affect the for loop
 * 
 * Uses if statements to cater the difference in type of playlist 
 */


export async function Tidyup_Emptylist(response, type) {
    if (type == "playlist") {
        for (let list_num = response.length - 1; list_num >= 0; list_num--) {
            if (response[list_num].newPlaylistMedia.content.length == 0) {
                response.splice(list_num, 1);
            }
        }
        
    }
    else if (type == "CC") {
        for (let list_num = response.length - 1; list_num >= 0; list_num--) {
            if (response[list_num].newCCMedia.content.length == 0) {
                response.splice(list_num, 1);
            }
        }
        
    }
    return response;
}
