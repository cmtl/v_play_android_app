import * as FileSystem from 'expo-file-system';
import { Log_MACHINE_info, Log_MACHINE_warn, Log_MACHINE_error, Log_MACHINE_debug } from './Log_shortcut';

export async function Delete_FileDirectory() {

    try {
        const files =  await FileSystem.readDirectoryAsync(FileSystem.documentDirectory);
        const deletePromises = files.map(file => {
            const filePath = `${FileSystem.documentDirectory}/${file}`;
            return FileSystem.deleteAsync(filePath);
        });
        await Promise.all(deletePromises);
        Log_MACHINE_warn(`All the file in the cacheDirectory have been removed!`) //Log the name of the file found in the document directory 
    } catch (error) {
        Log_MACHINE_error('Error deleting files in documentDirectory!', error);
    }

}

export async function Delete_CacheDirectory() {
    try {
        const files =  await FileSystem.readDirectoryAsync(FileSystem.cacheDirectory);
        const deletePromises = files.map(file => {
            const filePath = `${FileSystem.cacheDirectory}/${file}`;
            return FileSystem.deleteAsync(filePath);
        });
        await Promise.all(deletePromises);
        Log_MACHINE_warn(`All the file in the cacheDirectory have been removed!`) //Log the name of the file found in the document directory 
    } catch (error) {
        Log_MACHINE_error('Error deleting files in cacheDirectory!', error);
    }
}

