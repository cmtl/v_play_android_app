/**
 * 
 * Author: Ken Lung, Time: 03/04/2024
 * A helper function that halt the running process of the functions that use it
 * The time for halting is measured in ms 
 * 
 * 1 second = 1000 ms 
 */

export function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}