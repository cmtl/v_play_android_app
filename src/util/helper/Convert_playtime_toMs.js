/**
 * Author: Ken Lung, Time 03/04/2024
 * A helper function for converting the starting and ending of a video in (HH:MM:SS) into entirely ms 
 * Have several conditions to check to ensure smooth running of the program 
 *
 * Used in Media_player_new.js when a video is loaded
 */

export function Convert_playtime_toMs(startTime_inStr, endTime_inStr, original_duration) {


    let startTime_inMs = parseInt(startTime_inStr.split('.')[0]) * 1000 * 60 * 60 + parseInt(startTime_inStr.split('.')[1]) * 60 * 1000 + parseInt(startTime_inStr.split('.')[2] * 1000)
    let endTime_inMs = parseInt(endTime_inStr.split('.')[0]) * 1000 * 60 * 60 + parseInt(endTime_inStr.split('.')[1]) * 60 * 1000 + parseInt(endTime_inStr.split('.')[2] * 1000)
  


    if (Math.abs(endTime_inMs - startTime_inMs) > original_duration) {
        //console.log('entering first nested if')
        if (endTime_inMs > original_duration) { 
            //console.log('entering first nested if ge first if ')

            endTime_inMs = original_duration //If the ending time point is longer than the video's duration, set it to be as same as the video's duration
        }
        if (startTime_inMs > original_duration) {
            //console.log('entering first nested if ge second if ')

            startTime_inMs = 0 //If the ending time point is longer than the video's duration, set it to be 0 (the very beginning of the video)
        }
    }
    else {
        if (startTime_inMs == endTime_inMs) {
            endTime_inMs = original_duration
        }
        if (startTime_inMs > endTime_inMs) {
            startTime_inMs = 0  //If the starting time is longer than the ending time, set the starting time to be be 0 (the very beginning of the video) 
            //and keep the original ending time
    
        } 
        if (endTime_inMs < startTime_inMs) {
            endTime_inMs = original_duration //If the ending time point is longer than the video's duration, set it to be as same as the video's duration
        }
    }
    return ([startTime_inMs, endTime_inMs])
}