import { API_LOG, MEDIA_LOG, T_API_LOG, C_MEDIA_LOG } from "../../background_jobs/log/Logger";
import { Log_API_debug, Log_API_info, Log_API_warn, Log_API_error } from "./Log_shortcut";
import { Log_MEDIA_debug, Log_MEDIA_info, Log_MEDIA_warn, Log_MEDIA_error } from "./Log_shortcut";
import { Log_ALERT_debug, Log_ALERT_warn, Log_ALERT_info, Log_ALERT_error } from "./Log_shortcut";
import { Log_ROUTINE_debug, Log_ROUTINE_info, Log_ROUTINE_warn, Log_ROUTINE_error } from "./Log_shortcut";
import Constants from 'expo-constants'


import { showToast } from "../../background_jobs/notification/Toast_notification";
import { getFrom_LocalData, getFrom_LocalData_LessLogging } from "../stats/info/Secured_Machine_Info";
import { get_SignalStrength } from "../stats/Check_Connection";
import { Saving_Login_Info } from "../stats/info/Info_Bundle";

/**
 * Author: Ken Lung, Time: 03/04/2024
 * Async functions for handling the error response in different component
 * Overall pattern: 
 * If the response status starts with 4XX, report it as a client-side error and log it with the responsible logger 
 * If the response status starts with 5XX, report it as a server-side error and further check what number it is
 * Logging the error with the responsible logger
 * Return undefine at the end
 */

/**
 * Author: Ken Lung, Time: 03/04/2024
 * Login_errorResponse will include a function call showToast to have a little pop-up on screen that fades away after a while 
 */

const domain = "https://vplay.365vending.net"

export async function Login_errorResponse(response) {
    if (response.status >= 400 && response.status <= 499) {
        let json = await response.json();
        Log_API_error(`Client error with ${response.status} status in Login_errorResponse! (Content: [${json.errCode}] ${json.errName}: ${json.message}, Supporting details: ${JSON.stringify(json.errDetail)})`)

        showToast(`Error with ${response.status} status! (Error code: [${json.errCode}])`)

        return undefined;
    }
    else if (response.status >= 500 && response.status <= 599) {
        switch (response.status) { //Switch statement to log message according to specific status code 
            case 500:
                Log_API_error(`Server error with ${response.status} status in Login_errorResponse! (Supporting details: The server is currently facing some internal difficulties and not available)`)
                showToast(`Error with ${response.status} status! (Error code: [${json.errCode}])`)

                break;
            case 501:
                Log_API_error(`Server error with ${response.status} status in Login_errorResponse! (Supporting details: The server is currently have not implemented the response for the request and not available)`)
                showToast(`Error with ${response.status} status! (Error code: [${json.errCode}])`)

                break;
            case 502:
                Log_API_error(`Server error with ${response.status} status in Login_errorResponse! (Supporting details: The server get an invalid response)`)
                showToast(`Error with ${response.status} status! (Error code: [${json.errCode}])`)

                break;
            case 503:
                Log_API_error(`Server error with ${response.status} status in Login_errorResponse! (Supporting details: The server is not ready to handle the request because server is down for maintenance or is overloaded)`)
                showToast(`Error with ${response.status} status! (Error code: [${json.errCode}])`)

                break;
            case 504:
                Log_API_error(`Server error with ${response.status} status in Login_errorResponse! (Supporting details: The server cannot give a response in time)`)
                showToast(`Error with ${response.status} status! (Error code: [${json.errCode}])`)

                break;

            default:
                Log_API_error(`Server error with ${response.status} status in Login_errorResponse! (Supporting details: The server is currently facing some unknown difficulties and not available)`)
                showToast(`Error with ${response.status} status! (Error code: [${json.errCode}])`)

                break;

        }

        return undefined;
    }
}

export async function API_errorResponse(response) {


    if (response.status >= 400 && response.status <= 499) {
        let json = await response.json();
        Log_API_error(`Client error with ${response.status} status in API_errorResponse! (Content: [${json.errCode}] ${json.errName}: ${json.message}, Supporting details: ${JSON.stringify(json.errDetail)})`)
        if (json.errCode == 401017) {

            let activationKey = await getFrom_LocalData_LessLogging("activationKey");
            let androidID = await getFrom_LocalData("androidID");
            let signal_strength = await get_SignalStrength();
            let app_ver = Constants.expoConfig.version;

            await patch_Login_for_Reget_AccessToken(activationKey, androidID, app_ver, signal_strength);


        }


        return undefined;
    }
    else if (response.status >= 500 && response.status <= 599) {

        switch (response.status) { //Switch statement to log message according to specific status code 
            case 500:
                Log_API_error(`Server error with ${response.status} status in API_errorResponse! (Supporting details: The server is currently facing some internal difficulties and not available)`)
                break;
            case 501:
                Log_API_error(`Server error with ${response.status} status in API_errorResponse! (Supporting details: The server is currently have not implemented the response for the request and not available)`)

                break;
            case 502:

                Log_API_error(`Server error with ${response.status} status in API_errorResponse! (Supporting details: The server get an invalid response)`)
                break;
            case 503:
                Log_API_error(`Server error with ${response.status} status in API_errorResponse! (Supporting details: The server is not ready to handle the request)`)
                break;
            case 504:
                Log_API_error(`Server error with ${response.status} status in API_errorResponse! (Supporting details: The server cannot get a response in time)`)

                break;
            default:
                Log_API_error(`Server error with ${response.status} status in API_errorResponse! (Supporting details: The server is currently facing some difficulties and not available)`)
                break;

        }
        return undefined;
    }
}

export async function MEDIA_errorResponse(response) {
    //console.log(response)
    if (response.status >= 400 && response.status <= 499) {
        let json = await response.json();
        Log_MEDIA_error(`Client error with ${response.status} status in MEDIA_errorResponse! (Content: [${json.errCode}] ${json.errName}: ${json.message}, Supporting details: ${JSON.stringify(json.errDetail)})`)
        if (json.errCode == 401017) {

            let activationKey = await getFrom_LocalData_LessLogging("activationKey");
            let androidID = await getFrom_LocalData("androidID");
            let signal_strength = await get_SignalStrength();
            let app_ver = Constants.expoConfig.version;

            await patch_Login_for_Reget_AccessToken(activationKey, androidID, app_ver, signal_strength);

        }
        return undefined;
    }
    else if (response.status >= 500 && response.status <= 599) {
        switch (response.status) { //Switch statement to log message according to specific status code 
            case 500:
                Log_MEDIA_error(`Server error with ${response.status} status in MEDIA_errorResponse! (Supporting details: The server is currently facing some internal difficulties and not available)`)
                break;
            case 501:
                Log_MEDIA_error(`Server error with ${response.status} status in MEDIA_errorResponse! (Supporting details: The server is currently have not implemented the response for the request and not available)`)

                break;
            case 502:
                Log_MEDIA_error(`Server error with ${response.status} status in MEDIA_errorResponse! (Supporting details: The server get an invalid response)`)
                break;
            case 503:
                Log_MEDIA_error(`Server error with ${response.status} status in MEDIA_errorResponse! (Supporting details: The server is not ready to handle the request)`)
                break;
            case 504:
                Log_MEDIA_error(`Server error with ${response.status} status in MEDIA_errorResponse! (Supporting details: The server cannot get a response in time)`)

                break;
            default:
                Log_MEDIA_error(`Server error with ${response.status} status in MEDIA_errorResponse! (Supporting details: The server is currently facing some difficulties and not available)`)
                break;

        }
        return undefined;
    }
}

export async function MEDIA_errorResponse_forIncompleteButPlay(response, incompleteButPlay) {
    //console.log(response)
    if (response.status >= 400 && response.status <= 499) {
        let json = await response.json();
        Log_MEDIA_error(`Client error with ${response.status} status in MEDIA_errorResponse_forIncompleteButPlay! (Content: [${json.errCode}] ${json.errName}: ${json.message}, Supporting details: ${JSON.stringify(json.errDetail)})`)
        if (json.errCode == 401017) {

            let activationKey = await getFrom_LocalData_LessLogging("activationKey");
            let androidID = await getFrom_LocalData("androidID");
            let signal_strength = await get_SignalStrength();
            let app_ver = Constants.expoConfig.version;

            await patch_Login_for_Reget_AccessToken(activationKey, androidID, app_ver, signal_strength);

        }
        if (incompleteButPlay == true) {
            return null
        }
        else {
            return false
        }

    }
    else if (response.status >= 500 && response.status <= 599) {
        switch (response.status) { //Switch statement to log message according to specific status code 
            case 500:
                Log_MEDIA_error(`Server error with ${response.status} status in MEDIA_errorResponse_forIncompleteButPlay! (Supporting details: The server is currently facing some internal difficulties and not available)`)
                break;
            case 501:
                Log_MEDIA_error(`Server error with ${response.status} status in MEDIA_errorResponse_forIncompleteButPlay! (Supporting details: The server is currently have not implemented the response for the request and not available)`)

                break;
            case 502:
                Log_MEDIA_error(`Server error with ${response.status} status in MEDIA_errorResponse_forIncompleteButPlay! (Supporting details: The server get an invalid response)`)
                break;
            case 503:
                Log_MEDIA_error(`Server error with ${response.status} status in MEDIA_errorResponse_forIncompleteButPlay! (Supporting details: The server is not ready to handle the request)`)
                break;
            case 504:
                Log_MEDIA_error(`Server error with ${response.status} status in MEDIA_errorResponse_forIncompleteButPlay! (Supporting details: The server cannot get a response in time)`)

                break;
            default:
                Log_MEDIA_error(`Server error with ${response.status} status! (Supporting details: The server is currently facing some difficulties and not available)`)
                break;

        }
        return undefined;
    }
}

export async function ALERT_errorResponse(response) {
    //console.log(response)
    if (response.status >= 400 && response.status <= 499) {
        let json = await response.json();
        Log_ALERT_error(`Client error with ${response.status} status in ALERT_errorResponse! (Content: [${json.errCode}] ${json.errName}: ${json.message}, Supporting details: ${JSON.stringify(json.errDetail)})`)
        if (json.errCode == 401017) {

            let activationKey = await getFrom_LocalData_LessLogging("activationKey");
            let androidID = await getFrom_LocalData("androidID");
            let signal_strength = await get_SignalStrength();
            let app_ver = Constants.expoConfig.version;

            await patch_Login_for_Reget_AccessToken(activationKey, androidID, app_ver, signal_strength);

        }
        return undefined;
    }
    else if (response.status >= 500 && response.status <= 599) {
        switch (response.status) { //Switch statement to log message according to specific status code 
            case 500:
                Log_ALERT_error(`Server error with ${response.status} status in ALERT_errorResponse! (Supporting details: The server is currently facing some internal difficulties and not available)`)
                break;
            case 501:
                Log_ALERT_error(`Server error with ${response.status} status in ALERT_errorResponse! (Supporting details: The server is currently have not implemented the response for the request and not available)`)

                break;
            case 502:
                Log_ALERT_error(`Server error with ${response.status} status in ALERT_errorResponse! (Supporting details: The server get an invalid response)`)
                break;
            case 503:
                Log_ALERT_error(`Server error with ${response.status} status in ALERT_errorResponse! (Supporting details: The server is not ready to handle the request)`)
                break;
            case 504:
                Log_ALERT_error(`Server error with ${response.status} status in ALERT_errorResponse! (Supporting details: The server cannot get a response in time)`)

                break;
            default:
                Log_ALERT_error(`Server error with ${response.status} status in ALERT_errorResponse! (Supporting details: The server is currently facing some difficulties and not available)`)
                break;

        }
        return undefined;
    }
}

export async function ROUTINE_errorResponse(response) {
    //console.log(response)
    if (response.status >= 400 && response.status <= 499) {
        let json = await response.json();
        Log_ROUTINE_error(`Client error with ${response.status} status ROUTINE_errorResponse! (Content: [${json.errCode}] ${json.errName}: ${json.message}, Supporting details: ${JSON.stringify(json.errDetail)})`)
        if (json.errCode == 401017) {

            let activationKey = await getFrom_LocalData_LessLogging("activationKey");
            let androidID = await getFrom_LocalData("androidID");
            let signal_strength = await get_SignalStrength();
            let app_ver = Constants.expoConfig.version;

            await patch_Login_for_Reget_AccessToken(activationKey, androidID, app_ver, signal_strength);

        }
        return undefined;
    }
    else if (response.status >= 500 && response.status <= 599) {
        switch (response.status) { //Switch statement to log message according to specific status code 
            case 500:
                Log_ROUTINE_error(`Server error with ${response.status} status in ROUTINE_errorResponse! (Supporting details: The server is currently facing some internal difficulties and not available)`)
                break;
            case 501:
                Log_ROUTINE_error(`Server error with ${response.status} status in ROUTINE_errorResponse! (Supporting details: The server is currently have not implemented the response for the request and not available)`)

                break;
            case 502:
                Log_ROUTINE_error(`Server error with ${response.status} status in ROUTINE_errorResponse! (Supporting details: The server get an invalid response)`)
                break;
            case 503:
                Log_ROUTINE_error(`Server error with ${response.status} status in ROUTINE_errorResponse! (Supporting details: The server is not ready to handle the request)`)
                break;
            case 504:
                Log_ROUTINE_error(`Server error with ${response.status} status in ROUTINE_errorResponse! (Supporting details: The server cannot get a response in time)`)

                break;
            default:
                Log_ROUTINE_error(`Server error with ${response.status} status in ROUTINE_errorResponse! (Supporting details: The server is currently facing some difficulties and not available)`)
                break;

        }


        return undefined;
    }
}

/**
 * Author: Ken Lung, Time: 03/04/2024
 * In MEDIA_Failed_errorResponse, if json.errDetail is an array (Containing more than one detailed error thrown by the backend), 
 * Log all of them until all of them are read
 */

export async function MEDIA_Failed_errorResponse(response) {
    if (response.status >= 400 && response.status <= 499) {
        let json = await response.json();
       
        Log_MEDIA_error(`Client error with ${response.status} status in MEDIA_Failed_errorResponse! (Content: [${json.errCode}] ${json.errName}: ${json.message}, Supporting details: ${JSON.stringify(json.errDetail)})`)
        if (json.errDetail != undefined && Array.isArray(json.errDetail) == true) { //Check if the errDetail key in the JSON is an array or not 
            for (let error_num = 0; error_num != json.errDetail.length; error_num++) {
                Log_MEDIA_error(`Kind of error: ${json.errDetail[error_num].kind}, ${json.errDetail[error_num].message}`); //Keep logging each detail of the error 
            }
        }
        if (json.errCode == 401017) {

            let activationKey = await getFrom_LocalData_LessLogging("activationKey");
            let androidID = await getFrom_LocalData("androidID");
            let signal_strength = await get_SignalStrength();
            let app_ver = Constants.expoConfig.version;

            await patch_Login_for_Reget_AccessToken(activationKey, androidID, app_ver, signal_strength);

        }
        return undefined;

    }

    else if (response.status >= 500 && response.status <= 599) {
        switch (response.status) { //Switch statement to log message according to specific status code 
            case 500:
                Log_MEDIA_error(`Server error with ${response.status} status in MEDIA_Failed_errorResponse! (Supporting details: The server is currently facing some internal difficulties and not available)`)
                break;
            case 501:
                Log_MEDIA_error(`Server error with ${response.status} status in MEDIA_Failed_errorResponse! (Supporting details: The server is currently have not implemented the response for the request and not available)`)

                break;
            case 502:
                Log_MEDIA_error(`Server error with ${response.status} status in MEDIA_Failed_errorResponse! (Supporting details: The server get an invalid response)`)
                break;
            case 503:
                Log_MEDIA_error(`Server error with ${response.status} status in MEDIA_Failed_errorResponse! (Supporting details: The server is not ready to handle the request)`)
                break;
            case 504:
                Log_MEDIA_error(`Server error with ${response.status} status in MEDIA_Failed_errorResponse! (Supporting details: The server cannot get a response in time)`)

                break;
            default:
                Log_MEDIA_error(`Server error with ${response.status} status in MEDIA_Failed_errorResponse! (Supporting details: The server is currently facing some difficulties and not available)`)
                break;

        }


        return undefined;
    }
}

/**
 * Author: Ken Lung, Time: 03/04/2024
 * In ROUTINE_Failed_errorResponse, if json.errDetail is an array (Containing more than one detailed error thrown by the backend), 
 * Log all of them until all of them are read
 */

export async function ROUTINE_Failed_errorResponse(response) {
    if (response.status >= 400 && response.status <= 499) {
        let json = await response.json();
       
        Log_ROUTINE_error(`Client error with ${response.status} status in ROUTINE_Failed_errorResponse! (Content: [${json.errCode}] ${json.errName}: ${json.message}, Supporting details: ${JSON.stringify(json.errDetail)})`)
        if (json.errDetail != undefined && Array.isArray(json.errDetail) == true) { //Check if the errDetail key in the JSON is an array or not 
            for (let error_num = 0; error_num != json.errDetail.length; error_num++) {
                Log_ROUTINE_error(`Kind of error: ${json.errDetail[error_num].kind}, ${json.errDetail[error_num].message}`); //Keep logging each detail of the error 
            }
        }
        if (json.errCode == 401017) {

            let activationKey = await getFrom_LocalData_LessLogging("activationKey");
            let androidID = await getFrom_LocalData("androidID");
            let signal_strength = await get_SignalStrength();
            let app_ver = Constants.expoConfig.version;

            await patch_Login_for_Reget_AccessToken(activationKey, androidID, app_ver, signal_strength);


        }
        return undefined;

    }

    else if (response.status >= 500 && response.status <= 599) {
        switch (response.status) { //Switch statement to log message according to specific status code 
            case 500:
                Log_ROUTINE_error(`Server error with ${response.status} status in ROUTINE_Failed_errorResponse! (Supporting details: The server is currently facing some internal difficulties and not available)`)
                break;
            case 501:
                Log_ROUTINE_error(`Server error with ${response.status} status in ROUTINE_Failed_errorResponse! (Supporting details: The server is currently have not implemented the response for the request and not available)`)

                break;
            case 502:
                Log_ROUTINE_error(`Server error with ${response.status} status in ROUTINE_Failed_errorResponse! (Supporting details: The server get an invalid response)`)
                break;
            case 503:
                Log_ROUTINE_error(`Server error with ${response.status} status in ROUTINE_Failed_errorResponse! (Supporting details: The server is not ready to handle the request)`)
                break;
            case 504:
                Log_ROUTINE_error(`Server error with ${response.status} status in ROUTINE_Failed_errorResponse! (Supporting details: The server cannot get a response in time)`)

                break;
            default:
                Log_ROUTINE_error(`Server error with ${response.status} status! (Supporting details: The server is currently facing some difficulties and not available)`)
                break;

        }


        return undefined;
    }
}

export async function patch_Login_for_Reget_AccessToken(key_input, id_input, verison_input, signal_input) {
    async function call() {
        try {
            const data = {
                activationKey: key_input,
                deviceId: id_input,
                appVersion: verison_input,
                signalStrength: signal_input
            };

            const resource_location = 'authen/pm-login';

            const response = await fetch(`${domain}/databaseAPI/${resource_location}`, {
                method: 'PATCH',
                headers: {
                    'Origin': 'androidApp',
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify(data),

            });

            if (response.status > 299) {
                await Login_errorResponse(response);
            } else {
                Log_API_info(`Running patch_Login_for_Reget_AccessToken without problem! (status: ${response.status})`);
                const json = await response.json();

                if (json != undefined) {
                    if (json != false) {
                        const pmID = json.pmId;
                        const groupID = json.groupId;
                        const accessToken = json.accessToken;
                        await Saving_Login_Info(pmID, groupID, accessToken);
                        Log_API_info(`Successful Heartbeat!`);
                    } 
                    return undefined;

                } else {
                    Log_API_error(`Failed Heartbeat!`);
                    return false;
                }
            }
        } catch (err) {
            Log_API_error(`Request for patch_Login_for_Reget_AccessToken encountered some problems due to ${err}!`);
           
        }
    }
    return await call()

}