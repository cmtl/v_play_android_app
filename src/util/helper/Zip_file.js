import JSZip from 'jszip';
import * as FileSystem from 'expo-file-system'
import * as Sharing from 'expo-sharing'
import { Log_MACHINE_debug, Log_MACHINE_info, Log_MACHINE_warn, Log_MACHINE_error } from './Log_shortcut';

/**
 * Author: Ken Lung, Time: 03/04/2024
 * Zip_file is an async helper function for creating a new ZIP file based on the fileList (a list of file to be zipped)
 */
export async function Zip_file(fileList, zipName) {
    let fileData;

    let zipper = new JSZip(); //Create a new instance of zipper 

    for (let file_num = 0; file_num != fileList.length; file_num++) {

        fileData = await read_file_for_zipping(fileList[file_num]) //Process the file using read_file_for_zipping() and include it into the zipper 
        zipper.file(fileList[file_num], fileData, { base64: true })
    }
    let zipOptions = { type: 'base64' };

    
    let zipData = await zipper.generateAsync(zipOptions); //Generate the ZIP file's data using generateAsync

    
    let zipUri = FileSystem.cacheDirectory + zipName + '.zip'; //Create the file name for the ZIP file 
    let options = { encoding: FileSystem.EncodingType.Base64 };

    
    await FileSystem.writeAsStringAsync(zipUri, zipData, options); //Write a completed zip file with file name and file data with writeAsStringAsync
    Log_MACHINE_info(`Zipped the required file successfully!`)
}

/**
 * Author: Ken Lung, Time: 03/04/2024
 * read_file_for_zipping is an async helper function for reading the content in the file to be zipped 
 */

async function read_file_for_zipping(fileName) {
    let fileUri = FileSystem.documentDirectory + fileName;
    let options = { encoding: FileSystem.EncodingType.Base64 }
    let fileData = await FileSystem.readAsStringAsync(fileUri, options)
    return fileData
}


/**
 * Author: Ken Lung, Time: 03/04/2024
 * find_Zippedfile is an async helper function for finding the created ZIP file based on zipName 
 * If the file exists, return its URI
 * If not, return null 
 */

export async function find_Zippedfile(zipName) {
    const fileUri = FileSystem.cacheDirectory + zipName + '.zip'

    const fileInfo = await FileSystem.getInfoAsync(fileUri);

    if (fileInfo.exists) {
        Log_MACHINE_info(`Found the required zip package! Returning fileUri: ${fileUri}!`) 
        return fileUri //Return the URI of the found ZIP file 
    }
    else {
        Log_MACHINE_error(`Cannot found the required zip package! Returning null!`)
        return null //Return null because the function could not find the required file
    

    }
}
