import { ALERT_LOG, API_LOG, MACHINE_LOG, MEDIA_LOG, NETWORK_LOG, ROUTINE_LOG, RSS_LOG, U_ALERT_LOG, U_API_LOG, U_MACHINE_LOG, U_MEDIA_LOG, U_NETWORK_LOG, U_ROUTINE_LOG, U_RSS_LOG } from '../../background_jobs/log/Logger'

import { Testing_TESTING_LOG } from '../../background_jobs/log/Logger'

/**
 * Author: Ken Lung, Time: 03/04/2024
 * A series of helper functions that group the same log messages to different yet related loggers 
 * Loggers start with "U_" stands for "Unique", they are independent logger 
 * 
 * Loggers that do not start with "U_" are logger headers of the App's debugging logger 
 * It does not log into any file and will only have the content shown in the console 
 */

export function Log_MEDIA_info(string) {
    
    MEDIA_LOG.info(string)
    
    

    U_MEDIA_LOG.info(string)
    
}

export function Log_MEDIA_warn(string) {
    
    MEDIA_LOG.warn(string)
    
    

    U_MEDIA_LOG.warn(string)
    

}
export function Log_MEDIA_error(string) {
    
    MEDIA_LOG.error(string)
    
    

    U_MEDIA_LOG.error(string)
    

}
export function Log_MEDIA_debug(string) {
    
    MEDIA_LOG.debug(string)
    
    

    U_MEDIA_LOG.debug(string)
    

}

export function Log_NETWORK_info(string) {
    
    NETWORK_LOG.info(string)
    
    

    U_NETWORK_LOG.info(string)
    
}

export function Log_NETWORK_warn(string) {
    
    NETWORK_LOG.warn(string)
    
    

    U_NETWORK_LOG.warn(string)
    

}
export function Log_NETWORK_error(string) {
    
    NETWORK_LOG.error(string)
    
    

    U_NETWORK_LOG.error(string)
    

}
export function Log_NETWORK_debug(string) {
    
    NETWORK_LOG.debug(string)
    
    

    U_NETWORK_LOG.debug(string)
    

}

export function Log_RSS_info(string) {
    
    RSS_LOG.info(string)
    
    

    U_RSS_LOG.info(string)
    
}

export function Log_RSS_warn(string) {
    
    RSS_LOG.warn(string)
    
    

    U_RSS_LOG.warn(string)
    

}
export function Log_RSS_error(string) {
    
    RSS_LOG.error(string)
    
    

    U_RSS_LOG.error(string)
    

}
export function Log_RSS_debug(string) {
    
    RSS_LOG.debug(string)
    
    

    U_RSS_LOG.debug(string)
    

}

export function Log_ROUTINE_info(string) {
    
    ROUTINE_LOG.info(string)
    
    

    U_ROUTINE_LOG.info(string)
    
}

export function Log_ROUTINE_warn(string) {
    
    ROUTINE_LOG.warn(string)
    
    

    U_ROUTINE_LOG.warn(string)
    

}
export function Log_ROUTINE_error(string) {
    
    ROUTINE_LOG.error(string)
    
    

    U_ROUTINE_LOG.error(string)
    

}
export function Log_ROUTINE_debug(string) {
    
    ROUTINE_LOG.debug(string)
    
    

    U_ROUTINE_LOG.debug(string)
    

}

export function Log_MACHINE_info(string) {
    
    MACHINE_LOG.info(string)
    
    

    U_MACHINE_LOG.info(string)
    
}

export function Log_MACHINE_warn(string) {
    
    MACHINE_LOG.warn(string)
    
    

    U_MACHINE_LOG.warn(string)
    

}
export function Log_MACHINE_error(string) {
    
    MACHINE_LOG.error(string)
    
    

    U_MACHINE_LOG.error(string)
    

}
export function Log_MACHINE_debug(string) {
    
    MACHINE_LOG.debug(string)
    
    

    U_MACHINE_LOG.debug(string)
    

}

export function Log_API_info(string) {
    
    API_LOG.info(string)
    
    

    U_API_LOG.info(string)
    
}

export function Log_API_warn(string) {
    
    API_LOG.warn(string)
    
    

    U_API_LOG.warn(string)
    

}
export function Log_API_error(string) {
    
    API_LOG.error(string)
    
    

    U_API_LOG.error(string)
    

}
export function Log_API_debug(string) {
    
    API_LOG.debug(string)
    
    

    U_API_LOG.debug(string)
    

}

export function Log_ALERT_info(string) {
    
    ALERT_LOG.info(string)
    
    

    U_ALERT_LOG.info(string)
    
}

export function Log_ALERT_warn(string) {
    
    ALERT_LOG.warn(string)
    
    

    U_ALERT_LOG.warn(string)
    

}
export function Log_ALERT_error(string) {
    
    ALERT_LOG.error(string)
    
    

    U_ALERT_LOG.error(string)
    

}
export function Log_ALERT_debug(string) {
    
    ALERT_LOG.debug(string)
    
    

    U_ALERT_LOG.debug(string)
    

}
/* export function LOG_FK_DA_FS(string) {
    
    for (let i = 0; i!= 100; i++) {
        Testing_TESTING_LOG.debug(string)
    }
}*/

export function Log_Testing_debug(string) {
    
    Testing_TESTING_LOG.debug(string)
}

export function Log_Testing_info(string) {
    
    Testing_TESTING_LOG.info(string)
}

export function Log_Testing_error(string) {
    
    Testing_TESTING_LOG.error(string)
}

export function Log_Testing_warn(string) {
    
    Testing_TESTING_LOG.warn(string)
}
