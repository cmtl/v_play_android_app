import * as FileSystem from 'expo-file-system';
import { Log_MACHINE_info, Log_MACHINE_debug, Log_MACHINE_warn, Log_MACHINE_error } from './Log_shortcut';

/**
 * Author: Ken Lung, Time 03/04/2024
 * A helper function for checking if the required log exists or not before writing
 * If true, get previously written content and append the new log messages to the of content 
 * And write the entire content back to the log file for file logging
 * If not, just write the log message to the log file, creating a new log file after writting
 * 
 * Used in the file transport functions under the "log" folder (Pathway: src\background_jobs\log)
 */
export async function Checkif_LogExist(logFilePath, log_accumlator) {
    try{
        let log_Exist = await FileSystem.getInfoAsync(logFilePath);
        let newMsg = log_accumlator.join('\n')
        let newFile;
        
        if (!log_Exist.exists) {
            newFile = newMsg; //The content of the new log file will only be the messages stored in the buffer 
        } 
        else {
            const prevFile = await FileSystem.readAsStringAsync(logFilePath); //The content of the log file will be the messages stored in the buffer and the existing content
            
            newFile = prevFile + '\n' + newMsg; // Append new message with a line break
        }
       
        await FileSystem.writeAsStringAsync(logFilePath, newFile, {
            encoding: FileSystem.EncodingType.UTF8, 
        }); //There is not appending feature in Expo's FS. So we need to combine the old content, if any, with the new content and overwrite the log file entirely
        
    }
    catch (error) {
        Log_MACHINE_error(`Something unexpected happened in Checkif_LogExist due to ${error}`) //Show error happen during writing on the log file 
    }
}
