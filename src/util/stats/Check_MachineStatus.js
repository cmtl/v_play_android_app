import * as Device from 'expo-device';
import * as FileSystem from 'expo-file-system';

import { Log_MACHINE_debug, Log_MACHINE_error, Log_MACHINE_info, Log_MACHINE_warn } from '../helper/Log_shortcut';

import { post_Abnormal_Alert } from '../../background_jobs/alert/Send_Alert.js';
import { convert_Byte_to_Readable } from './convert_Byte_to_Readable.js';

/**
 * Author: Ken Lung, Time: 03/04/2024
 * getMemoryUsage is a helper function to check the memory assigned
 * It will get the maximum memory assigned to the app and the device's total memory 
 * return the proportion of assigned memory to total memory (in decimal)
 * 
 * CAUTION: This function was originally intended for getting the memory USAGE
 * HOWEVER, it seems the data collected is not what it was thought to be 
 * 
 * IMPORTANT!!: Might require modification in the future
 */

export async function getMemoryUsage() {
    let useable_memory = 0;
    let total_memory = 0;
    let memory_usage = 0;
    
    useable_memory = await Device.getMaxMemoryAsync()
    total_memory = Device.totalMemory;
    memory_usage = Math.round(useable_memory / Device.totalMemory * 100)//The proportion of assigned memory to total memory
    //tbd cache check
    Log_MACHINE_debug("Usable Memory (in MB): " + convert_Byte_to_Readable(useable_memory));
    Log_MACHINE_debug("Total Memory (in MB): " + convert_Byte_to_Readable(Device.totalMemory)); 
    Log_MACHINE_debug("Memory Usage (in %): " + memory_usage + "%")
    return memory_usage //Return the proportion of assigned memory to total memory

}

/**
 * Author: Ken Lung, Time: 03/04/2024
 * getMemoryUsage is a helper function to check the free disk left in the DEVICE
 * It will get the free disk storage and the device's total disk storage (in decimal)
 * return the proportion of used  disk storage to total disk storage
 * 
 * CAUTION: This function returns storage usage of the WHOLE device, not just the app (total free disk, total disk capacity)
 * HOWEVER, this issue could be neglected because the intended purpose of the device is to operate this project only, so it is OK
 */

export async function getStorageUsage() {
    let useable_storage = 0;
    let total_storage = 0;
    let storage_usage = 0;
    useable_storage = await FileSystem.getFreeDiskStorageAsync()
    total_storage = await FileSystem.getTotalDiskCapacityAsync()
    storage_usage = Math.round(useable_storage / total_storage * 100) //The proportion of used disk storage to total disk storage
    Log_MACHINE_debug("Usasble Disk Capacity (in MB): " + convert_Byte_to_Readable(useable_storage));
    Log_MACHINE_debug("Total Disk Capacity (in MB): " + convert_Byte_to_Readable(total_storage));
    Log_MACHINE_info("Storage Usage (in %): " + storage_usage + "%")
    return storage_usage //Return the proportion of used  disk storage to total disk storage
}

/**
 * Author: Ken Lung, Time: 03/04/2024
 * WifiReport is a helper function that groups the other two functions together 
 * and passing the result obtained from the two functions to abnormalities_Report_device
 */

export async function DeviceReport() {
    let mem = getMemoryUsage();
    let stor = getStorageUsage();
    abnormalities_Report_device(mem, stor)
}

/**
 * Author: Ken Lung, Time: 03/04/2024
 * Check_Abnormal_Connection is a helper function reports and logs the connection's signal strength 
 * If the occupied memory is higher than or equal to 60, it will further checks it is higher than or equal to 80
 * Report error or warning depending on occupied memory
 * 
 * If the occupied storage is higher than or equal to 60, it will further checks it is higher than or equal to 80
 * Report error or warning depending on occupied storage
 */

function abnormalities_Report_device(mem, stor) {
    if (mem >= 60) {
        if (mem >= 80) {
            Log_MACHINE_error("Memory Usage is more than 80%, please investigate soon! (current usage: " + mem + "%)"); //Serious memory usage, log as error
            post_Abnormal_Alert("Memory Usage is more than 80%, please investigate soon! (current usage: " + mem + "%)", "MEMORY_USAGE") //Report serious memory usage to the backend as an alert

        }
        else {
            Log_MACHINE_warn("Memory Usage is more than 60%. (current usage: " + mem + "%)"); //Severe memory usage, log as warning

            post_Abnormal_Alert("Memory Usage is more than 60%. (current usage: " + mem + "%)", "MEMORY_USAGE") //Report severe memory usage to the backend as an alert

        }
    }
    if (stor >= 60) {
        if (stor >= 80) {
            Log_MACHINE_error("Storage Usage is more than 80%, please remove unwanted file soon! (current usage: " + stor + "%)"); //Serious storage usage, log as error
            post_Abnormal_Alert("Storage Usage is more than 80%, please remove unwanted file soon! (current usage: " + stor + "%)", "STORAGE_USAGE")  //Report serious storage usage to the backend as an alert

        }
        else {
            Log_MACHINE_warn("Storage Usage is more than 60%. (current usage: " + stor + "%)");  //Severe storage usage, log as warning
            post_Abnormal_Alert("Storage Usage is more than 60%. (current usage: " + stor + "%)", "STORAGE_USAGE") //Report severe storage usage to the backend as an alert
        }
    }
}

