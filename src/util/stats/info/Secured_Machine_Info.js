import * as SecureStore from 'expo-secure-store';
import { Log_MACHINE_error, Log_MACHINE_info, Log_MACHINE_warn } from '../../helper/Log_shortcut.js';

/**
 * Author: Ken Lung, Time: 03/04/2024
 * These helper functions are related to the creation, edition, and deletion of value of SecureStore keys
 * Those with the name "_LessLogging" in the end works essentially the same 
 * But they generate less log messages to save memory and avoid leaking private information 


 * storeAs_LocalData_AsyncStorage is a helper function for storing a certain value to a specfic key in SecureStore
 * while creating log messages to state the data has been stored 
 */

export async function storeAs_LocalData(key, value) {
  try {
    await SecureStore.setItemAsync(key, JSON.stringify(value)); //Store the value into the key as stringified JSON
    Log_MACHINE_info("The new SecureStore data of " + key + " is saved!")
    Log_MACHINE_info(`Current SecureStore value of ${key}:\n` + JSON.stringify(value, null, 4)) //Log the value into the key for debugging with prettier formatting


  } catch (error) {
    Log_MACHINE_error(`Error storing SecureStore value into key ${key}: ${error}`); //Show the error occured while storing SecureStore data
  }
};

export async function storeAs_LocalData_LessLogging(key, value) {
  try {
    await SecureStore.setItemAsync(key, JSON.stringify(value)); //Store the value into the key as stringified JSON, omit logging due to lengthy/classified info
    //Log_MACHINE_info("The new SecureStore data of " + key + " is saved!")
    //Log_MACHINE_info(`Current SecureStore value of ${key}:\n` + JSON.stringify(value, null, 4))


  } catch (error) {
    Log_MACHINE_error(`Error storing SecureStore value into key ${key}: `, error); //Show the error occured while storing SecureStore data
  }
};

/* 
 * Author: Ken Lung, Time: 03/04/2024
 * getFrom_LocalData_AsyncStorage is a helper function for getting the SecureStore value stored to a specfic key 
 * while creating log messages to state the data has been taken 
 * If the result is true (valid), return the value 
 * Else if the result is empty, return null
 * Else (undefined), return undefined
 */

export async function getFrom_LocalData(key) {
  try {
    let result = await SecureStore.getItemAsync(key); //Get the value stored in the key as string
    let jsonObj = await JSON.parse(result); //Parse the string back into JSON

    if (result) {
      Log_MACHINE_info("The SecureStore value read from " + key + " is: " + JSON.stringify(jsonObj, null, 4)) //Log the obtained value from the key for debugging with prettier formatting

      return jsonObj; //Return the obtained JSON from the key 
    }
    else if (result === null) {
      Log_MACHINE_warn(key + " is an empty SecureStore key!") //No value was stored in the key
      return null //Return null as the obtained value is nothing 
    }
    else {
      Log_MACHINE_error(key + " is an invalid SecureStore key!") //Special case: the value stored have some problems for some reasons
      return undefined //Return undefined for error report 
    }
  } catch (error) {
    Log_MACHINE_error(`Error retrieving SecureStore value: ${error}`); //Show the error occured while getting SecureStore data
  }
};

export async function getFrom_LocalData_LessLogging(key) {
  try {

    let result = await SecureStore.getItemAsync(key); //Get the value stored in the key as string
    
    let jsonObj = await JSON.parse(result);//Parse the string back into JSON

    if (result) {
      Log_MACHINE_info("The SecureStore value read from " + key + " is: " + JSON.stringify(jsonObj, null, 4))

      return jsonObj;  //Return the obtained JSON from the key, omit logging due to lengthy/classified info
    }
    else if (result === null) {
      Log_MACHINE_warn(key + " is an empty SecureStore key!")//No value was stored in the key
      return null //Return null as the obtained value is nothing 
    }
    else {
      Log_MACHINE_error(key + " is an invalid SecureStore key!") //Special case: the value stored have some problems 
      return undefined
    }
  } catch (error) {
    Log_MACHINE_error('Error retrieving SecureStore value: ', error); //Show the error occured while getting SecureStore data
  }
};

/* 
 * Author: Ken Lung, Time: 03/04/2024
 * deletetFrom_LocalData is a helper function for deleting the SecureStore value stored to a specfic key 
 * while creating log messages to state the data has been deleted
 * If the result is not true (valid) after deleting, then the deleting process is successful  
 */


export async function deletetFrom_LocalData(key) {
  try {
    await SecureStore.deleteItemAsync(key); //Delete the value saved in the key 
    let result = await SecureStore.getItemAsync(key)
    if (!result) { //Confirm if the value in the key was deleted
      Log_MACHINE_info("The stored SecureStore data of " + key + " is deleted!")
    }
    else {
      Log_MACHINE_error("The stored SecureStore data of " + key + " is NOT deleted!") //The value in the key was not deleted for some reasons
    }
  } catch (error) {
    Log_MACHINE_error(`Error deleting SecureStore value: : ${error}`); //Show the error occured while deleting SecureStore data
  }
};

//Do not actually need this but made just in case, essentially as same as deletetFrom_LocalData
export async function deletetFrom_LocalData_LessLogging(key) {
  try {
    await SecureStore.deleteItemAsync(key); //Delete the value saved in the key 
    let result = await SecureStore.getItemAsync(key)
    if (result) { //Confirm if the value in the key was deleted
      Log_MACHINE_error("The stored SecureStore data of " + key + " is NOT deleted!") //The value in the key was not deleted for some reasons

    }
  } catch (error) {
    Log_MACHINE_error('Error deleting SecureStore value: ', error); //Show the error occured while deleting SecureStore data
  }
};

