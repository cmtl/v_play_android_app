import { storeAs_LocalData, getFrom_LocalData, deletetFrom_LocalData } from "./Secured_Machine_Info";
import { storeAs_LocalData_LessLogging } from "./Secured_Machine_Info";
import AsyncStorage from "@react-native-async-storage/async-storage";
import SecureStore from 'expo-secure-store'
import { Log_MACHINE_debug, Log_MACHINE_error, Log_MACHINE_warn, Log_MACHINE_info } from "../../helper/Log_shortcut";

/* 
 * Author: Ken Lung, Time: 03/04/2024
 * keylist is an array that stores all the used key(s) in the project 
 * It is for effectively clearing all the key values 
 * CAUTION: whenever you need to include more key value, it is better to include them here as well 
 * HOWEVER, the keylist is NOT shared with other components, and it is for this component's reference only 
 */

 let keylist = [
  "inverted", "activated", "usingToken", "Routine",
  "playlist_config", "CC_config", "address", "CurMediaReport_Needed",
  "NonRoutine", "display_config", "showRSS", "showTime",

  "showDate", "CompletePlaylist_Reported",
   "CompleteCC_Reported", "playlist_media_ArrList",
  "CC_media_ArrList", "Activation_Dates", 

  "Last_NonPlayOnce_playlist", "Last_Reported_playlist_media",
  "pmID", "groupID", "activationKey", "androidID",
  "accessToken", "horizontal", "CompleteCC_Reported", "CompletePlaylist_Reported"
]

/* 
 * Author: Ken Lung, Time: 03/04/2024
 * Saving_Activated_Info and Saving_Login_Info are helper functions that group all the required storeAs_LocalData functions together 
 * 
 * Used in App.js when the app boots up for the first time 
 */
export async function Saving_Activated_Info(pmid_val, groupid_val, key_val, id_val) {
  await storeAs_LocalData('pmID', pmid_val);
  await storeAs_LocalData('groupID', groupid_val);
  await storeAs_LocalData_LessLogging('activationKey', key_val); //Store with less logging to prevent classified info leaking out
  await storeAs_LocalData_LessLogging('androidID', id_val); //Store with less logging to prevent classified info leaking out
  await storeAs_LocalData('activated', true)
}

export async function Saving_Login_Info(pmid_val, groupid_val, accessToken_val) {
  await storeAs_LocalData('pmID', pmid_val);
  await storeAs_LocalData('groupID', groupid_val);
  await storeAs_LocalData_LessLogging('accessToken', accessToken_val); //Store with less logging to prevent classified info leaking out
  await storeAs_LocalData('usingToken', true)
}

/* 
 * Author: Ken Lung, Time: 03/04/2024
 * Deleting_Info is a helper functions that deletes all the value that are stored in the keys in keylist
 * 
 * Used in App.js's admin function 
 */

export async function Deleting_Info() {
  try {
    for (let item = 0; item != keylist.length; item++) {
      await deletetFrom_LocalData(keylist[item]); //Deleting the value stored in the keylist (Mainly for SecureStore keys)
    }
    Delete_AsyncStorage() //Clear out the keys in AsyncStorage
    Log_MACHINE_warn('All keys cleared!');
  } catch (error) {
    Log_MACHINE_error('Error clearing the keys', error);
  }
}


/* 
 * Author: Ken Lung, Time: 03/04/2024
 * Delete_AsyncStorage is a helper functions that clears the AsyncStorage
 */

function Delete_AsyncStorage() {
  AsyncStorage.clear()
}