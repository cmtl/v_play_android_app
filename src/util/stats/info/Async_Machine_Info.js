import AsyncStorage from '@react-native-async-storage/async-storage';
import { Log_MACHINE_error, Log_MACHINE_info, Log_MACHINE_warn } from '../../helper/Log_shortcut.js';

/**
 * Author: Ken Lung, Time: 03/04/2024
 * These helper functions are related to the creation, edition, and deletion of value of AsyncStorage keys
 * Those with the name "_LessLogging" in the end works essentially the same 
 * But they generate less log messages to save memory and avoid leaking private information 
 */

/* 
 * Author: Ken Lung, Time: 03/04/2024
 * storeAs_LocalData_AsyncStorage is a helper function for storing a certain value to a specfic key in AsyncStorage
 * while creating log messages to state the data has been stored 
 */

export async function storeAs_LocalData_AsyncStorage(key, value) {
  try {
    await AsyncStorage.setItem(key, JSON.stringify(value)) //Store the value into the key as stringified JSON
    Log_MACHINE_info("The new AsyncStorage data of " + key + " is saved!")
    //Log_MACHINE_info(`Current and new AsyncStorage value ${key}:\n` + JSON.stringify(value, null, 4)) //Log the value into the key for debugging with prettier formatting


  } catch (error) {
    Log_MACHINE_error('Error storing AsyncStorage value: ', error); //Show the error occured while storing AsyncStorage data
  }
};

export async function storeAs_LocalData_AsyncStorage_LessLogging(key, value) {
  try {
    await AsyncStorage.setItem(key, JSON.stringify(value)) //Store the value into the key as stringified JSON, omit logging due to lengthy/classified info
    Log_MACHINE_info("The new AsyncStorage data of " + key + " is saved!")
    //Log_MACHINE_info(`Current AsyncStorage value ${key}:\n` + JSON.stringify(value, null, 4))


  } catch (error) {
    Log_MACHINE_error('Error storing AsyncStorage value: ', error); //Show the error occured while storing AsyncStorage data
  }
};

/* 
 * Author: Ken Lung, Time: 03/04/2024
 * getFrom_LocalData_AsyncStorage is a helper function for getting the AsyncStorage value stored to a specfic key 
 * while creating log messages to state the data has been taken 
 * If the result is true (valid), return the value 
 * Else if the result is empty, return null
 * Else (undefined), return undefined
 */

export async function getFrom_LocalData_AsyncStorage(key) {
  try {
    let result = await AsyncStorage.getItem(key); //Get the value stored in the key as string
    let jsonObj = await JSON.parse(result); //Parse the string back into JSON
    
    if (result) {
     // Log_MACHINE_info("The AsyncStorage value read from " + key + " is: " + JSON.stringify(jsonObj, null, 4))

      return jsonObj; //Return the obtained JSON from the key 
    }
    else if (result === null) {
      Log_MACHINE_warn(key + " is an empty AsyncStorage key!") //No value was stored in the key
      return null //Return null as the obtained is empty
    }
    else {
      Log_MACHINE_error(key + " is an invalid AsyncStorage key!") //Special case: the value stored have some problems for some reasons
      return undefined //Return undefined for error report 
    }
  } catch (error) {
    Log_MACHINE_error('Error retrieving SecureStore value: ', error);  //Show the error occured while getting AsyncStorage data
  }
};

export async function getFrom_LocalData_AsyncStorage_LessLogging(key) {
  try {
    let result = await AsyncStorage.getItem(key); //Get the value stored in the key as string
    let jsonObj = await JSON.parse(result); //Parse the string back into JSON
    
    if (result) {
      //Log_MACHINE_info("The AsyncStorage value read from " + key + " is: " + JSON.stringify(jsonObj, null, 4))

      return jsonObj;  //Return the obtained JSON from the key 
    }
    else if (result === null) {
      Log_MACHINE_warn(key + " is an empty AsyncStorage key!") //No value was stored in the key
      return null //Return null as the obtained JSON is empty

    }
    else {
      Log_MACHINE_error(key + " is an invalid AsyncStorage key!") //Special case: the value stored have some problems for some reasons
      return undefined //Return undefined for error report 
    }
  } catch (error) {
    Log_MACHINE_error('Error retrieving SecureStore value: ', error);
  }
};

/* 
 * Author: Ken Lung, Time: 03/04/2024
 * deletetFrom_LocalData_AsyncStorage is a helper function for deleting the AsyncStorage value stored to a specfic key 
 * while creating log messages to state the data has been deleted
 * If the result is not true (valid) after deleting, then the deleting process is successful  
 */

export async function deletetFrom_LocalData_AsyncStorage(key) {
  try {
    await AsyncStorage.removeItem(key)
    let result = await AsyncStorage.getItem(key)
    if (!result) {
      Log_MACHINE_info("The stored AsyncStorage data of " + key + " is deleted!")
    }
  } catch (error) {
    Log_MACHINE_error('Error deleting AsyncStorage data: ', error);
  }
};

export async function deletetFrom_LocalData_AsyncStorage_LessLogging(key) {
  try {
    await AsyncStorage.removeItem(key)
    let result = await AsyncStorage.getItem(key)
    if (!result) {
      Log_MACHINE_info("The stored AsyncStorage data of " + key + " is deleted!")
    }
  } catch (error) {
    Log_MACHINE_error('Error deleting AsyncStorage data: ', error);
  }
};


