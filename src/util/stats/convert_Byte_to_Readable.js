/**
 * Author: Ken Lung, Time: 03/04/2024
 * convert_Byte_to_Readable is a helper function for converting value in Byte to MB
 * 
 * Used in getStorageUsage in Check_MachineStatus.js
 */

export function convert_Byte_to_Readable(value) {

    value = value / (1024 * 1024); //1 MB = 1 * 1024 * 1024 Byte

    return Math.round(value); //Round off the value because we do not need very accuratae number
}
