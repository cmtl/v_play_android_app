import * as NetInfo from '@react-native-community/netinfo'

import { Log_NETWORK_debug, Log_NETWORK_info, Log_NETWORK_warn, Log_NETWORK_error } from '../helper/Log_shortcut'
import { post_Abnormal_Alert } from '../../background_jobs/alert/Send_Alert';


/**
 * Author: Ken Lung, Time: 03/04/2024
 * get_SignalStrength is a helper function for getting the signal strength of the device 
 * The value returned will be rounded off to be an integer from 0 to 10  
 */

export async function get_SignalStrength() {
    let wifi_stat = await NetInfo.fetch()
    return Math.round(wifi_stat.details.strength / 10)
}
/**
 * Author: Ken Lung, Time: 03/04/2024
 * WifiReport is a helper function that groups the other two functions together 
 * and allows them to share the same variable for their operation 
 * wifi_stat is a variable that the device returns detailed data concerning to the device's connection 
 * the following JSON is an example of the data expected to return 
 * {
        "type": "wifi",
        "isConnected": true,
        "isInternetReachable": true,
        "details": {
            "isConnectionExpensive": false,
            "ssid": "your_wifi_network",
            "strength": 75,
            "ipAddress": "192.168.1.4",
            "subnet": "255.255.255.0",
            "frequency": 2462,
            "linkSpeed": 150
        }
    }
 */

export async function WifiReport() {
    let wifi_stat = await NetInfo.fetch();
    get_Connection(wifi_stat)
    get_ConnectionQuality(wifi_stat);

}
/**
 * Author: Ken Lung, Time: 03/04/2024
 * get_ConnectionQuality is a helper function that focuses on the signal strenght
 * After logging the signal strenght data, it will pass the signal strength to Check_Abnormal_Connection
 */

export async function get_ConnectionQuality(stat) {
    let signal_strength = Math.round(stat.details.strength / 10);
    Log_NETWORK_info("Strength (0-10): " + signal_strength);
    Log_NETWORK_info("Linkspeed (in Mbps): " + stat.details.linkSpeed);
    Check_Abnormal_Connection(signal_strength)

}
/**
 * Author: Ken Lung, Time: 03/04/2024
 * get_ConnectionQuality is a helper function that focuses on the the nature of the connection
 * After logging the connection type and if the device is connect, it will pass these data to Check_Sudden_Disconnection
 */

export async function get_Connection(stat) {
    Log_NETWORK_info("Connection Type: " + stat.type);
    Log_NETWORK_info("Is Connected: " + stat.isConnected);
    Check_Sudden_Disconnection(stat.isConnected); 
}

/**
 * Author: Ken Lung, Time: 03/04/2024
 * Check_Sudden_Disconnection is a helper function reports and logs the connection
 * If there was a disconnection 
 */

function Check_Sudden_Disconnection(connected) {
    if (connected == false) {
        Log_NETWORK_error("Player is now disconnected from the Internet!"); //Log disconnection

    }
}

/**
 * Author: Ken Lung, Time: 03/04/2024
 * Check_Abnormal_Connection is a helper function reports and logs the connection's signal strength 
 * If the signal strength is lower than or equal to 4, it will further checks it is lower than or equal to 2
 * Report error or warning depending on signal strength 
 */

function Check_Abnormal_Connection(strength) {

    if (strength <= 4) {
        if (strength <= 2) {
            Log_NETWORK_error("Signal Strength is very low (lower than or equal to 2), please report and investigate soon! (current signal strenght: " + strength + ")"); //Report seriously bad connection to the backend as an error
            post_Abnormal_Alert("Signal Strength is very low (lower than or equal to 2), please report and investigate soon! (current signal strenght: " + strength + ")", "NETWORK_CONDITION") //Report seriously bad connection to the backend as an alert
        }
        else {
            Log_NETWORK_warn("Signal Strength is low (lower than or equal to 4). (current signal strenght: " + strength + ")"); //Severely bad connection, log as warning
            post_Abnormal_Alert("Signal Strength is low (lower than or equal to 4). (current signal strenght: " + strength + ")", "NETWORK_CONDITION") //Report severely bad connection to the backend as an alert

        }
    }
}
