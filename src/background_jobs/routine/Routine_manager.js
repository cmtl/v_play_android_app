import { Routine_PMinfo } from "./Routine_PMinfo";
import { Routine_checkNonRoutine } from "./Routine_checkNonRoutine";
import { Routine_checkRoutine } from "./Routine_checkRoutine";
import { Routine_heartbeat } from "./Routine_heartbeat";
import { Routine_mediaUpdate } from "./Routine_mediaUpdate";

export async function call_Routine_manager(new_schedule_item) {
    switch (new_schedule_item.jobName) {
        case 'SEND_HEARTBEAT':
            await Routine_heartbeat(new_schedule_item)
        case 'GET_PM_INFO':
            await Routine_PMinfo(new_schedule_item)
        case 'CHECK_ROUTINE_SCHEDULE_UPDATE':
            await Routine_checkRoutine(new_schedule_item)
        case 'CHECK_MEDIA_UPDATE_JOB':
            await Routine_mediaUpdate(new_schedule_item)
        case 'CHECK_NON_ROUTINE_JOB':
            await Routine_checkNonRoutine(new_schedule_item)
    }
}


