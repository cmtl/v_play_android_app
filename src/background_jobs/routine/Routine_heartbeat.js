import * as API_Job from '../../API/Fetch_API';
import { getFrom_LocalData, storeAs_LocalData } from '../../util/stats/info/Secured_Machine_Info'
import { WifiReport, get_SignalStrength } from '../../util/stats/Check_Connection'
import Constants from 'expo-constants';
import { DeviceReport } from '../../util/stats/Check_MachineStatus';
import { Log_ROUTINE_debug, Log_ROUTINE_info, Log_ROUTINE_warn, Log_ROUTINE_error } from '../../util/helper/Log_shortcut';
import { Housekeeping_Log } from '../housekeeping/Housekeeping_Log';
import { sleep } from '../../util/helper/Sleep';
import { getFrom_LocalData_LessLogging } from '../../util/stats/info/Secured_Machine_Info';

let routine_version = -1;
let isRunning = false;
export async function Routine_heartbeat(props) {

    try {
        let res;
        let activationKey;
        let androidID;
        let signal_strength;
        let app_ver;
        // check media update props.retryLimits

        if (isRunning == false) {
            isRunning = true;
            if (props.__v != routine_version) {
                if (routine_version == -1) {
                    Log_ROUTINE_info(`Current Send Heartbeat schedule is as follow! (interval: ${props.interval}, retryLimit: ${props.retryLimits}, retryInterval: ${props.retryInterval})`)
                }
                else {
                    Log_ROUTINE_warn(`Send Heartbeat schedule changed! (interval: ${props.interval}, retryLimit: ${props.retryLimits}, retryInterval: ${props.retryInterval})`)
                }
                routine_version = props.__v

            }
            else {
                Log_ROUTINE_info("Send Heartbeaat schedule remains the same!")
            }

            for (let cur_retry = 0; cur_retry < props.retryLimits; cur_retry++) {
                activationKey = await getFrom_LocalData_LessLogging("activationKey");
                androidID = await getFrom_LocalData("androidID");
                signal_strength = await get_SignalStrength();
                app_ver = Constants.expoConfig.version;

                await DeviceReport();
                await WifiReport();

                res = await API_Job.patch_Login_for_Heartbeat(activationKey, androidID, app_ver, signal_strength);


                if (res === true) {
                    Log_ROUTINE_info("Send Heartbeat completed!");
                    return true;
                } else if (cur_retry + 1 < props.retryLimits) {
                    Log_ROUTINE_warn(`Send Heartbeat failed ${cur_retry + 1} time(s)! Pausing for ${props.retryInterval} seconds!`);
                    await sleep(props.retryInterval * 1000);
                } else {
                    Log_ROUTINE_error(`Send Heartbeat failed ${cur_retry + 1} time(s)! Quitting due to exceeding limits allowed!`);
                    return false;
                }
            }
        }
    }
    catch (error) {
        //return error
        Log_ROUTINE_error(`Send Heartbeat failed due to ${error}!`)
        return undefined
    }
    finally {
        isRunning = false
    }
}





