import * as API_Job from '../../API/Fetch_API';
import { Log_ROUTINE_debug, Log_ROUTINE_info, Log_ROUTINE_warn, Log_ROUTINE_error } from '../../util/helper/Log_shortcut';
import { sleep } from '../../util/helper/Sleep';

let routine_version = -1;
let isRunning = false;

export async function Routine_checkNonRoutine(props) {

    let res;

    // a flag checking section to prevent starting the next job before the previous job has finished 
    if (isRunning == false) {
        isRunning = true;
        if (props.__v != routine_version) {
            if (routine_version == -1) {
                Log_ROUTINE_info(`Current Check Non-routine Schedule Update schedule is as follow! (interval: ${props.interval}, retryLimit: ${props.retryLimits}, retryInterval: ${props.retryInterval})`)
            }
            else {
                Log_ROUTINE_warn(`Check Non-routine Schedule Update schedule changed! (interval: ${props.interval}, retryLimit: ${props.retryLimits}, retryInterval: ${props.retryInterval})`)
            }
            routine_version = props.__v
        }
        else {
            Log_ROUTINE_info("Check Non-routine Schedule Update schedule remains the same!")
        }
        // retry until success or reach retry limits 
        try {
            for (let cur_retry = 0; cur_retry != props.retryLimits; cur_retry++) {

                // get the upcoming non-routine jobs from the server 
                // return true when the operation of the job is successful 
                res = await API_Job.get_NonRoutine();

                // return true if the job is successful
                if (res == true) {
                    Log_ROUTINE_info("Check Non-routine Schedule update completed!")
                    return true;
                }

                else if (res != true && (cur_retry + 1 < props.retryLimits)) {
                    Log_ROUTINE_warn(`Check Non-routine Schedule update failed ${cur_retry + 1} time(s)! Pausing for ${props.retryInterval} seconds!`)
                    await sleep(props.retryInterval * 1000);
                }
                else if (res != true && (cur_retry + 1 == props.retryLimits)) {
                    Log_ROUTINE_error(`Check Non-routine Schedule update failed ${cur_retry + 1} time(s)! Quitting due to exceeding limits allowed!`)
                    isRunning = false

                    return false;

                }
            }
        }
        catch (error) {
            //return errors
            Log_ROUTINE_error(`Check Non-routine Schedule update failed due to ${error}!`)
            return undefined;

        }
        finally {
            // set the job to be finished 
            isRunning = false
        }
    }
}
