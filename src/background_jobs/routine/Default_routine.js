// The hard-coded json for the program to follow on first activation 
// will be deemed useless once the device record the new one from the server 
export const default_routine = [
    {
        "jobName": "SEND_HEARTBEAT",
        "interval": 5,
        "retryLimits": 3,
        "retryInterval": 30,
        "__v": 0
    },
   {
        "jobName": "GET_PM_INFO",
        "interval": 10,
        "retryLimits": 3,
        "retryInterval": 30,
        "__v": 0
    },
    {
        "jobName": "CHECK_ROUTINE_SCHEDULE_UPDATE",
        "interval": 60,
        "retryLimits": 3,
        "retryInterval": 30,
        "__v": 0
    },
    {
        "jobName": "CHECK_MEDIA_UPDATE_JOB",
        "interval": 10,
        "retryLimits": 3,
        "retryInterval": 30,
        "__v": 0
    },
    {
        "jobName": "CHECK_NON_ROUTINE_JOB",
        "interval": 10,
        "retryLimits": 3,
        "retryInterval": 30,
        "__v": 0
    }
]
