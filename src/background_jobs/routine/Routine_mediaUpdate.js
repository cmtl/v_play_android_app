import * as API_Job from '../../API/Fetch_API';
import { Log_ROUTINE_debug, Log_ROUTINE_info, Log_ROUTINE_warn, Log_ROUTINE_error } from '../../util/helper/Log_shortcut';
import { sleep } from '../../util/helper/Sleep';



let routine_version = -1;
let isRunning = false;


let arr_of_tasks = [async function () { return await API_Job.get_MediaUpdate("playlist"); },
async function () { return await API_Job.get_MediaUpdate("CC") }];


export async function Routine_mediaUpdate(props) {
    let res;
    let res_cnt = 0;
    if (isRunning == false) {
        isRunning = true
        if (props.__v != routine_version) {
            if (routine_version == -1) {
                Log_ROUTINE_info(`Current Check Media Update Info schedule is as follow! (interval: ${props.interval}, retryLimit: ${props.retryLimits}, retryInterval: ${props.retryInterval})`)
            }
            else {
                Log_ROUTINE_warn(`Check Media Update Info schedule changed! (interval: ${props.interval}, retryLimit: ${props.retryLimits}, retryInterval: ${props.retryInterval})`)
            }
            routine_version = props.__v
        }
        else {
            Log_ROUTINE_info("Check Media Update Info schedule remains the same!")
        }
        try {
            for (let cur_retry = 0; cur_retry != props.retryLimits; cur_retry++) {
                for (let task_no = 0; task_no != arr_of_tasks.length; task_no++) {
                    res = await arr_of_tasks[task_no](); // call function to get returned Promise
                    if (res == true) {
                        res_cnt++
                    }
                }
                if (res_cnt == arr_of_tasks.length) {
                    Log_ROUTINE_info("Check Media Update Info completed!")
                    return true
                }
                else if (res_cnt != arr_of_tasks.length && (cur_retry + 1 < props.retryLimits)) {//&& (cur_retry < props.retryLimits)) {
                    Log_ROUTINE_warn(`Check Media Update Info failed ${cur_retry + 1} time(s)! Pausing for ${props.retryInterval} seconds!`)
                    await sleep(props.retryInterval * 1000);

                }

                else if (res_cnt != arr_of_tasks.length && (cur_retry + 1 == props.retryLimits)) {//&& (cur_retry < props.retryLimits)) {
                    Log_ROUTINE_error(`Check Media Update Info failed ${cur_retry + 1} time(s)! Quitting due to exceeding limits allowed!`)
                    isRunning = false

                    return false;
                }
                res_cnt = 0
            }
        }
        catch (error) {
            //return error
            Log_ROUTINE_error(`Check Media Update Info failed due to ${error}!`)
            return undefined
        }
        finally {
            isRunning = false
        }
    }

}