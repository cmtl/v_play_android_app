import * as API_Job from '../../API/Fetch_API';
import { Log_ROUTINE_error, Log_ROUTINE_info, Log_ROUTINE_warn } from '../../util/helper/Log_shortcut';
import { sleep } from '../../util/helper/Sleep';

let routine_version = -1;
let isRunning = false;

let arr_of_tasks = [
    async function () { let res = await API_Job.get_MediaConfig("playlist"); return res },
    async function () { let res = await API_Job.get_MediaConfig("CC"); return res },
    async function () { let res = await API_Job.get_Address(); return res },
    async function () { let res = await API_Job.get_isMediaReport_Needed(); return res },
    async function () { let res = await API_Job.get_Display_Config(); return res }
]



export async function Routine_PMinfo(props) {
    let res;
    let res_cnt = 0;

    if (isRunning == false) {
        isRunning = true
        if (props.__v != routine_version) {
            if (routine_version == -1) {
                Log_ROUTINE_info(`Current Send PM Info schedule is as follow! (interval: ${props.interval}, retryLimit: ${props.retryLimits}, retryInterval: ${props.retryInterval})`)
            }
            else {
                Log_ROUTINE_warn(`Check Send PM Info schedule changed! (interval: ${props.interval}, retryLimit: ${props.retryLimits}, retryInterval: ${props.retryInterval})`)
            }
            routine_version = props.__v
        }
        else {
            Log_ROUTINE_info("Send PM Info schedule remains the same!")
        }
        try {
            for (let cur_retry = 0; cur_retry != props.retryLimits; cur_retry++) {
                for (let task_no = 0; task_no != arr_of_tasks.length; task_no++) {
                    res = await arr_of_tasks[task_no](); // call function to get returned Promise
                    if (res == true) {
                        res_cnt++
                    }
                    else if (res === null && (task_no == 0 || task_no == 1)) {
                        res_cnt++
                    }
                    //console.log(`current task no: ${task_no}: ${res}`)
                }
                //console.log(res_cnt)
                if (res_cnt == arr_of_tasks.length) {
                    Log_ROUTINE_info("Send PM info completed!")
                    return true
                }
                else if (res_cnt != arr_of_tasks.length && (cur_retry + 1 < props.retryLimits)) {//&& (cur_retry < props.retryLimits)) {
                    Log_ROUTINE_warn(`Send PM info failed ${cur_retry + 1} time(s)! Pausing for ${props.retryInterval} seconds!`)
                    await sleep(props.retryInterval * 1000);
                }

                else if (res_cnt != arr_of_tasks.length && (cur_retry + 1 == props.retryLimits)) {//&& (cur_retry < props.retryLimits)) {
                    Log_ROUTINE_error(`Send PM info failed ${cur_retry + 1} time(s)! Quitting due to exceeding limits allowed!`)
                    isRunning = false
                    return false;

                }
                res_cnt = 0
            }
        }
        catch (error) {
            //return error
            Log_ROUTINE_error(`Send PM info failed due to ${error}!`)
            return undefined
        }
        finally {
            isRunning = false;
        }
    }
}


