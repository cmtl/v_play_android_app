import * as FileSystem from 'expo-file-system'
import moment from 'moment';
import { Checkif_LogExist } from '../../util/helper/Checkif_LogExist';


const log_accumlator = []
const logFilePath = `${FileSystem.documentDirectory}/testing.txt`;

export const custom_fileAsyncTransport_testing_Emergency = async () => {
    try {

        await Checkif_LogExist(logFilePath, log_accumlator)
        log_accumlator.length = 0

    } catch (error) {
        console.error('Error writing log to file:', error);
    }
    finally{
        log_accumlator.length = 0

    }

}

export const custom_fileAsyncTransport_testing = async (logMessage) => {
    try {
        log_accumlator.push(logMessage.msg)

        if (log_accumlator.length >= 5) {

            await Checkif_LogExist(logFilePath, log_accumlator)
            log_accumlator.length = 0
        }
    } catch (error) {
        console.error('Error writing log to file:', error);
    }

}
