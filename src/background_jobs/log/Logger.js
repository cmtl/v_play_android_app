import * as FileSystem from 'expo-file-system';
import { consoleTransport, logger } from 'react-native-logs';
//import * as MediaLibrary from 'expo-media-library'
import moment from 'moment';
import { ALERT_FileTransport } from './ALERT_FileTransport';
import { API_FileTransport } from './API_FileTransport';
import { MACHINE_FileTransport } from './MACHINE_FileTransport';
import { MEDIA_FileTransport } from './MEDIA_FileTransport';
import { NETWORK_FileTransport } from './NETWORK_FileTransport';
import { ROUTINE_FileTransport } from './ROUTINE_FileTransport';
import { RSS_FileTransport } from './RSS_FileTransport';


// Logger configuration set up for console debugging 
const APP_DevConfig = {

  levels: {
    debug: 0,
    info: 1,
    warn: 2,
    error: 3,
  },

  severity: "debug",// __DEV__ ? "debug" : "error",
  //transport: "debug"

  transport: consoleTransport,
  //transport: fileAsyncTransport,
  //transport: consoleTransport,
  transportOptions: {
    colors: {
      info: "blueBright",
      warn: "yellowBright",
      error: "redBright",
      debug : "magenta",
    },
    FS: FileSystem,
    filePath: FileSystem.documentDirectory,
    fileName: `log_APP_${moment().format('DD-MM-YYYY')}.txt`
  },
  async: true,
  dateFormat: "local",
  printLevel: true,
  printDate: true,
  enabled: true,
  enabledExtensions: ["NETWORK", "MACHINE", "MEDIA", "RSS", "ROUTINE", "API", "ALERT"]
};

// Logger configuration set up for logging into file (focus on media)

const MEDIAConfig = {

  levels: {
    debug: 0,
    info: 1,
    warn: 2,
    error: 3,
  },

  severity: "info",// __DEV__ ? "debug" : "error",
  //transport: "debug"

  transport: MEDIA_FileTransport,
  //transport: fileAsyncTransport,
  //transport: consoleTransport,
  transportOptions: {
    colors: {
      info: "blueBright",
      warn: "yellowBright",
      error: "redBright",
      debug: "magenta",
    },
    FS: FileSystem,
    filePath: FileSystem.documentDirectory,
    fileName: `log_MEDIA_${moment().format('DD-MM-YYYY')}.txt`
  },
  async: true,
  dateFormat: "local",
  printLevel: true,
  printDate: true,
  enabled: true,
  enabledExtensions: ["MEDIA"]
};
// Logger configuration set up for logging into file (focus on RSS)
const RSSConfig = {

  levels: {
    debug: 0,
    info: 1,
    warn: 2,
    error: 3,
  },

  severity: "info",// __DEV__ ? "debug" : "error",
  //transport: "debug"

  transport: RSS_FileTransport,
  //transport: fileAsyncTransport,
  //transport: consoleTransport,
  transportOptions: {
    colors: {
      info: "blueBright",
      warn: "yellowBright",
      error: "redBright",
      debug: "magenta",
    },
    FS: FileSystem,
    filePath: FileSystem.documentDirectory,
    fileName: `log_RSS_${moment().format('DD-MM-YYYY')}.txt`
  },
  async: true,
  dateFormat: "local",
  printLevel: true,
  printDate: true,
  enabled: true,
  enabledExtensions: ["RSS"]
};


// Logger configuration set up for logging into file (focus on API)
const APIConfig = {

  levels: {
    debug: 0,
    info: 1,
    warn: 2,
    error: 3,
  },

  severity: "info",// __DEV__ ? "debug" : "error",
  //transport: "debug"

  transport: API_FileTransport,
  //transport: fileAsyncTransport,
  //transport: consoleTransport,
  transportOptions: {
    colors: {
      info: "blueBright",
      warn: "yellowBright",
      error: "redBright",
      debug: "magenta",
    },
    FS: FileSystem,
    filePath: FileSystem.documentDirectory,
    fileName: `log_API_${moment().format('DD-MM-YYYY')}.txt`
  },
  async: true,
  dateFormat: "local",
  printLevel: true,
  printDate: true,
  enabled: true,
  enabledExtensions: ["API"]
};

// Logger configuration set up for logging into file (focus on routine job)
const ROUTINEConfig = {

  levels: {
    debug: 0,
    info: 1,
    warn: 2,
    error: 3,
  },

  severity: "info",// __DEV__ ? "debug" : "error",
  //transport: "debug"

  transport: ROUTINE_FileTransport,
  //transport: fileAsyncTransport,
  //transport: consoleTransport,
  transportOptions: {
    colors: {
      info: "blueBright",
      warn: "yellowBright",
      error: "redBright",
      debug: "magenta",
    },
    FS: FileSystem,
    filePath: FileSystem.documentDirectory,
    fileName: `log_ROUTINE_${moment().format('DD-MM-YYYY')}.txt`
  },
  async: true,
  dateFormat: "local",
  printLevel: true,
  printDate: true,
  enabled: true,
  enabledExtensions: ["ROUTINE"]
};

// Logger configuration set up for logging into file (focus on network connection )
const NETWORKConfig = {

  levels: {
    debug: 0,
    info: 1,
    warn: 2,
    error: 3,
  },

  severity: "info",// __DEV__ ? "debug" : "error",
  //transport: "debug"

  transport: NETWORK_FileTransport,
  //transport: fileAsyncTransport,
  //transport: consoleTransport,
  transportOptions: {
    colors: {
      info: "blueBright",
      warn: "yellowBright",
      error: "redBright",
      debug: "magenta",
    },
    FS: FileSystem,
    filePath: FileSystem.documentDirectory,
    fileName: `log_NETWORK_${moment().format('DD-MM-YYYY')}.txt`
  },
  async: true,
  dateFormat: "local",
  printLevel: true,
  printDate: true,
  enabled: true,
  enabledExtensions: ["NETWORK"]
};
// Logger configuration set up for logging into file (focus on machine's storage and memory )
const MACHINEConfig = {

  levels: {
    debug: 0,
    info: 1,
    warn: 2,
    error: 3,
  },

  severity: "info",// __DEV__ ? "debug" : "error",
  //transport: "debug"

  transport: MACHINE_FileTransport,
  //transport: fileAsyncTransport,
  //transport: consoleTransport,
  transportOptions: {
    colors: {
      info: "blueBright",
      warn: "yellowBright",
      error: "redBright",
      debug: "magenta",
    },
    FS: FileSystem,
    filePath: FileSystem.documentDirectory,
    fileName: `log_MACHINE_${moment().format('DD-MM-YYYY')}.txt`
  },
  async: true,
  dateFormat: "local",
  printLevel: true,
  printDate: true,
  enabled: true,
  enabledExtensions: ["MACHINE"]
};

const ALERTConfig = {

  levels: {
    debug: 0,
    info: 1,
    warn: 2,
    error: 3,
  },

  severity: "info",// __DEV__ ? "debug" : "error",
  //transport: "debug"

  transport: ALERT_FileTransport,
  //transport: fileAsyncTransport,
  //transport: consoleTransport,
  transportOptions: {
    colors: {
      info: "blueBright",
      warn: "yellowBright",
      error: "redBright",
      debug: "",
    },
    FS: FileSystem,
    filePath: FileSystem.documentDirectory,
    fileName: `log_MACHINE_${moment().format('DD-MM-YYYY')}.txt`
  },
  async: true,
  dateFormat: "local",
  printLevel: true,
  printDate: true,
  enabled: true,
  enabledExtensions: ["ALERT"]
};


// create the logger for the entire app for debugging  
let LOG = logger.createLogger(APP_DevConfig);
let NETWORK_LOG = LOG.extend("NETWORK");
let MEDIA_LOG = LOG.extend("MEDIA");
let RSS_LOG = LOG.extend("RSS");
let ROUTINE_LOG = LOG.extend("ROUTINE");
let MACHINE_LOG = LOG.extend("MACHINE");
let API_LOG = LOG.extend("API");
let ALERT_LOG = LOG.extend("ALERT")


// individual loggers, each log messages into a different file 
let U_MEDIA_LOG = logger.createLogger(MEDIAConfig);
let U_RSS_LOG = logger.createLogger(RSSConfig);


let U_API_LOG = logger.createLogger(APIConfig);
let U_ROUTINE_LOG = logger.createLogger(ROUTINEConfig);

let U_MACHINE_LOG = logger.createLogger(MACHINEConfig);
let U_NETWORK_LOG = logger.createLogger(NETWORKConfig);

let U_ALERT_LOG = logger.createLogger(ALERTConfig);


export { ALERT_LOG, API_LOG, MACHINE_LOG, MEDIA_LOG, NETWORK_LOG, ROUTINE_LOG, RSS_LOG };

  export { U_ALERT_LOG, U_API_LOG, U_MACHINE_LOG, U_MEDIA_LOG, U_NETWORK_LOG, U_ROUTINE_LOG, U_RSS_LOG };
























/**
 * async function fileTransport  (msg, enabledExtension, level) {
  const date = moment().format().toString().split('T')[0];
  const fileUri = FileSystem.documentDirectory + date + '_log.txt'
  const timestamp = moment().format('DD-MM-YYYY, HH:mm:ss')
  const logMessage = `${timestamp} | ${enabledExtension} | ${level} : ${msg}\n`
  await FileSystem.appendAsStringAsync
}
 * export async function start_logging() {
  let permissions = await FileSystem.StorageAccessFramework.requestDirectoryPermissionsAsync();
  if (!permissions.granted) {
    LOG.error("Permission for saving log files was denied!")
  }
  else {
    try {
      let fileName = `log_${moment().format('DD-MM-YYYY')}.txt.txt`
      let logData = 'hello'
      
      console.log(permissions.directoryUri)
      FileSystem.StorageAccessFramework.createFileAsync(permissions.directoryUri, fileName , 'text/plain')
      .then(async (fileUri) => {
        await FileSystem.writeAsStringAsync(fileUri, logData, {append: true});
      })
      .catch((error)=> {
        console.log(error)})
      }
      catch (error) {
        throw new Error(error)
      }
    }
  }
 */
