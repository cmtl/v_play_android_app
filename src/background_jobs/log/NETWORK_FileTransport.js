import * as FileSystem from 'expo-file-system'
import moment from 'moment';
import { Checkif_LogExist } from '../../util/helper/Checkif_LogExist';

export const log_accumlator = []
export const logFilePath = `${FileSystem.documentDirectory}/log_NETWORK_${moment().format('DD-MM-YYYY')}.txt`;

export const NETWORK_FileTransport_Emergency = async () => {
    try {

        await Checkif_LogExist(logFilePath, log_accumlator)
        log_accumlator.length = 0

    } catch (error) {
        console.error('Error writing log to file:', error);
    }

}

export const NETWORK_FileTransport = async (logMessage) => {
    try {
        log_accumlator.push(logMessage.msg)
        //console.log(log_accumlator)

        if (log_accumlator.length >= 50) {

            await Checkif_LogExist(logFilePath, log_accumlator)
            log_accumlator.length = 0
        }
    } catch (error) {
        console.error('Error writing log to file:', error);
    }

}


