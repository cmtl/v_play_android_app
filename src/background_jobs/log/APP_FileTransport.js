import * as FileSystem from 'expo-file-system'
import moment from 'moment';
import { Checkif_LogExist } from '../../util/helper/Checkif_LogExist';

export const log_accumlator = []
export const logFilePath = `${FileSystem.documentDirectory}/log_APP_${moment().format('DD-MM-YYYY')}.txt`;

export const APP_FileTransport_Emergency = async () => {
    try {

        await Checkif_LogExist(logFilePath, log_accumlator)
        log_accumlator.length = 0

    } catch (error) {
        console.error('Error writing log to file:', error);
    }
    finally{
        log_accumlator.length = 0

    }

}

export const APP_FileTransport = async (logMessage) => {
    try {
        log_accumlator.push(logMessage.msg)

        if (log_accumlator.length >= 100) {

            await Checkif_LogExist(logFilePath, log_accumlator)
            log_accumlator.length = 0
        }
    } catch (error) {
        console.error('Error writing log to file:', error);
    }

}


