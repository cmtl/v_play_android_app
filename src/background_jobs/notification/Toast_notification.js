import { View, StyleSheet, ToastAndroid, } from 'react-native';

// Toast (pop-up messages) with different display time that will show up at the bottom of the screen 
// They are quite similar so pick freely 
export function showToast(message) {
  ToastAndroid.show(message, ToastAndroid.SHORT);
}

export function showToastWithGravity(message) {
  ToastAndroid.showWithGravity(message, ToastAndroid.SHORT, ToastAndroid.CENTER,);
}

export function showToastWithGravityAndOffset(message) {
  ToastAndroid.showWithGravityAndOffset(message, ToastAndroid.LONG, ToastAndroid.BOTTOM, 25, 50,);
}

