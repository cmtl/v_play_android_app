import { getFrom_LocalData_AsyncStorage, storeAs_LocalData_AsyncStorage, deletetFrom_LocalData_AsyncStorage } from "../../util/stats/info/Async_Machine_Info"
import moment from "moment"

//merge the old stack, if there is one, with the new stack
//reorder them from the oldest on the top and the latest at the bottom 
export async function process_DLedjobs(downloaded_jobs, type) {
    const original_Arr = await getFrom_LocalData_AsyncStorage(`${type}_media_ArrList`)
    let new_Arr;
    
    if (original_Arr === undefined || original_Arr === null) {
        new_Arr = [...downloaded_jobs];

    }
    else {
        new_Arr = [...original_Arr, ...downloaded_jobs];
    }

    if (type == "playlist") {
        new_Arr.sort((a, b) => {
            const dateA = moment(a.newPlaylistMedia.activatedAt);
            const dateB = moment(b.newPlaylistMedia.activatedAt);
            return dateB.diff(dateA); // Sort in descending order
        });
    }
    else if (type == "CC") {
        new_Arr.sort((a, b) => {
            const dateA = moment(a.newCCMedia.activatedAt);
            const dateB = moment(b.newCCMedia.activatedAt);
            return dateB.diff(dateA); // Sort in descending order
        });
    }
    await storeAs_LocalData_AsyncStorage(`${type}_media_ArrList`, new_Arr)
    return new_Arr
}

export function process_2B_DLjobs(download_jobs, type) {
   
    if (type == "playlist") {
        download_jobs.sort((a, b) => {
            const dateA = moment(a.newPlaylistMedia.activatedAt);
            const dateB = moment(b.newPlaylistMedia.activatedAt);
            return dateB.diff(dateA); // Sort in descending order
        });
    }
    else if (type == "CC") {
        download_jobs.sort((a, b) => {
            const dateA = moment(a.newCCMedia.activatedAt);
            const dateB = moment(b.newCCMedia.activatedAt);
            return dateB.diff(dateA); // Sort in descending order
        });
    }
    //console.log(new_Arr);
    
    return download_jobs
}
