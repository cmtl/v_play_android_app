import moment from 'moment';
import { get_MediaUpdate } from '../../API/Fetch_API.js';
import { triggerForcePopping_Deleted_Playlist } from '../../components/player/Media_player_new.js';
import { triggerForcePopping_Deleted_CC } from '../../components/player/RSS_player_new.js';
import { MEDIA_Failed_errorResponse, MEDIA_errorResponse, MEDIA_errorResponse_forIncompleteButPlay } from '../../util/helper/Error_Response.js';
import { Log_MEDIA_error, Log_MEDIA_info, Log_MEDIA_warn } from '../../util/helper/Log_shortcut.js';
import { sleep } from '../../util/helper/Sleep.js';
import { getFrom_LocalData_LessLogging } from '../../util/stats/info/Secured_Machine_Info.js';
import { showToast } from '../notification/Toast_notification.js';
import { checkForJobStatusNotAllowError, checkForMongoDBIndexNotFoundError, checkForNotSatisfyConditionError, checkForPending } from './Checking_for_DownloadMedia.js';

const domain = "https://vplay.365vending.net"
const sleep_time = 3000;

export async function patch_DL_Processing_beforeDL(jobId, attempts = 3) {
    async function call(attempts) {
        const resource_location = `playerDB/eventJobs/mediaUpdate/jobId/${jobId}/report-job-processing`;
        try {
            const accessToken = await getFrom_LocalData_LessLogging("accessToken")
            const response = await fetch(`${domain}/databaseAPI/${resource_location}`, {
                method: 'PATCH',
                headers: {
                    'Origin': 'androidApp',
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${accessToken}`
                },
            });

            if (response.status > 299) {
                if (response.status == 405) {
                    if (checkForJobStatusNotAllowError(response)) {
                        if (checkForPending(response)) {
                            get_MediaUpdate("Playlist")
                            return false
                        }
                    }
                }
                else if (response.status == 400) {
                    if (checkForMongoDBIndexNotFoundError(response)) {
                        return null
                    }

                }
                return MEDIA_errorResponse(response);
            } else {
                showToast(`Running patch_DL_Processing_beforeDL (jobId: ${jobId}) without problem! (status: ${response.status}), start downloading`);
                //showToast(`Successfully Sent "Processing type 1" Status to the backend on downloading media, Downloading media now!`);
                Log_MEDIA_info('Successfully Sent "Processing type 1" Status to the backend on downloading media, Downloading media now!');

                return true;
            }
        } catch (err) {
            showToast(`Request for patch_DL_Processing_beforeDL (Processing type 1) Failed due to ${err}!`);
            Log_MEDIA_warn(`Wait for ${sleep_time / 1000} seconds (current retry attempt: ${attempts})!`);
            sleep(sleep_time)
            if (attempts == 0) {
                return undefined;
            }
            else {
                return await call(attempts - 1);
            }
        }
    }
    return await call(attempts)
}



export async function patch_DL_Processing_afterDL(jobId, downloaded_list, attempts = 3) {
    async function call(attempts) {
        const data = {
            downloaded: downloaded_list
        };

        try {
            const accessToken = await getFrom_LocalData_LessLogging("accessToken")
            const response = await fetch(`${domain}/databaseAPI/${resource_location}`, {
                method: 'PATCH',
                headers: {
                    'Origin': 'androidApp',
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${accessToken}`

                },
                body: JSON.stringify(data)
            });

            if (response.status > 299) {
                if (response.status == 405) {
                    if (checkForJobStatusNotAllowError(response) == true) {
                        if (checkForPending(response) == true) {
                            get_MediaUpdate("Playlist")
                            return false

                        }
                    }
                }
                else if (response.status == 400) {
                    if (checkForMongoDBIndexNotFoundError(response) == true) {
                       
                        return null
                    }

                }
                else {
                    return MEDIA_errorResponse(response);

                }
                Log_MEDIA_error(`Request for patch_DL_Processing_afterDL encountered abnormal status code of ${response.status}!`);

            } else {
                Log_MEDIA_info(`Running patch_DL_Processing_afterDL (jobId: ${jobId}) without problem! (status: ${response.status})`);
                Log_MEDIA_info(`Successfully Sent "Processing type 2" Status to the backend on downloading media, Media downloaded! (Downloaded mediaID(s): ${downloaded_list})`);
                return true;
            }
        } catch (err) {
            Log_MEDIA_info(`Request for patch_DL_Processing_afterDL (jobId: ${jobId}) Failed due to ${err}!`);
            Log_MEDIA_warn(`Wait for ${sleep_time / 1000} seconds until the next retry (current retry attempt: ${attempts})!`);
            sleep(sleep_time)
            if (attempts == 0) {
                return undefined;
            }
            else {
                return await call(attempts - 1);
            }
        }
    }
    return await call(attempts)
}


//MEDIA_LOG.error(`Request for patch_DL_Processing_afterDL (Processing type 2) Failed due to ${err}!!`)
// return undefined

export async function patch_DL_Ready_afterDL(jobId, attempts = 3) {
    async function call(attempts) {
        const resource_location = `playerDB/eventJobs/mediaUpdate/jobId/${jobId}/report-job-ready`;
        try {
            const accessToken = await getFrom_LocalData_LessLogging("accessToken")
            const response = await fetch(`${domain}/databaseAPI/${resource_location}`, {
                method: 'PATCH',
                headers: {
                    'Origin': 'androidApp',
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${accessToken}`
                },
            });

            if (response.status > 299) {
                if (response.status == 405) {
                    if (checkForJobStatusNotAllowError(response) == true) {
                        if (checkForPending(response) == true) {
                            get_MediaUpdate(type)
                            return false

                        }
                    }
                }
                else if (response.status == 400) {
                    
                    if (checkForMongoDBIndexNotFoundError(response) == true) {
                        if (type == "playlist") {
                            triggerForcePopping_Deleted_Playlist()
                        }
                        else if (type == "CC") {
                            triggerForcePopping_Deleted_CC()

                        }
                        return null
                    }

                }
                else {
                    return MEDIA_errorResponse(response);
                }
                Log_MEDIA_error(`Request for patch_DL_Ready_afterDL encountered abnormal status code of ${response.status}!`);

            } else {
                Log_MEDIA_info(`Running patch_DL_Ready_afterDL (jobId: ${jobId}) without problem! (status: ${response.status})`);
                Log_MEDIA_info(`Successfully Sent "Ready" Status to the backend on downloading media, All Media downloaded!`);
                return true;
            }
        } catch (err) {
            Log_MEDIA_error(`Request for patch_DL_Ready_afterDL (jobId: ${jobId}) Failed due to ${err}!`);
            Log_MEDIA_warn(`Wait for ${sleep_time / 1000} seconds until the next retry (current retry attempt: ${attempts})!`);
            sleep(sleep_time)
            if (attempts == 0) {
                return undefined;
            }
            else {
                return await call(attempts - 1);
            }
        }
    }
    return await call(attempts)
}


export async function patch_DL_Uncompleted_afterDL(jobId, attempts = 3) {
    async function call(attempts) {


        const resource_location = `playerDB/eventJobs/mediaUpdate/jobId/${jobId}/report-job-uncompleted`;

        try {
            const accessToken = await getFrom_LocalData_LessLogging("accessToken")
            const response = await fetch(`${domain}/databaseAPI/${resource_location}`, {
                method: 'PATCH',
                headers: {
                    'Origin': 'androidApp',
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${accessToken}`

                },
            });

            if (response.status > 299) {
                if (response.status == 405) {
                    if (checkForJobStatusNotAllowError(response) == true) {
                        if (checkForPending(response) == true) {
                            get_MediaUpdate(type)
                            return false

                        }
                    }
                }
                else if (response.status == 400) {
                    if (checkForMongoDBIndexNotFoundError(response) == true) {
                        if (type == "playlist") {
                            triggerForcePopping_Deleted_Playlist()
                        }
                        else if (type == "CC") {
                            triggerForcePopping_Deleted_CC()

                        }
                        return null
                    }

                }
                else {
                    return MEDIA_errorResponse(response);
                }
                Log_MEDIA_error(`Request for patch_DL_Uncompleted_afterDL encountered abnormal status code of ${response.status}!`);

            } else {
                Log_MEDIA_info(`Running patch_DL_Uncompleted_afterDL (jobId: ${jobId}) without problem! (status: ${response.status})`);
                Log_MEDIA_warn(`Successfully Sent "Uncompleted" Status to the backend on downloading media, some media is MISSING!`);
                return true;
            }
        } catch (err) {
            Log_MEDIA_error(`Request for patch_DL_Uncompleted_afterDL (jobId: ${jobId}) Failed due to ${err}!`);
            Log_MEDIA_warn(`Wait for ${sleep_time / 1000} seconds until the next retry (current retry attempt: ${attempts})!`);
            sleep(sleep_time)
            if (attempts == 0) {
                return undefined;
            }
            else {
                return await call(attempts - 1);
            }
        }
    }
    return await call(attempts)
}


export async function patch_DL_Failed_afterDL(jobId, error, attempts = 3) {
    async function call(attempts) {
        let data = null;
        if (error != null) {
            data = {
                reason: {
                    date: moment().utc(),
                    description: error
                }
            };
        }

        const resource_location = `playerDB/eventJobs/mediaUpdate/jobId/${jobId}/report-job-failed`;

        try {
            const accessToken = await getFrom_LocalData_LessLogging("accessToken")
            const response = await fetch(`${domain}/databaseAPI/${resource_location}`, {
                method: 'PATCH',
                headers: {
                    'Origin': 'androidApp',
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${accessToken}`
                },
                body: JSON.stringify(data)
            });

            if (response.status > 299) {
                if (response.status == 405) {
                    if (checkForJobStatusNotAllowError(response) == true) {
                        if (checkForPending(response) == true) {
                            get_MediaUpdate(type)
                            return false

                        }
                    }
                }
                else if (response.status == 400) {
                    if (checkForMongoDBIndexNotFoundError(response) == true) {
                       
                        return null
                    }

                }
                else {
                    return MEDIA_errorResponse(response);
                }
                
                Log_MEDIA_error(`Request for patch_DL_Failed_afterDL encountered abnormal status code of ${response.status}!`);

                return MEDIA_Failed_errorResponse(response);
            } else {
                Log_MEDIA_info(`Running patch_DL_Failed_afterDL (jobId: ${jobId}) without problem! (status: ${response.status})`);
                Log_MEDIA_warn(`Successfully Sent "Failed" Status to the backend on downloading media! ALL media is MISSING due to ${error}!`);
                return true;
            }
        } catch (err) {
            Log_MEDIA_error(`Request for patch_DL_Failed_afterDL (jobId: ${jobId}) Failed due to ${err}!`);
            Log_MEDIA_warn(`Wait for ${sleep_time / 1000} seconds until the next retry (current retry attempt: ${attempts})!`);
            sleep(sleep_time)
            if (attempts == 0) {
                return undefined;
            }
            else {
                return await call(attempts - 1);
            }
        }
    }
    return await call(attempts)
}


export async function patch_DL_Incomplete_afterDL(jobId, medialist, activation_date, attempts = 3) {
    async function call(attempts) {
        const data = {
            playing: medialist,
            activatedDate: activation_date
        };

        const resource_location = `playerDB/eventJobs/mediaUpdate/jobId/${jobId}/report-incomplete-playlist`;

        try {
            const accessToken = await getFrom_LocalData_LessLogging("accessToken")
            const response = await fetch(`${domain}/databaseAPI/${resource_location}`, {
                method: 'PATCH',
                headers: {
                    'Origin': 'androidApp',
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${accessToken}`
                },
                body: JSON.stringify(data)
            });

            if (response.status > 299) {
                if (response.status == 405) {
                    if (checkForNotSatisfyConditionError(response) == true) {
                        if (checkForPending(response) == true) {
                            get_MediaUpdate(type)
                        }
                    }
                }
                return MEDIA_errorResponse(response);
            } else {
                Log_MEDIA_info(`Running patch_DL_Incomplete_afterDL (jobId: ${jobId}) without problem! (status: ${response.status})`);
                Log_MEDIA_warn(`Successfully Sent "Incompleted" Status to the backend on downloading media, Some media is MISSING but the player plays the playlist anyways!`);
                return true;
            }
        } catch (err) {
            Log_MEDIA_error(`Request for patch_DL_Incomplete_afterDL (jobId: ${jobId}) Failed due to ${err}!`);
            Log_MEDIA_warn(`Wait for ${sleep_time / 1000} seconds until the next retry (current retry attempt: ${attempts})!`);
            sleep(sleep_time)
            if (attempts == 0) {
                return undefined;
            }
            else {
                return await call(attempts - 1);
            }
        }
    }
    return await call(attempts)
}


export async function patch_DL_Completed_afterDL(jobId, activation_date, type, incompleteButPlay, attempts = 3) {
    async function call(attempts) {
        const data = {
            activatedDate: activation_date
        };

        const resource_location = `playerDB/eventJobs/mediaUpdate/jobId/${jobId}/report-job-completed`;

        try {
            const accessToken = await getFrom_LocalData_LessLogging("accessToken")
            const response = await fetch(`${domain}/databaseAPI/${resource_location}`, {
                method: 'PATCH',
                headers: {
                    'Origin': 'androidApp',
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${accessToken}`
                },
                body: JSON.stringify(data)
            });
            if (response.status > 299) {
                if (response.status == 405) {
                    if (checkForJobStatusNotAllowError(response) == true) {
                        if (checkForPending(response) == true) {
                            get_MediaUpdate(type)
                            return MEDIA_errorResponse_forIncompleteButPlay(response, incompleteButPlay);


                        }
                    }
                }
                else if (response.status == 400) {
                    if (checkForMongoDBIndexNotFoundError(response) == true) {
                        if (type == "playlist") {
                            triggerForcePopping_Deleted_Playlist()
                        }
                        else if (type == "CC") {
                            triggerForcePopping_Deleted_CC()

                        }
                        return null
                    }

                }
                else {
                    return MEDIA_errorResponse(response);
                }
            } else {
                Log_MEDIA_info(`Running patch_DL_Completed_afterDL (type: ${type}, jobId: ${jobId}) without problem! (status: ${response.status})`);
                Log_MEDIA_info(`Successfully Sent "Completed" Status to the backend on downloading media, playing media successfully!`);
                return true;
            }
        } catch (err) {
            Log_MEDIA_error(`Request for patch_DL_Completed_afterDL (type: ${type}, jobId: ${jobId}) Failed due to ${err}!`);
            Log_MEDIA_warn(`Wait for ${sleep_time / 1000} seconds until the next retry (current retry attempt: ${attempts})!`);
            sleep(sleep_time)
            if (attempts == 0) {
                return undefined;
            }
            else {
                return await call(attempts - 1);
            }
        }
    }
    return await call(attempts)
}


export async function patch_DL_PlayOnce_afterDL(jobId, finishedDate, type, attempts = 3) {
    async function call(attempts) {

        const data = {
            finishedDate: finishedDate
        };

        const resource_location = `playerDB/eventJobs/mediaUpdate/jobId/${jobId}/report-one-time-job-finished`;

        try {
            const accessToken = await getFrom_LocalData_LessLogging("accessToken")
            const response = await fetch(`${domain}/databaseAPI/${resource_location}`, {
                method: 'PATCH',
                headers: {
                    'Origin': 'androidApp',
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${accessToken}`
                },
                body: JSON.stringify(data)
            });

            if (response.status > 299) {
                return MEDIA_errorResponse(response);
            } else {
                Log_MEDIA_info(`Running patch_DL_PlayOnce_afterDL (type: ${type}, jobId: ${jobId}) without problem! (status: ${response.status})`);
                Log_MEDIA_info(`Successfully Sent "Finished" Status to the backend on playing PlayOnce playlist!`);
                return true;
            }
        } catch (err) {
            Log_MEDIA_error(`Request for patch_DL_PlayOnce_afterDL (type: ${type}, jobId: ${jobId}) Failed due to ${err}!`);
            Log_MEDIA_warn(`Wait for ${sleep_time / 1000} seconds until the next retry (current retry attempt: ${attempts})!`);
            sleep(sleep_time)
            if (attempts == 0) {
                return undefined;
            }
            else {
                return await call(attempts - 1);
            }
        }
    }
    return await call(attempts)
}



