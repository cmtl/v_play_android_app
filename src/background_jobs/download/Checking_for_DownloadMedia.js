export function checkForJobStatusNotAllowError(response) {
    const json = response.json();
    if (json.errCode == 405004) {
        return true;
    }
    else {
        return false;
    }
}
export function checkForMongoDBIndexNotFoundError(response) {
    const json = response.json();
    if (json.errCode == 404001) {
        return true;
    }
    else {
        return false;
    }
}
export function checkForNotSatisfyConditionError(response) {
    const json = response.json();
    if (json.errCode == 405007) {
        return true;
    }
    else {
        return false;
    }

}
export function checkForPending(response) {
    const json = response.json();
    if (json.errDetail == "pending") {
        return true;
    }
    else {
        return false;
    }
}
