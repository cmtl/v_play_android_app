import * as FileSystem from 'expo-file-system';
//import * as MediaLibrary from 'expo-media-library';
import { MEDIA_errorResponse } from '../../util/helper/Error_Response.js';
import { Log_MEDIA_error, Log_MEDIA_info, Log_MEDIA_warn } from '../../util/helper/Log_shortcut.js';
import { sleep } from '../../util/helper/Sleep.js';
//import { getDCIM_dir } from '../../util/helper/get_Directory.js';
import moment from 'moment';
//import { setIsLoading } from '../../../src/components/player/Media_player_new.js';
import { getFrom_LocalData_LessLogging } from '../../util/stats/info/Secured_Machine_Info.js';
import { ConfigReport } from '../modify_FromConfig/ConfigReport.js';
import { showToast } from '../notification/Toast_notification.js';
import { patch_DL_Completed_afterDL, patch_DL_Failed_afterDL, patch_DL_Processing_afterDL, patch_DL_Ready_afterDL, patch_DL_Uncompleted_afterDL } from './DownloadMedia_ReportStatus.js';




const domain = "https://vplay.365vending.net"

async function download_single_file(resource_locaiton, file_location, fileName, mediaid) {
  const accessToken = await getFrom_LocalData_LessLogging("accessToken");
  let attempt = 0;
  let success = false;
  let options = {
    headers: {
      'Origin': 'androidApp',
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': `Bearer ${accessToken}`,
    },
  };
  while (attempt < 3 && !success) {
  try {

    let result = await FileSystem.downloadAsync(`${domain}/databaseAPI/${resource_locaiton}`, file_location, options);

    if (result == null || result == undefined) {
      Log_MEDIA_warn((`Cannot download ${fileName} (mediaID: ${mediaid})!`))
    }
    else {
      Log_MEDIA_warn(`${fileName} (mediaID: ${mediaid}) is downloaded!`)
      success = true
      return true
    }
  }
  catch (error) {
    Log_MEDIA_error((`An unexpected error happeend while downloading  ${fileName} due to ${error}!`))
    return undefined;
  }
  attempt++;
  if(!success && attempt < 3){
    Log_MEDIA_error('Failed to download file in '+ attempt +' attempts, retrying...')
  }
}
if(!success) {
  Log_MEDIA_error('Failed to download file after 3 attempts')
  return false;
}

}



async function get_MediaFiles_from_Server_mediaID_inPlaylist(new_playlist_item) {
  let pmid = await getFrom_LocalData_LessLogging("pmID");
  let mediaid = new_playlist_item.uploadedMedia.mediaId
  let fileName = mediaid + '.' + new_playlist_item.uploadedMedia.fileName.split('.').pop()
  const accessToken = await getFrom_LocalData_LessLogging("accessToken");


  let confirm_download = false;

  const file_location = FileSystem.documentDirectory + fileName;
  //console.log(file_location)
  const fileInfo = await FileSystem.getInfoAsync(file_location);
  // console.log(fileInfo)

  if (fileInfo.exists == true) {
    // If yes, set the file URI state
    Log_MEDIA_info(`${fileName} exists! (Original name: ${new_playlist_item.uploadedMedia.fileName})`)

    confirm_download = true
    return confirm_download;
  }
  else {
    Log_MEDIA_info(`${fileName} does not exist! (Original name: ${new_playlist_item.uploadedMedia.fileName})`)
    const resource_locaiton = `playerDB/upload/media/playlist/pmId/${pmid}/download-media/${mediaid}`

    //console.log('file_location: ' + file_location)

    return new Promise((resolve, reject) => {
      let attempt = 0;
      const downloadFile = async () => {
        try {
          const response = await fetch(`${domain}/databaseAPI/${resource_locaiton}`, {
            method: 'GET',
            headers: {
              'Origin': 'androidApp',
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'Authorization': `Bearer ${accessToken}`,
            },
          });

          if (response.status > 299) {
            Log_MEDIA_error(`Request for get_MediaFiles_from_Server_mediaID_inPlaylist encountered abnormal status code of ${response.status}!`);
            return MEDIA_errorResponse(response);
          } else {
            Log_MEDIA_info(`No Client or Server issue! Getting media file from the server...`);
            const downloadResponse = await download_single_file(resource_locaiton, file_location, fileName, mediaid);
            if (downloadResponse === true) {
              confirm_download = true;
            }
          }

          if (confirm_download === true) {
            Log_MEDIA_info(`Media file ${fileName} is downloaded in ${file_location}!`);
            resolve(confirm_download);
          } else {
            Log_MEDIA_error(`Media file ${fileName} is NOT downloaded!`);
            reject(new Error(`Media file ${fileName} is NOT downloaded!`));
          }
        } catch (error) {
          Log_MEDIA_error(`Something wrong happened when downloading media file ${fileName} due to ${error}!`);
          reject(error);
        }
      };

      const attemptDownload = async () => {
        try {
          await downloadFile();
        } catch (error) {
          attempt++;
          if (attempt < 3) {
            Log_MEDIA_error(`Failed to download file in attempt ${attempt}, retrying...`);
            attemptDownload();
          } else {
            Log_MEDIA_error(`Failed to download file after 3 attempts`);
            reject(new Error(`Failed to download file after 3 attempts`));
          }
        }
      };

      attemptDownload();
    });

    //return confirm_download;
  }

}

export async function DownloadPlaylist(new_playlist) {
  let total_list = []
  let downloaded_list = []
  let completed = false
  
  try {

    if (new_playlist.newPlaylistMedia.content.length == 0) {
      Log_MEDIA_warn(`This playlist (jobId: ${new_playlist.jobId}) is empty!`) //tbd
      await patch_DL_Completed_afterDL(new_playlist.jobId, moment().utc(),'playlist')
      Log_MEDIA_warn('No media in the playlist!') //tbd
      return true
    }
    else {
      for (let retry = 0; retry != new_playlist.downloadRetryLimits; retry++) {
        for (let item_num = 0; item_num != new_playlist.newPlaylistMedia.content.length; item_num++) {
          
            let mediaID = new_playlist.newPlaylistMedia.content[item_num].uploadedMedia.mediaId;
            total_list.push(mediaID)
            //const res = await get_MediaFiles_from_Server(new_playlist.newPlaylistMedia.content[item_num])
            const res = await get_MediaFiles_from_Server_mediaID_inPlaylist(new_playlist.newPlaylistMedia.content[item_num])

            if (res == true) {
              downloaded_list.push(mediaID)
              let dl_res = await patch_DL_Processing_afterDL(new_playlist.jobId, downloaded_list)
              if (dl_res === null) {
                continue
              }
            }
            if (downloaded_list.length == total_list.length) {
              completed = true
            }    
        }
        if (completed == false) {
          Log_MEDIA_warn(`Cannot download all media from the playlist for ${retry + 1} time(s)!`)

          if (retry + 1 < new_playlist.downloadRetryLimits) {
            Log_MEDIA_warn((`Cannot download all media from playlist ${new_playlist.newPlaylistMedia.playlistName} and store into the device for ${retry + 1} time(s)! Now pause for ${downloadRetryInterval} min!`))
            total_list = []
            downloaded_list = []
            await sleep(new_playlist.downloadRetryInterval * 60 * 1000);
            continue;
          }
          else if (retry + 1 == new_playlist.downloadRetryLimits) {
            Log_MEDIA_error(`Cannot download all media from playlist ${new_playlist.newPlaylistMedia.playlistName} and store into the device for ${retry + 1} time(s)! Quitting due to exceeding limits allowed!`)

            if (downloaded_list.length == 0) {
              Log_MEDIA_error(`No media in playlist ${new_playlist.newPlaylistMedia.playlistName} (JobID: ${new_playlist.jobId}) is downloaded but the list is not empty!`)
              showToast(`Missed all mediaID(s) during DL (Job ID: ${new_playlist.jobId})!`)

              await patch_DL_Failed_afterDL(new_playlist.jobId, "No media is downloaded despite the list is not empty!")
              //return false;

            }
            else if (downloaded_list.length != total_list.length) {
              Log_MEDIA_warn(`Not all media from playlist ${new_playlist.newPlaylistMedia.playlistName} (JobID: ${new_playlist.jobId}) is downloaded! (Downloaded mediaID(s): ${downloaded_list}, missing mediaID(s): ${total_list.filter((element) => !downloaded_list.includes(element))}`)
              showToast(`Missed some mediaID(s) during DL (Job ID: ${new_playlist.jobId})!`)

              let uncomplete_res = await patch_DL_Uncompleted_afterDL(new_playlist.jobId)
              if (uncomplete_res === null) {
                continue
              }
              completed = true

            }
            break

          }

        }

        else if (completed == true) {
          Log_MEDIA_info(`Can download playlist ${new_playlist.newPlaylistMedia.playlistId} and store into the device!`)
          Log_MEDIA_info(`All media from playlist ${new_playlist.newPlaylistMedia.playlistId} (JobID: ${new_playlist.jobId}) is downloaded! (Downloaded mediaID(s): ${downloaded_list})`)
          showToast(`All media from playlist is downloaded! (Job ID: ${new_playlist.jobId})`)

          await patch_DL_Ready_afterDL(new_playlist.jobId)

          break
        }

      }
      return completed
    }
  }
  catch (error) {
    Log_MEDIA_error((`An unexpected error happeend while downloading  ${new_playlist.newPlaylistMedia.playlistName}! ${error}`))
  }

}
/*
export async function rssreDownloadPlaylist(new_playlist) {

  let total_list = []
  let downloaded_list = []
  let completed = false

  try {

    for (let retry = 0; retry != new_playlist.downloadRetryLimits; retry++) {
      for (let item_num = 0; item_num != new_playlist.newPlaylistMedia.content.length; item_num++) {
        if (new_playlist.newPlaylistMedia.content.length == 0) {
          Log_MEDIA_warn("This is #" + list_num + " playlist and the list is empty!")
          continue;
        }
        else {
          let mediaID = new_playlist.newPlaylistMedia.content[item_num].uploadedMedia.mediaId;
          total_list.push(mediaID)
          //const res = await get_MediaFiles_from_Server(new_playlist.newPlaylistMedia.content[item_num])
          const res = await get_MediaFiles_from_Server_mediaID_inPlaylist(new_playlist.newPlaylistMedia.content[item_num], new_playlist)

          if (res == true) {
            downloaded_list.push(mediaID)
            //await patch_DL_Processing_afterDL(new_playlist.jobId, downloaded_list)
          }
          if (downloaded_list.length == total_list.length) {
            completed = true
          }
          /**
           * if (total_list.includes(mediaID) && downloaded_list.includes(mediaID)) {
            continue
          }
           
          //console.log('total_list: ' + total_list)
          //console.log('downloaded_list: ' + downloaded_list)
        }
      }


      if (completed == false) {
        showToast(`Cannot download all media from the playlist for ${retry + 1} time(s)!`)

        if (retry + 1 < new_playlist.downloadRetryLimits) {
          Log_MEDIA_warn((`Cannot download all media from playlist ${new_playlist.newPlaylistMedia.playlistName} (JobID: ${new_playlist.jobId}) and store into the device for ${retry + 1} time(s)! Now pause for ${downloadRetryInterval} min!`))

          total_list = []
          downloaded_list = []
          await sleep(new_playlist.downloadRetryInterval * 60 * 1000);
          continue;
        }
        else if (retry + 1 == new_playlist.downloadRetryLimits) {
          Log_MEDIA_error(`Cannot download all media from playlist ${new_playlist.newPlaylistMedia.playlistName} (JobID: ${new_playlist.jobId}) and store into the device for ${retry + 1} time(s)! Quitting due to exceeding limits allowed!`)


          if (downloaded_list.length == 0) {
            Log_MEDIA_error(`No media in playlist ${new_playlist.newPlaylistMedia.playlistName} (JobID: ${new_playlist.jobId}) is downloaded but the list is not empty!`)
            showToast(`No media in playlist is downloaded but the list is not empty!`)

            //await patch_DL_Failed_afterDL(new_playlist.jobId, "No media is downloaded despite the list is not empty!")
          }
          else if (downloaded_list.length != total_list.length) {
            Log_MEDIA_warn(`Not all media from playlist ${new_playlist.newPlaylistMedia.playlistName} (JobID: ${new_playlist.jobId}) is downloaded! (Downloaded mediaID(s): ${downloaded_list}, missing mediaID(s): ${total_list.filter((element) => !downloaded_list.includes(element))}`)
            showToast(`Not all media from playlist is downloaded but the list is not empty! (missing mediaID(s): ${total_list.filter((element) => !downloaded_list.includes(element))})`)

            //await patch_DL_Uncompleted_afterDL(new_playlist.jobId)

          }

          return false;
        }

      }

      else if (completed == true) {
        Log_MEDIA_info(`Can download playlist ${new_playlist.newPlaylistMedia.playlistName} and store into the device!`)
        Log_MEDIA_info(`All media from playlist ${new_playlist.newPlaylistMedia.playlistName} (JobID: ${new_playlist.jobId}) is downloaded! (Downloaded mediaID(s): ${downloaded_list})`)
        showToast(`All media from playlist is downloaded! (Downloaded mediaID(s): ${downloaded_list})`)

        await patch_DL_Ready_afterDL(new_playlist.jobId)
        //triggerUpdateList()
        return true
      }
    }
  }
  catch (error) {
    Log_MEDIA_error((`An unexpected error happeend while downloading  ${new_playlist.newPlaylistMedia.playlistName}! ${error}`))
  }


}
*/
export async function reDownloadItem(reDL_item_content) {

  let id = reDL_item_content[0]
  let fileName = reDL_item_content[1]
  let res = await get_MediaFiles_from_Server_mediaID(id, fileName)

  
  if (res == true) {
    Log_MEDIA_info(`The missing media (mediaID: ${id}) has been re-downloaded!`)
    showToast(`The missing media (mediaID: ${id}) has been re-downloaded!`)
    await ConfigReport(id)

  }
  else {
    Log_MEDIA_error(`The missing media (mediaID: ${id}) has NOT been re-downloaded!`) //tbd on refreshing the app
    showToast(`The missing media (mediaID: ${id}) has NOT been re-downloaded!`)
    
  }

}


async function get_MediaFiles_from_Server_mediaID (id, fileName) {
  const pmid = await getFrom_LocalData_LessLogging("pmID");
  const accessToken = await getFrom_LocalData_LessLogging("accessToken");

  let confirm_download = false;

  const file_location = FileSystem.documentDirectory + fileName;
  const fileInfo = await FileSystem.getInfoAsync(file_location);

  if (fileInfo.exists == true) {
    // If yes, set the file URI state
    Log_MEDIA_info(`${fileName} exists! (Original name: ${fileName})`)
    confirm_download = true
    return confirm_download;
  }
  else {
    Log_MEDIA_info(`${fileName} does not exist! (Original name: ${fileName})`)
    const resource_locaiton = `playerDB/upload/media/playlist/pmId/${pmid}/download-media/${id}`

    //console.log('file_location: ' + file_location)

    let attempt = 0;
    let success = false;

    while (attempt < 3 && !success) {
      try {
        const response = await fetch(`${domain}/databaseAPI/${resource_locaiton}`, {
          method: 'GET',
          headers: {
            'Origin': 'androidApp',
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': `Bearer ${accessToken}`,
          },
        });

        if (response.status > 299) {
          Log_MEDIA_error(`Request for get_MediaFiles_from_Server_mediaID encountered abnormal status code of ${response.status}!`);
          return MEDIA_errorResponse(response);
        }
        else {
          Log_MEDIA_info(`No Client or Server issue! Getting media file from the server...`);
          const response = await download_single_file(resource_locaiton, file_location, fileName, id);
          if (response == true) {
            confirm_download = true;
            success = true;
          }
        }
      }
      catch (error) {
        Log_MEDIA_error(`Something wrong happened when downloading media file ${fileName} due to ${error}!`);
      }

      attempt++;
      if (!success && attempt < 3) {
        await sleep(1000); // Wait for 1 second before retrying
      }
    }

    if (confirm_download == true) {
      Log_MEDIA_info(`Media file ${fileName} is downloaded in ${file_location}!`);
    }
    else {
      Log_MEDIA_error(`Media file ${fileName} is NOT downloaded!`);
    }
    return confirm_download;
  }
}


