import { Get_logs } from "./NRoutine_Get_logs";
import { Reload_app } from "./NRoutine_Reload_app";
import { Upgrade_app } from "./NRoutine_Upgrade_app";
import { getFrom_LocalData, storeAs_LocalData } from "../../util/stats/info/Secured_Machine_Info";
import { Log_ROUTINE_debug, Log_ROUTINE_warn, Log_ROUTINE_error, Log_ROUTINE_info } from "../../util/helper/Log_shortcut";
import moment from "moment";

export async function call_NRoutine_manager(new_schedule) {
    let start_time;
    let item_done;
    for (let task_no = 0; task_no != new_schedule.length; task_no++) {
        start_time = new_schedule[task_no].startAt
        if (moment().isSameOrAfter(moment(start_time).format()) 
        && (new_schedule[task_no].status == 'pending' || new_schedule[task_no].status == 'read')) {
            switch (new_schedule[task_no].jobName) {
                case 'UPLOAD_LOG':
                    await Get_logs(new_schedule[task_no])
                    break;
              case 'RESTART_ANDROID_APP':
                    await Reload_app(new_schedule[task_no]);
                    break;
                case 'ANDROPD_APP_UPGRADE':
                    await Upgrade_app(new_schedule[task_no]);
                    break;
            }
        }
        else {
            Log_ROUTINE_info('No non-routine job needs to be executed!')
            continue
        }

    }
}