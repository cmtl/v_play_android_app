import * as FileSystem from 'expo-file-system';
import * as Sharing from 'expo-sharing';
import moment from 'moment';
import { Log_API_error, Log_API_info, Log_ROUTINE_error, Log_ROUTINE_info, Log_ROUTINE_warn } from '../../util/helper/Log_shortcut';
import { sleep } from '../../util/helper/Sleep';
import { Zip_file, find_Zippedfile } from '../../util/helper/Zip_file';
import { getFrom_LocalData_LessLogging } from '../../util/stats/info/Secured_Machine_Info';
import { API_FileTransport_Emergency } from '../log/API_FileTransport';
import { MACHINE_FileTransport_Emergency } from '../log/MACHINE_FileTransport';
import { MEDIA_FileTransport_Emergency } from '../log/MEDIA_FileTransport';
import { NETWORK_FileTransport_Emergency } from '../log/NETWORK_FileTransport';
import { ROUTINE_FileTransport_Emergency } from '../log/ROUTINE_FileTransport';
import { RSS_FileTransport_Emergency } from '../log/RSS_FileTransport';
import { patch_NR_Completed, patch_NR_Failed } from './NRoutine_ReportStatus';


//const dirUri = FileSystem.documentDirectory; // replace with your directory URI if different
//const dirInfo = await FileSystem.readDirectoryAsync(dirUri);

const domain = "https://vplay.365vending.net"

let isRunning = false;

export async function share_SingleLog(fileName) {
    const fileUri = FileSystem.documentDirectory + fileName
    const fileInfo = await FileSystem.getInfoAsync(fileUri);
    if (fileInfo.exists) {
        const data = await FileSystem.readAsStringAsync(fileUri)
        const newFileIri = FileSystem.cacheDirectory + `${fileName}`
        await FileSystem.writeAsStringAsync(newFileIri, data)
        await Sharing.shareAsync(newFileIri);
    }
}

export async function Get_logs(props) {
    const dirUri = FileSystem.documentDirectory;
    const dirInfo = await FileSystem.readDirectoryAsync(dirUri);
    let logType;
    let required_logs = [];
    let utcdate;
    let res = null;
    try {
        if (isRunning == false) {
            isRunning = true
            for (let cur_retry = 0; cur_retry != props.retryLimits; cur_retry++) {

                for (const file of dirInfo) {
                    if (file.startsWith('log_')) {
                        utcdate = moment(file.split('_')[2].split('.')[0], "DD-MM-YYYY").utc()
                        utcdate = utcdate.add(8,'hours');
                        //Log_ROUTINE_warn(`Checking file ${file} with date ${utcdate}!`);
                        if (moment(utcdate).isBetween(props.from, props.to, null, '[]')) {
                      //      Log_ROUTINE_warn(`File ${file} is within the range!`);
                            logType = file.split('_')[1]

                            if ((props.logType).includes(logType)) {
                                //console.log(`file with matching type!`)
                                switch (logType) {
                                    case "API":
                                        await API_FileTransport_Emergency()
                                        break
                                    case "MACHINE":
                                        await MACHINE_FileTransport_Emergency()
                                        break
                                    case "MEDIA":
                                        await MEDIA_FileTransport_Emergency()
                                        break
                                    case "NETWORK":
                                        await NETWORK_FileTransport_Emergency()
                                        break
                                    case "ROUTINE":
                                        await ROUTINE_FileTransport_Emergency()
                                        break
                                    case "RSS":
                                        await RSS_FileTransport_Emergency()
                                        break
                                }
                                required_logs.push(file)
                            }
                        }
                    }
                }
                if (required_logs.length == 0) {
                    Log_ROUTINE_error(`There is no log matches with the requirement(s) posed by the request!`)
                    patch_NR_Failed(props.jobId, "No logs matches requirement(s)")
                    break;

                }
                else {
                    await Zip_file(required_logs, moment().format('DD-MM-YYYY') + '_log_package')
                    let zipped_file = await find_Zippedfile(moment().format('DD-MM-YYYY') + '_log_package')
                    if (zipped_file === null || zipped_file == undefined) {
                        break;
                    }
                    else {
                        res = await post_Uploadlogs(props.jobId, zipped_file, moment().format('DD-MM-YYYY') + '_log_package')//post_Uploadlogs_fetchWithFS(props.jobId, zipped_file, moment().format('DD-MM-YYYY') + '_log_package')
                    }

                    if (res == true) {
                        Log_ROUTINE_info("Upload log completed!")
                        patch_NR_Completed(props.jobId, moment().utc())
                        return true
                    }
                    else if (res != true && (cur_retry + 1 < props.retryLimits)) {
                        Log_ROUTINE_warn(`Upload log failed ${cur_retry + 1} time(s)! Pausing for ${props.retryInterval} seconds!`)
                        await sleep(props.retryInterval * 1000);
                    }
                    else if (res != true && (cur_retry + 1 == props.retryLimits)) {
                        Log_ROUTINE_error(`Upload log failed ${cur_retry + 1} time(s)! Quitting due to exceeding limits allowed!`)
                        patch_NR_Failed(props.jobId, "Exceeding limits allowed")
                        return false;
                    }
                    required_logs = []
                }
            }
        }
    }
    catch (error) {
        Log_ROUTINE_error(`Upload log failed due to ${error}!`)
        patch_NR_Failed(props.jobId, error)
        return undefined
    }
    finally {
        isRunning = false;
    }
}

async function post_Uploadlogs (jobId, zipped_file, zipped_name) {
    try {
        let attempts = 0;
        let success = false;
        while (attempts < 3 && !success) {
            const accessToken = await getFrom_LocalData_LessLogging("accessToken")
            const resource_locaiton = `playerDB/upload/log/jobId/${jobId}`
            const url = `${domain}/databaseAPI/${resource_locaiton}`; // Replace with your backend server URL
            ; // Replace with your file's name
        
            const options = {
                headers: {
                    'Origin': 'androidApp',
                    'Content-Type': 'multipart/form-data',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${accessToken}`
                },
                httpMethod: 'POST',
                uploadType: FileSystem.FileSystemUploadType.MULTIPART,
                fieldName: zipped_name, // This is the name expected by the server for the file
            };
        
            const response = await FileSystem.uploadAsync(url, zipped_file, options);
            if (response.status > 299) {
                Log_API_error(`Request for patch_Login encountered abnormal status code of ${response.status}!`);
                attempts++;
            } else {
                Log_API_info(`Running post_Uploadlogs without problem! (status: ${response.status})`);
                success = true;
            }
        }
        if (!success) {
            Log_API_info(`Request for post_Uploadlogs failed after 3 attempts!`);
            return false;
        }
        return true;
    } catch (error) {
        Log_API_info(`Request for post_Uploadlogs encountered some problems due to ${error}!`);
        return false;
    }
}









