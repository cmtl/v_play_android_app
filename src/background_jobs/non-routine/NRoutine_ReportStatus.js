import moment from 'moment';
import { getFrom_LocalData } from '../../util/stats/info/Secured_Machine_Info.js';
import { Log_ROUTINE_info, Log_ROUTINE_warn, Log_ROUTINE_error } from '../../util/helper/Log_shortcut.js';
import { ROUTINE_Failed_errorResponse, ROUTINE_errorResponse } from '../../util/helper/Error_Response.js';
import { getFrom_LocalData_LessLogging } from '../../util/stats/info/Secured_Machine_Info.js';


const domain = "https://vplay.365vending.net"





  export async function patch_NR_Failed(jobId, error) {
    let data = null;
    if (error != null) {
        data = {
            reason: {
                date: moment().utc(),
                description: error
            }
        };
    }

    const resource_location = `playerDB/eventJobs/jobId/${jobId}/report-job-status/failed`;
    const accessToken =  await getFrom_LocalData_LessLogging("accessToken")
    try {
        const response = await fetch(`${domain}/databaseAPI/${resource_location}`, {
            method: 'PATCH',
            headers: {
                'Origin': 'androidApp',
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${accessToken}`
            },
            body: JSON.stringify(data)
        });

        if (response.status > 299) {
            return ROUTINE_Failed_errorResponse(response);
        } else {
            Log_ROUTINE_info(`Running patch_NR_Failed (jobId: ${jobId}) without problem! (status: ${response.status})`);
            Log_ROUTINE_warn(`Successfully Sent "Failed" Status to the backend on doing non-routine job! Non-routine job failed due to ${error}!`);
            return true;
        }
    } catch (err) {
        Log_ROUTINE_error(`Request for patch_NR_Failed (jobId: ${jobId}) Failed due to ${err}!`);
        return undefined;
    }
}


export async function patch_NR_Completed(jobId) {
  const data = {
      activationDate: moment().utc(),
  };

  const resource_location = `playerDB/eventJobs/jobId/${jobId}/report-job-status/completed`;
  const accessToken =  await getFrom_LocalData_LessLogging("accessToken")
  try {
      const response = await fetch(`${domain}/databaseAPI/${resource_location}`, {
          method: 'PATCH',
          headers: {
              'Origin': 'androidApp',
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'Authorization': `Bearer ${accessToken}`
          },
          body: JSON.stringify(data)
      });

      if (response.status > 299) {
          return ROUTINE_errorResponse(response);
      } else {
          Log_ROUTINE_info(`Running patch_NR_Completed (jobId: ${jobId}) without problem! (status: ${response.status})`);
          Log_ROUTINE_info(`Successfully Sent "Completed" Status to the backend on doing non-routine job! Non-routine job completed!`);
          return true;
      }
  } catch (err) {
      Log_ROUTINE_error(`Request for patch_NR_Completed (jobId: ${jobId}) Failed due to ${err}!`);
      return undefined;
  }
}


export async function patch_NR_Processing(jobId) {
  const data = {
      activationDate: moment().utc(),
  };

  const resource_location = `playerDB/eventJobs/jobId/${jobId}/report-job-status/processing`;
  const accessToken =  await getFrom_LocalData_LessLogging("accessToken")
  try {
      const response = await fetch(`${domain}/databaseAPI/${resource_location}`, {
          method: 'PATCH',
          headers: {
              'Origin': 'androidApp',
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'Authorization': `Bearer ${accessToken}`
          },
          body: JSON.stringify(data)
      });

      if (response.status > 299) {
          return ROUTINE_errorResponse(response);
      } else {
          Log_ROUTINE_info(`Running patch_NR_Processing (jobId: ${jobId}) without problem! (status: ${response.status})`);
          Log_ROUTINE_info(`Successfully Sent "Processing" Status to the backend on doing non-routine job! Doing non-routine job now!`);
          return true;
      }
  } catch (err) {
      Log_ROUTINE_error(`Request for patch_NR_Processing (jobId: ${jobId}) Failed due to ${err}!`);
      return undefined;
  }
}

