import { resetState } from '../../../App';
import { Log_ROUTINE_error, Log_ROUTINE_warn } from '../../util/helper/Log_shortcut';
import { sleep } from '../../util/helper/Sleep';
import { patch_NR_Completed, patch_NR_Failed } from './NRoutine_ReportStatus';

const handleReset = () => {
    resetState();
  };
let isRunning = false;

export async function Reload_app(props) {
    try {
        if (isRunning == false) {
            isRunning = true;
            for (let retry_num = 0; retry_num != props.retryLimits; retry_num++) { // Corrected loop variable
                await patch_NR_Completed(props.jobId);
                Log_ROUTINE_warn(`Reloading app in 5 seconds to follow restart request!`);
                await sleep(5 * 1000);
                Log_ROUTINE_warn(`Restarting player now!`);
                handleReset();
                break;
            }
            isRunning = false;
        }
    } catch (error) {
        Log_ROUTINE_error(`Reloading app failed due to ${error}!`);
        await patch_NR_Failed(props.jobId);
        isRunning = false; // Ensure isRunning is reset in case of error
    }
}