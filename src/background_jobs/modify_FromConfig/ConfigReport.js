import { API_errorResponse } from '../../util/helper/Error_Response.js';
import { Log_API_error, Log_API_info } from '../../util/helper/Log_shortcut.js';
import { getFrom_LocalData_LessLogging } from '../../util/stats/info/Secured_Machine_Info.js';
import { sleep } from '../../util/helper/Sleep.js';

const domain = "https://vplay.365vending.net"
const sleep_time = 3000;

export async function ConfigReport(mediaId, attempts = 3) {
    async function call(attempts) {
        try {
            const pmid = await getFrom_LocalData_LessLogging("pmID");
            const accessToken = await getFrom_LocalData_LessLogging("accessToken");

            ///playerDB/playerinfos/pmId/:pmId/report-media-downloaded/:mediaId
            const resource_location = `playerDB/playerinfos/pmId/${pmid}/report-media-downloaded/${mediaId}`;

            const response = await fetch(`${domain}/databaseAPI/${resource_location}`, {
                method: 'PATCH',
                headers: {
                    'Origin': 'androidApp',
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${accessToken}`
                },
            });

            if (response.status > 299) {
                await API_errorResponse(response);
            } else {
                Log_API_info(`Running post_ConfigReport without problem! (status: ${response.status})`);
                return true;
            }
        } catch (err) {
            Log_API_error(`Request for post_ConfigReport encountered some problems due to ${err}!`);
            Log_API_warn(`Wait for ${sleep_time / 1000} seconds until the next retry (current retry attempt: ${attempts})!`);
            sleep(sleep_time)
            if (attempts == 0) {
                return undefined;
            }
            else {
                return await call(attempts - 1);
            }
        }
    }
    return await call(attempts)
}