import lodash from 'lodash';
//import { setIsLoading } from "../../../src/components/player/Media_player_new";
import { triggerMedia_Reload_new } from "../../components/player/Media_player_new";
import { triggerCC_Reload_new } from "../../components/player/RSS_player_new";
import { Log_MACHINE_info, Log_MACHINE_warn } from "../../util/helper/Log_shortcut";
import { getFrom_LocalData_AsyncStorage, storeAs_LocalData_AsyncStorage } from "../../util/stats/info/Async_Machine_Info";
import { reDownloadItem } from "../download/DownloadMedia_Content";

export async function Modify_playlist(curPlaying_playlist, empty_But_newConfig) {


  let isChanged = false;
  let changes = await getFrom_LocalData_AsyncStorage('playlist_config')

  if (changes === null || changes === undefined) {
    Log_MACHINE_warn(`There is no data store in playlist_config because it is ${changes}!`)
    return
  }
  else if (curPlaying_playlist == [] || empty_But_newConfig == true) {
    isChanged = true
    Log_MACHINE_warn("The current media playlist stack is empty!")
    //setIsLoading(true)
    await reDownload_SpecificItem_Fromjson(changes.content, [])
    Log_MACHINE_info("Redownload new items in the playlist based on the new configuration's order!")

  }
  else {
    if (checkDifferent_length(changes.content, curPlaying_playlist[curPlaying_playlist.length - 1].newPlaylistMedia.content) == true) {
      isChanged = true
      await reDownload_SpecificItem_Fromjson(changes.content, curPlaying_playlist[curPlaying_playlist.length - 1].newPlaylistMedia.content)
      Log_MACHINE_info("Redownload new items in the playlist based on the new configuration's order!")
    }

    if (checkDifferent_Order(changes.content, curPlaying_playlist[curPlaying_playlist.length - 1].newPlaylistMedia.content) == true
      || checkDiffernt_Detail_Playlist(changes.content, curPlaying_playlist[curPlaying_playlist.length - 1].newPlaylistMedia.content) == true
      || checkDiffernt_playMode(changes, curPlaying_playlist[curPlaying_playlist.length - 1].newPlaylistMedia) == true) {
      isChanged = true
    }

  }

  if (isChanged == true) {
    Log_MACHINE_info("Switching to playing the media playlist according to the latest configuration!")

    if (empty_But_newConfig == false) {
      let copy = lodash.cloneDeep(curPlaying_playlist)
      copy[copy.length - 1].newPlaylistMedia = changes
      await storeAs_LocalData_AsyncStorage("playlist_media_ArrList", copy)
    }
    else if (empty_But_newConfig == true) {
      let copy = [{
        newPlaylistMedia: changes
      }]
      await storeAs_LocalData_AsyncStorage("playlist_media_ArrList", copy)
      await storeAs_LocalData_AsyncStorage("Last_NonPlayOnce_playlist", {
        newPlaylistMedia: changes
      })
    }
    Log_MACHINE_info("Refreshing the Media_player!")
    triggerMedia_Reload_new()
  }
  else {
    Log_MACHINE_info("Not refreshing the Media_player due to no change in media playlist!")
  }
}

export async function Modify_CC(curPlaying_CC, empty_But_newConfig) {
  let isChanged = false;
  let changes = await getFrom_LocalData_AsyncStorage('CC_config')

  if (changes === null || changes === undefined) {

    Log_MACHINE_warn(`There is no data store in CC_config because it is ${changes}!`)
    return
  }
  else if (curPlaying_CC == [] || empty_But_newConfig == true) {

    isChanged = true
    Log_MACHINE_info("The current RSS playlist stack is empty!")

  }
  else {

    if (checkDifferent_Order_CC(changes.content, curPlaying_CC[curPlaying_CC.length - 1].newCCMedia.content) == true
      || checkDiffernt_Detail_CC_speed(changes, curPlaying_CC[curPlaying_CC.length - 1].newCCMedia) == true
      || checkDiffernt_Detail_CC_playMode(changes, curPlaying_CC[curPlaying_CC.length - 1].newCCMedia) == true
      || checkDifferent_length(changes.content, curPlaying_CC[curPlaying_CC.length - 1].newCCMedia.content) == true) {
      isChanged = true
    }

  }


  if (isChanged == true) {
    Log_MACHINE_info("Switching to playing the RSS playlist according to the latest configuration!")

    if (empty_But_newConfig == false) {
      let copy = lodash.cloneDeep(curPlaying_CC)
      copy[copy.length - 1].newCCMedia = changes
      await storeAs_LocalData_AsyncStorage("CC_media_ArrList", copy)
    }
    else if (empty_But_newConfig == true) {
      let copy = [{
        newCCMedia: changes
      }]
      await storeAs_LocalData_AsyncStorage("CC_media_ArrList", copy)
      await storeAs_LocalData_AsyncStorage("Last_NonPlayOnce_CC", {
        newCCMedia: changes
      })
    }
    Log_MACHINE_info("Refreshing the RSS_player!")
    triggerCC_Reload_new()
  }
  else {
    Log_MACHINE_info("Not refreshing the RSS_player due to no changes in RSS playlist!")
  }
}

function checkDifferent_length(newConfig, oriConfig) {
  if (newConfig.length !== oriConfig.length) {
    Log_MACHINE_info("There is a difference in the number of items between the original configuration and the new configuration's order!")
    return true
  }
  else {
    return false
  }

}

function checkDifferent_Order(configJson, oriJson) {

  if (configJson.length !== oriJson.length) {
    return true
  }


  for (let cnt_changes = 0; cnt_changes != configJson.length; cnt_changes++) {
    if (configJson[cnt_changes].uploadedMedia.mediaId !== oriJson[cnt_changes].uploadedMedia.mediaId) {
      return true
    }
  }
  return false
}

function checkDiffernt_Detail_Playlist(configJson, oriJson) {

  for (let cnt_config = 0; cnt_config != configJson.length; cnt_config++) {
    switch (configJson[cnt_config].uploadedMedia.mediaType) {
      case "slide":
        //"enAnime":"FlipInEasyY","displayTime":10
        if (configJson[cnt_config].enAnime != oriJson[cnt_config].enAnime) {
          return true
        }
        if (configJson[cnt_config].displayTime != oriJson[cnt_config].displayTime) {
          return true
        }
        break;
      case "video":
        if (configJson[cnt_config].enAnime != oriJson[cnt_config].enAnime) {
          return true
        }
        if (configJson[cnt_config].stopPt != oriJson[cnt_config].stopPt) {
          return true
        }
        if (configJson[cnt_config].startPt != oriJson[cnt_config].startPt) {
          return true
        }
        if (configJson[cnt_config].duration != oriJson[cnt_config].duration) {
          return true
        }
        break;
    }

  }
  return false
}

function checkDiffernt_playMode(configJson, oriJson) {

  if (configJson.playMode != oriJson.playMode) {
    return true
  }
  else {
    return false
  }
}

function checkDiffernt_Detail_CC_playMode(configJson, oriJson) {

  if (configJson.playMode != oriJson.playMode) {
    return true
  }

  else {
    return false
  }
}

function checkDiffernt_Detail_CC_speed(configJson, oriJson) {

  if (configJson.speed != oriJson.speed) {
    return true
  }
  else {
    return false
  }
}


function checkDifferent_Order_CC(configJson, oriJson) {



  for (let cnt_changes = 0; cnt_changes != configJson.length; cnt_changes++) {
    if (configJson[cnt_changes].rssId !== oriJson[cnt_changes].rssId) {
      return true
    }
  }
  return false
}




async function reDownload_SpecificItem_Fromjson(newConfig, oriConfig) {
  const targetArr_forReDL = []
  const id_arr = []

  for (let id_num = 0; id_num != oriConfig.length; id_num++) {
    id_arr.push(oriConfig[id_num].uploadedMedia.mediaId)
  }
  
  for (let reDL_item = 0; reDL_item != newConfig.length; reDL_item++) {
    if (id_arr.includes(newConfig[reDL_item].uploadedMedia.mediaId) == false) {
      let id = newConfig[reDL_item].uploadedMedia.mediaId

      let filename = id + "." + newConfig[reDL_item].uploadedMedia.fileName.split('.').pop()
      targetArr_forReDL.push([id, filename]);

    }
  }



  for (let download_item = 0; download_item != targetArr_forReDL.length; download_item++) {
    await reDownloadItem(targetArr_forReDL[download_item])
  }


}
