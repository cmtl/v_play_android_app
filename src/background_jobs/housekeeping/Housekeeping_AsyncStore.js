import { Log_MACHINE_error, Log_MACHINE_info, Log_MACHINE_warn } from '../../util/helper/Log_shortcut';
import { deletetFrom_LocalData_AsyncStorage } from '../../util/stats/info/Async_Machine_Info';


export async function Housekeeping_AsyncStore() {
    const limit = 5

    Log_MACHINE_info('Start checking for over-length AsyncStore data to clear!')


  //  let get_CompletePlaylist_Reported = await getFrom_LocalData_AsyncStorage('CompletePlaylist_Reported')
  //  let get_CompleteCC_Reported = await getFrom_LocalData_AsyncStorage('CompleteCC_Reported')

//    await Del_AsyncStore(get_CompletePlaylist_Reported, limit, "CompletePlaylist_Reported")
  //  await Del_AsyncStore(get_CompleteCC_Reported, limit, "CompleteCC_Reported")


 //   Log_MACHINE_info('Checking for over-length AsyncStore data completed!')



}

async function Del_AsyncStore(val, limit, str) {

    try {

        if (val === undefined || val === null) {
            Log_MACHINE_info(`AsyncStore data ${str} is ${val} so there is no need to clear!`)

        }
        else if (val.length > limit) {
            //let latest_item = val.pop()
            Log_MACHINE_warn(`AsyncStore data ${str} is longer than the set length limit of ${limit}! Clearing the data!`)
            await deletetFrom_LocalData_AsyncStorage(str)


        }

        else {
            Log_MACHINE_info(`AsyncStore data ${str} is shorter than the set length limit of ${limit}!`)

            Log_MACHINE_info(`AsyncStore data ${str} was not cleared!`)
        }
    }
    catch (error) {
        Log_MACHINE_error(`Something wrong happened while clearing AsyncStore data ${str} due to ${error}!`)

    }


}

