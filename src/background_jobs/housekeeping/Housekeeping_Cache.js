import * as FileSystem from 'expo-file-system'
//import { parse_Dir } from '../../util/helper/get_Directory';
import { Log_MACHINE_debug, Log_MACHINE_warn, Log_MACHINE_info, Log_MACHINE_error } from '../../util/helper/Log_shortcut';

export async function Housekeeping_Cache() {
  const day_to_delete = 1;
  const now = Date.now()

  const time_before_delete = (now - (day_to_delete * 24 * 60 * 60 * 1000)) // delete the item if it is modified more than one day ago
  const allowed_file_num = 10;

  let cnt = 0;

  //console.log(new Date(time_before_delete))
  try {
    const cacheDirectory = await FileSystem.readDirectoryAsync(FileSystem.cacheDirectory);
    if (cacheDirectory.length <= allowed_file_num) {
      Log_MACHINE_info(`There are ${allowed_file_num} or fewer files in the cacheDirectory. No file will be deleted!`);
      return;
    }
    else {

      Log_MACHINE_warn(`There are more than ${allowed_file_num} files in the cacheDirectory! Proceeding to check the cacheDirectory!`);
      Log_MACHINE_warn(`File(s) in the cacheDirectory will be deleted if it has not been modified for ${day_to_delete} day(s)!`);
      const files = await FileSystem.readDirectoryAsync(FileSystem.cacheDirectory);

      await Promise.all(
        files.map(async (file) => {
          const info = await FileSystem.getInfoAsync(FileSystem.cacheDirectory + file);
          const fileName = (info.uri).split('/').pop();

          if (info.modificationTime < time_before_delete) {
            Log_MACHINE_warn(`${fileName} was not modified for ${day_to_delete} day(s)! Deleting this file in the cacheDirectory!`);
            await FileSystem.deleteAsync(FileSystem.cacheDirectory + file, { idempotent: true });
            Log_MACHINE_warn(`File ${fileName} deleted!`);
            cnt++
          }
        })
      );
      Log_MACHINE_info(`Deleted a total of ${cnt} file(s) from cacheDirectory!`);
    }
  }
  catch (error) {
    console.error('Error while cleaning cache:', error);
  }
}


