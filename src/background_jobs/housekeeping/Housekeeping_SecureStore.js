import { getFrom_localData, deletetFrom_localData } from '../../util/stats/info/Machine_Info';
import { Log_MACHINE_debug, Log_MACHINE_warn, Log_MACHINE_info, Log_MACHINE_error } from '../../util/helper/Log_shortcut';


export async function Housekeeping_SecureStore() {
    let del = false;
    const limit = 14

    Log_MACHINE_info('Start checking for over-length SecureStore data to clear!')
    //let playlist = await getFrom_localData('playlist_media')
    //let CC = await getFrom_localData('CC_media')

    let get_CompletePlaylist_Reported = await getFrom_localData('CompletePlaylist_Reported')
    let get_playOncePlayList = await getFrom_localData('PlayOnce_playlist')
    let get_CompleteCC_Reported = await getFrom_localData('CompleteCC_Reported')
    let get_playOnceCC = await getFrom_localData('PlayOnce_CC')

    await Del_SecureStore(get_CompletePlaylist_Reported, limit, "CompletePlaylist_Reported")
    await Del_SecureStore(get_playOncePlayList, limit, "PlayOnce_playlist")
    await Del_SecureStore(get_CompleteCC_Reported, limit, "CompleteCC_Reported")
    await Del_SecureStore(get_playOnceCC, limit, "PlayOnce_CC")
    

    Log_MACHINE_info('Checking for over-length SecureStore data completed!')



}

async function Del_SecureStore(val, limit, str) {
    try {
        if (val === undefined || val === null) {
            Log_MACHINE_info(`SecureStore data ${str} is ${val} so there is no need to clear!`)

        }
        else if (val.length > limit) {

            Log_MACHINE_warn(`SecureStore data ${str} is longer than the set length limit of ${limit}! Clearing PlayOnce_playlist!`)
            deletetFrom_localData(str)
            Log_MACHINE_warn(`SecureStore data ${str} cleared!`)
        }
       
        else {
            Log_MACHINE_info(`SecureStore data ${str} is shorter than the set length limit of ${limit}!`)

            Log_MACHINE_info(`SecureStore data ${str} was not cleared!`)
        }
    }
    catch (error) {
        Log_MACHINE_error(`Something wrong happened while clearing SecureStore data ${str} due to ${error}!`)

    }


}

