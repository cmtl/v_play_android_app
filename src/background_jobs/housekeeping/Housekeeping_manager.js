import { Log_MACHINE_info } from '../../util/helper/Log_shortcut';
import { Housekeeping_AsyncStore } from "./Housekeeping_AsyncStore";
import { Housekeeping_Cache } from "./Housekeeping_Cache";
import { Housekeeping_Log } from "./Housekeeping_Log";
export async function call_Housekeeping_Manager_onStartUp() {
    Log_MACHINE_info(`Start housingkeeping on start up!`)
    await Housekeeping_Log()
    await Housekeeping_Cache()
    await Housekeeping_AsyncStore()
    Log_MACHINE_info(`Start up housingkeeping ended!`)
}

export async function call_Housekeeping_Manager_onPeriod() {
    Log_MACHINE_info(`Start housingkeeping on period!`)
    //await Housekeeping_Log()
    await Housekeeping_Cache()
    //await Housekeeping_AsyncStore()
    Log_MACHINE_info(`Periodic housingkeeping ended!`)
}