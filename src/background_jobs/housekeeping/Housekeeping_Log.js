import * as FileSystem from 'expo-file-system'
import { Log_MACHINE_debug, Log_MACHINE_warn, Log_MACHINE_info, Log_MACHINE_error } from '../../util/helper/Log_shortcut';

export async function show_Logs() {
    const dirUri = FileSystem.documentDirectory;

    const dirInfo = await FileSystem.readDirectoryAsync(dirUri);
    for (const file of dirInfo) {
        console.log(file)
    }
}

export async function Housekeeping_Log() {
    let del = false;

    const now = Date.now()

    let fileUri;
    try {
        const dirInfo = await FileSystem.readDirectoryAsync(FileSystem.documentDirectory);
        Log_MACHINE_info(`Start checking for old logs to delete!`)
        for (const file of dirInfo) {
            if (file.startsWith('log_')) {

                let parts = file.split('_')[2].split('.')[0].split('-')

                let time = new Date(parts[2], parts[1] - 1, parts[0]);
                let timestamp = time.getTime()

                let age = Math.floor((now - timestamp) / (1000 * 60 * 60 * 24));
                fileUri = `${FileSystem.documentDirectory}${file}`;
                

                /**
                 * console.log(`===========log name: ${file}===========`)
                console.log(`now: ` + now)
                console.log(`time ` + time)
                console.log(`timestamp: ` + timestamp)
                console.log(`age: ` + age)
                console.log(`fileUri: ` + fileUri)

                console.log(`===========log name: ${file}===========`)
                 */
                
                if (age > 14) {
                    del = true
                    Log_MACHINE_warn(`${file} was created more than 14 days ago! Deleting this log!`)
                    await FileSystem.deleteAsync(fileUri, { idempotent: true });
                    Log_MACHINE_warn(`Log ${file} deleted!`)
                }
            }
        }
        if (del == true) {
            Log_MACHINE_info(`Finish deleting log(s)!`)
        }
        else {
            Log_MACHINE_info(`No log was deleted!`)
        }
    }
    catch (error) {
        Log_MACHINE_error(`Something wrong happened while deleting log(s) from documentDirectory due to ${error}!`)

    }

}