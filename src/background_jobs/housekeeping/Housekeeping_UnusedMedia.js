
import * as FileSystem from 'expo-file-system';
import { Log_MEDIA_error, Log_MEDIA_info, Log_MEDIA_warn } from '../../util/helper/Log_shortcut';


export async function Housekeeping_UnusedMedia(download_job) {
    let del = false;
    let total_neededList = []
    let total_curList = []
    let total_delList = []
    try{
        const dirInfo = await FileSystem.readDirectoryAsync(FileSystem.documentDirectory);
        Log_MEDIA_info(`Start checking for unused media to delete!`)
    
        for (let needed_playlist_num = 0; needed_playlist_num != download_job.length; needed_playlist_num++) {
            
            for (let needed_media_num = 0; needed_media_num != download_job[needed_playlist_num].newPlaylistMedia.content.length; needed_media_num++) {
                let fileMediaID = download_job[needed_playlist_num].newPlaylistMedia.content[needed_media_num].uploadedMedia.mediaId
                let fileExtend = download_job[needed_playlist_num].newPlaylistMedia.content[needed_media_num].uploadedMedia.fileName.split('.').pop()
                let file_LocalName = fileMediaID + '.' + fileExtend
                total_neededList.push(file_LocalName)
            }
        }
        Log_MEDIA_info(`The media needed in the coming playlist: [${total_neededList}]`)
        for (const file of dirInfo) {
            if (file.includes('.mp4') || file.includes('.png')|| file.includes('.jpg') || file.includes('.jpeg')){
                total_curList.push(file)
            }
        }
        Log_MEDIA_info(`The media needed currently in the device: [${total_curList}]`)
    
        total_delList = total_curList.filter((item) => !total_neededList.includes(item));
        if (total_delList.length != 0) {
            Log_MEDIA_warn(`Found unused media from the device! Unused media: [${total_delList}]`)
            del = true
            for (let del_num = 0 ; del_num != total_delList.length;del_num++) {
                let fileUri = `${FileSystem.documentDirectory}/${total_delList[del_num]}`
                Log_MEDIA_warn(`Media ${total_delList[del_num]} is not used in the entire media playing job that the device currently has! Deleting this media!`)
                await FileSystem.deleteAsync(fileUri, { idempotent: true })
                Log_MEDIA_warn(`Media ${total_delList[del_num]} deleted!`)
            }
        }
        
        if (del == true) {
            Log_MEDIA_info(`Finish deleting unused media!`)
        }
        else {
            Log_MEDIA_info(`No media was deleted!`)
        }
    }
    catch (error) {
        Log_MEDIA_error(`Something wrong happened while deleting unused media from documentDirectory due to ${error}!`)

    }
}