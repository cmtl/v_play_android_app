import { Log_ALERT_debug, Log_ALERT_warn, Log_ALERT_error, Log_ALERT_info } from "../../util/helper/Log_shortcut";
import { getFrom_LocalData } from "../../util/stats/info/Secured_Machine_Info";
import { ALERT_errorResponse } from "../../util/helper/Error_Response";
import moment from "moment";
import { getFrom_LocalData_LessLogging } from "../../util/stats/info/Secured_Machine_Info";

const domain = "https://vplay.365vending.net"

export async function post_Abnormal_Alert(string, type, attempts = 3) {
    async function call(attempts) {
        try {
            const pmid = await getFrom_LocalData_LessLogging("pmID");
            const accessToken = await getFrom_LocalData_LessLogging("accessToken");

            let data = {
                alertType: type,
                description: string,
                alertAt: moment().utc()
            };

            const resource_location = `playerDB/report/alert/pmId/${pmid}`;

            const response = await fetch(`${domain}/databaseAPI/${resource_location}`, {
                method: 'POST',
                headers: {
                    'Origin': 'androidApp',
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${accessToken}`
                },
                body: JSON.stringify(data)
            });

            if (response.status > 299) {
                Log_ALERT_error(`Request for post_Abnormal_alert encountered abnormal status code of ${response.status}!`);

                return await ALERT_errorResponse(response);
            } else {
                Log_ALERT_warn(`Running post_Abnormal_alert (type: ${type}) without problem! (status: ${response.status})`);
                Log_ALERT_warn(`Reported message: ${string}`);
                return true;
            }
        } catch (err) {
            Log_ALERT_error(`Request for post_Abnormal_alert (type: ${type}) encountered some problems due to ${err}!`);
            Log_ALERT_error(`Wait for ${sleep_time / 1000} seconds until the next retry (current retry attempt: ${attempts})!`);
            sleep(sleep_time)
            if (attempts <= 0) {
                Log_ALERT_error(`Request for post_Abnormal_alert (type: ${type}) failed due to ${err}!`);

                return undefined;
            }
            else {
                return await call(attempts - 1);
            }
        }
    }
    return await call(attempts)
}
