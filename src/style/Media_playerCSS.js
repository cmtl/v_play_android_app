
import { StyleSheet } from 'react-native';

/**
 * Author: Ken Lung, Time 03/04/2024
 * Small styleSheet for setting the styles of the elements in the View in Media_player_new.js
 */
export const styles = StyleSheet.create({
    container: {
        height: '100%',
        justifyContent: 'center',
        flex: 30,
        alignItems: 'center',
        backgroundColor:'black'

    },
    slide_pic_active: {
        contentFit: 'contain',
        flex: 20,
        width: '100%',
        height: '100%',
    },
    slide_pic_dummy: {
        contentFit: 'contain',
        flex: 20,
        width: '100%',
        height: '100%',
        backgroundColor: "black"
    },
    slide_container: {
        flex: 1,
        height: '100%',
        width: '100%'
    },
    time_bar_container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%',
        width: '100%',
    },

    video_clip_active: {
        resizeMode: 'contain',
        flex: 20,
        width: '100%',
        height: '100%',
        backgroundColor: 'black'
    },
    video_container: {
        flex: 1,
        height: '100%',
        width: '100%'
    }
})
