import { StyleSheet } from 'react-native';

/**
 * Author: Ken Lung, Time 03/04/2024
 * Small styleSheet for setting the styles of the elements in the View in Date_timer.js
 */
export const styles = StyleSheet.create({
    date: {
        textAlign: 'left',
        fontSize: 22,
        paddingHorizontal: '2%',
        color: 'white',
        textShadowColor: 'black',
        textShadowOffset: { width: -1, height: 1 },
        textShadowRadius: 10,
    },
    time: {
        textAlign: 'right',
        fontSize: 22,
        paddingHorizontal: '2%',
        color: 'white',
        textShadowColor: 'black',
        textShadowOffset: { width: -1, height: 1 },
        textShadowRadius: 10,
    },
    time_container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        textAlignVertical: 'bottom',
        flex: 1,
        
    },

    abs: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
    },
});
