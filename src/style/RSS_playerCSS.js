
import { StyleSheet } from 'react-native';

/**
 * Author: Ken Lung, Time 03/04/2024
 * Small styleSheet for setting the styles of the elements in the View in RSS_player_new.js
 */
export const styles = StyleSheet.create({
    container: {
        height: '100%',
        width: '100%',
        justifyContent: 'center',
        backgroundColor: 'blue',
        flex: 1,
        display: 'flex'
    },
    rss: {
        fontSize: 15,
        textAlign: 'right',
        overflow: 'hidden',
        color: 'white',
        fontWeight: 'bold'
    },
})