import { Dimensions, StyleSheet } from 'react-native';
/**
 * Author: Ken Lung, Time 03/04/2024
 * A big styleSheet for setting the styles of the elements in the View in App.js
 * Noted that only the elements within the app are inverted 
 * Keyboard, home buttons, and any other visual elements OUTSIDE of the application remains the same.
 * 
 */

const { height , width } = Dimensions.get('screen')

export const inverted_style = StyleSheet.create({ //inverted style: for catering the requirement for inverting the screen 

    container: {
      transform: [
        { rotateX: '180deg' },
        { rotateY: '180deg' }]
    }
  });
  export const normal_style = StyleSheet.create({ //normal style: the default style of the elements in the View in App.js
    container: {
      transform: [
        { rotateX: '0deg' },
        { rotateY: '0deg' }]
    }
  });
  
  export const horizontal_styles = StyleSheet.create({ //horizontal style: the style of the elements in the View in App.js in landscape
    container: {
      flex: 1,
      backgroundColor: 'white',
      alignItems: 'center',
    },
  
    media_container: {
      flexDirection: 'column',
      height: '100%',
      width: '100%',
      flex: 20,
    },
    header: {
      backgroundColor: 'transparent',
      position: 'absolute',
      zIndex: 50,
      width: '100%',
      flex: 2,
      width: '100%'
    },
    footer: {
      flex: 1,
      alignItems: 'center',
      textAlign: 'center'
    },
  
    logo: {
      resizeMode: 'contain',
      borderColor: 'transparent',
      borderWidth: 5,
      width: '100%',
      height: '80%',
      flex: 6,
  
    },
  
    input_container: {
      position: 'absolute',
      justifyContent: 'center',
      alignItems: 'center',
      top: '40%',
      left: '0%',
      padding: '5%',
      borderColor: 'black',
      borderWidth: 5,
      flex: 3,
      width: '100%',
      backgroundColor: 'white'
    },
    input_element: {
      flexDirection: 'row',
    },
  
    input: {
      borderColor: 'black',
      borderWidth: 2,
      height: '80%',
      width: '50%',
      paddingHorizontal: "1%"
    },
    title: {
      fontSize: 30,
      fontWeight: "bold",
    },
    buttons: {
      flexDirection: 'row',
    },
    button: {
      marginHorizontal: '10%'
    },
    invisibleButton: {
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      height: 50,
      opacity: 0,
      zIndex: 100
    },
    rss: {
      flex: 2,
      display: 'flex'
    }
  });
  
  export const vertical_styles = StyleSheet.create({ //vertical style: the style of the elements in the View in App.js in portrait 
    container: {
      flex: 1,
      backgroundColor: 'white',
      alignItems: 'center',
    },
  
    media_container: {
      flexDirection: 'column',
      height: '100%',
      width: '100%',
      flex: 20,
    },
    header: {
      backgroundColor: 'transparent',
      position: 'absolute',
      zIndex: 50,
      flex: 1,
      width: '100%'
    },
    footer: {
      flex: 1,
      alignItems: 'center',
      textAlign: 'center'
    },
    logo: {
      resizeMode: 'contain',
      borderColor: 'transparent',
      borderWidth: 5,
      width: '100%',
      height: '80%',
      flex: 6,
  
    },
    input_container: {
      position: 'absolute',
      justifyContent: 'center',
      alignItems: 'center',
      top: '45%',
      left: '0%',
      padding: '10%',
      borderColor: 'black',
      borderWidth: 5,
      flex: 3,
      width: '100%',
      backgroundColor: 'white'
    },
    input_element: {
      flexDirection: 'row',
    },
  
    input: {
      borderColor: 'black',
      borderWidth: 2,
      height: '80%',
      width: '50%',
      paddingHorizontal: "1%"
    },
    title: {
      fontSize: 30,
      fontWeight: "bold",
    },
    buttons: {
      flexDirection: 'row',
    },
    button: {
      marginHorizontal: '10%'
    },
    invisibleButton: {
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      height: 50,
      
      opacity: 0,
      zIndex: 100
    },
    rss: {
      flex: 1,
      display: 'flex'
    }
  });