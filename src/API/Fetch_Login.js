import * as Application from 'expo-application';
import { showToast } from "../background_jobs/notification/Toast_notification.js";
import { Login_errorResponse } from "../util/helper/Error_Response.js";
import { Log_API_error, Log_API_info } from "../util/helper/Log_shortcut.js";
import { sleep } from '../util/helper/Sleep.js';
import { Saving_Activated_Info, Saving_Login_Info } from '../util/stats/info/Info_Bundle.js';

const domain = "https://vplay.365vending.net"
const sleep_time = 3000


export async function patch_Activation(key_input, id_input, attempts = 3) {
    async function call(attempts) {
        try {
            const data = {
                activationKey: key_input,
                deviceId: id_input
            };

            const resource_location = 'authen/activate-pm';

            const response = await fetch(`${domain}/databaseAPI/${resource_location}`, {
                method: 'PATCH',
                headers: {
                    'Origin': 'androidApp',
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify(data),


            });

            if (response.status > 299) {
                Log_API_error(`Request for patch_Activation encountered abnormal status code of ${response.status}!`);

                await Login_errorResponse(response);
            } else {
                Log_API_info(`Running patch_Activation without problem! (status: ${response.status})`);
                const json = await response.json();

                if (json != undefined) {
                    if (json != false) {
                        const pmID = json.pmId;
                        const groupID = json.groupId;
                        await Saving_Activated_Info(pmID, groupID, key_input, Application.getAndroidId());
                        Log_API_info(`Successful activation!`);
                        showToast(`Successful activation!`);
                        return true;
                    } else {
                        return true;
                    }
                } else {
                    Log_API_error(`Failed activation!`);
                    showToast(`Failed activation!`);
                    return false;
                }
            }
        } catch (err) {
            Log_API_error(`Request for patch_Activation encountered some problems due to ${err}!`);
            showToast(`Request for patch_Activation encountered some problems due to ${err}!`);
            Log_API_warn(`Wait for ${sleep_time / 1000} seconds until the next retry (current retry attempt: ${attempts})!`);
            sleep(sleep_time)
            if (attempts <= 0) {
                Log_API_error(`Request for patch_Activation failed due to ${err}!`);

                return undefined;
            }
            else {
                return await call(attempts - 1);
            }
        }
    }
    return await call(attempts)
}


export async function patch_Login(key_input, id_input, verison_input, signal_input, attempts = 3) {
    async function call(attempts) {
        try {
            const data = {
                activationKey: key_input,
                deviceId: id_input,
                appVersion: verison_input,
                signalStrength: signal_input
            };

            const resource_location = 'authen/pm-login';

            const response = await fetch(`${domain}/databaseAPI/${resource_location}`, {
                method: 'PATCH',
                headers: {
                    'Origin': 'androidApp',
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify(data),


            });

            if (response.status > 299) {
                Log_API_error(`Request for patch_Login encountered abnormal status code of ${response.status}!`);

                await Login_errorResponse(response);
            } else {
                Log_API_info(`Running patch_Login without problem! (status: ${response.status})`);
                Log_API_info(`Running patch_Login without problem! (status: ${response.status})`);
                const json = await response.json();

                if (json != undefined) {
                    if (json != false) {
                        const pmID = json.pmId;
                        const groupID = json.groupId;
                        const accessToken = json.accessToken;
                        await Saving_Login_Info(pmID, groupID, accessToken);
                        Log_API_info(`Successful Login!`);
                        showToast(`Successful Login!`);
                        return true;
                    } else {
                        return true;
                    }
                } else {
                    Log_API_error(`Failed Login!`);
                    showToast(`Failed Login!`);
                    return false;
                }
            }
        } catch (err) {
            Log_API_error(`Request for patch_Login encountered some problems due to ${err}!`);
            Log_API_warn(`Wait for ${sleep_time / 1000} seconds until the next retry (current retry attempt: ${attempts})!`);
            sleep(sleep_time)
            if (attempts <= 0) {
                Log_API_error(`Request for patch_Login failed due to ${err}!`);

                return undefined;
            }
            else {
                return await call(attempts - 1);
            }
        }
    }
    return await call(attempts)
}