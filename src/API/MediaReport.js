import { API_errorResponse } from "../util/helper/Error_Response.js";
import { Log_API_info } from "../util/helper/Log_shortcut.js";
import { sleep } from '../util/helper/Sleep.js';
import { getFrom_LocalData_LessLogging } from '../util/stats/info/Secured_Machine_Info.js';

const domain = "https://vplay.365vending.net"
const sleep_time = 3000

export async function MediaReport(type, index_input, time_input, attempts = 3) {
    async function call(attempts) {
        try {
            const pmid = await getFrom_LocalData_LessLogging("pmID");
            const accessToken = await getFrom_LocalData_LessLogging("accessToken");

            const data = {
                mediaIndex: index_input,
                playedAt: time_input
            };

            const resource_location = `playerDB/report/media/pmId/${pmid}/${type}`;

            const response = await fetch(`${domain}/databaseAPI/${resource_location}`, {
                method: 'POST',
                headers: {
                    'Origin': 'androidApp',
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${accessToken}`
                },
                body: JSON.stringify(data),
            });

            if (response.status > 299) {
                Log_API_error(`Request for post_MediaReport encountered abnormal status code of ${response.status}!`);

                await API_errorResponse(response);
            } else {
                //Omitted due to possible frequent logging 

                //Log_API_info(`Running post_MediaReport (type: ${type}) without problem! (status: ${response.status})`);
                return true;
            }
        } catch (err) {
            Log_API_info(`Request for post_MediaReport encountered some problems due to ${err}!`);
            Log_API_warn(`Wait for ${sleep_time / 1000} seconds until the next retry (current retry attempt: ${attempts})!`);
            sleep(sleep_time)
            if (attempts <= 0) {
                Log_API_info(`Request for post_MediaReport failed due to ${err}!`);

                return undefined;
            }
            else {
                return await call(attempts - 1);
            }
        }
    }
    return await call(attempts)
}
