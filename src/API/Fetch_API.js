import { Saving_Login_Info } from '../util/stats/info/Info_Bundle.js';
import { getFrom_LocalData, storeAs_LocalData } from '../util/stats/info/Secured_Machine_Info.js';
//import { DownloadPlaylist_Content_dummy, patch_DL_Processing_beforeDL, get_Ready_afterDL } from "../components/storage/DownloadPlaylist_Content_dummy.js";

import { DownloadPlaylist } from "../background_jobs/download/DownloadMedia_Content.js";
import { patch_DL_Processing_beforeDL, patch_DL_Ready_afterDL } from '../background_jobs/download/DownloadMedia_ReportStatus.js';

import moment from 'moment';
import { trigger_updateRoutine } from '../../App.js';
import { process_2B_DLjobs, process_DLedjobs } from '../background_jobs/download/Process_DLjobs.js';
import { Housekeeping_UnusedMedia } from '../background_jobs/housekeeping/Housekeeping_UnusedMedia.js';
import { call_NRoutine_manager } from "../background_jobs/non-routine/NRoutine_manager.js";
import { triggerNewDisplay_date, triggerNewDisplay_time } from '../components/Date_timer.js';
import { triggerMedia_Reload_new, triggerModify_playlist_new, triggerReportMedia_new, } from '../components/player/Media_player_new.js';
import { triggerCC_Reload_new, triggerModify_CC_new, triggerNewDisplay_RSS } from '../components/player/RSS_player_new.js';
import { API_errorResponse, Login_errorResponse } from "../util/helper/Error_Response.js";
import { Log_API_debug, Log_API_error, Log_API_info, Log_API_warn } from "../util/helper/Log_shortcut.js";
import { sleep } from '../util/helper/Sleep.js';
import { getFrom_LocalData_AsyncStorage_LessLogging, storeAs_LocalData_AsyncStorage, storeAs_LocalData_AsyncStorage_LessLogging } from '../util/stats/info/Async_Machine_Info.js';
import { getFrom_LocalData_LessLogging } from '../util/stats/info/Secured_Machine_Info.js';

/**
 * Author: Ken Lung, Time:  03/04/2024
 * domain is the URL for accessing the backend 
 * Different API would need different directory to call for different resource
 * But all of them has the same domain/common URL 
 */

const domain = "https://vplay.365vending.net"
const sleep_time = 3000

/**
 * Author: Ken Lung, Time:  03/04/2024
 * Every API call uses Fetch and they follow a similar structure
 * Overall pattern:
 * 1st: A constant called resource-location to show the detailed pathway to get the resource 
 * 2nd: Might include a piece of data (usually a JSON) to send as body 
 * 3rd: Most of them will include an accessToken 
 * 4th: A Fetch API call with the following header:
 * headers: {
 *      'Origin': 'androidApp',
 *      'Content-Type': 'application/json',
 *      'Accept': 'application/json'
 *      'Authorization': `Bearer ${accessToken}` <- Most of them need this accessToken 
 *      },
 * 5th: An error handling seciton if the status code is 4XX or 5XX (refer to errorResponse)
 * 6th: A normal operation section if the status code is 2XX
 * 
 * CAUTION: Might require further refactoring on the if-statements
 */



/**
 * Author: Ken Lung, Time:  03/04/2024
 * 
 * If the response is a successful one, it will take 3 variables returned from the backend
 * And store them in the SecureStore with Saving_Login_Info
 * 
 * Although it seems unnecessary to save pmId and groupId over and over again,
 * There might be a possiblility that these two data will change 
 * Not saving them could lead to bugs, so it is better to overwrite them nonetheless
 * 
 * accessToken will change per heartbeat so it has to be overwritten in each heartbeat job 
 * 
 * If somehow the JSON is undefined, the API call will return false 
 * If an error is encountered, the API call will return undefined
 * 
 */

export async function patch_Login_for_Heartbeat(key_input, id_input, verison_input, signal_input, attempts = 3) {
    async function call(attempts) {
        try {
            const data = {
                activationKey: key_input,
                deviceId: id_input,
                appVersion: verison_input,
                signalStrength: signal_input
            };

            const resource_location = 'authen/pm-login';



            const response = await fetch(`${domain}/databaseAPI/${resource_location}`, {
                method: 'PATCH',
                headers: {
                    'Origin': 'androidApp',
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify(data),

            });

            if (response.status > 299) {
                Log_API_error(`Request for patch_Login_for_Heartbeat encountered abnormal status code of ${response.status}!`);

                await Login_errorResponse(response);
            } else {
                Log_API_info(`Running patch_Login_for_Heartbeat without problem! (status: ${response.status})`);
                const json = await response.json();

                if (json != undefined) {
                    if (json != false) {
                        const pmID = json.pmId;
                        const groupID = json.groupId;
                        const accessToken = json.accessToken;
                        await Saving_Login_Info(pmID, groupID, accessToken);
                        Log_API_info(`Successful Heartbeat!`);
                        return true;
                    } else {
                        return true;
                    }
                } else {
                    Log_API_error(`Failed Heartbeat!`);
                    return false;
                }
            }
        } catch (err) {
            Log_API_error(`Request for patch_Login_for_Heartbeat encountered some problems due to ${err}!`);
            Log_API_warn(`Wait for ${sleep_time / 1000} seconds until the next retry (current retry attempt: ${attempts})!`);
            sleep(sleep_time)
            if (attempts <= 0) {
                Log_API_error(`Request for patch_Login_for_Heartbeat failed due to ${err}!`);
                return undefined;
            }
            else {
                return await call(attempts - 1);
            }
        }
    }
    return await call(attempts)

}

/**
 * Author: Ken Lung, Time:  03/04/2024
 * 
 * If the response is a successful one, it will take 1 variables returned from the backend
 * And store it in the AsyncStorage with storeAs_LocalData_AsyncStorage
 * 
 * result is saved as playlist_config or CC_config in SecureStore, depending on type 
 * 
 * The API call will then trigger triggerModify_playlist_new or triggerModify_CC_new 
 * To change the configuration in the currently playing playlist
 * Returning true at the end 
 * 
 * If an error is encountered, the API call will return undefined
 */

export async function get_MediaConfig(type, attempts = 3) {
    Log_API_debug("get_MediaConfig "+type+" starts")
    async function call(attempts) {
        try {
            const pmid = await getFrom_LocalData_LessLogging("pmID");
            const accessToken = await getFrom_LocalData_LessLogging("accessToken");
            const resource_location = `playerDB/playerinfos/pmId/${pmid}/get-media/${type}`;

            const response = await fetch(`${domain}/databaseAPI/${resource_location}`, {
                method: 'GET',
                headers: {
                    'Origin': 'androidApp',
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${accessToken}`
                },
            });

            if (response.status > 299) {
                Log_API_error(`Request for get_MediaConfig encountered abnormal status code of ${response.status}!`);

                return API_errorResponse(response);
            } else {
                Log_API_info(`Running get_MediaConfig (type:${type}) without problem! (status: ${response.status})`);


                if (response.status == 204) {
                    Log_API_info(`Did not receive new media configuration (type: ${type}), keeping the old one!`);

                    return true;
                }

                else {
                    const json = await response.json();

                    if (json != undefined) {
                        if (json != null) {
                            const media_config = json.result;
                            await storeAs_LocalData_AsyncStorage(`${type}_config`, media_config);
                            Log_API_info(`Saved new media configuration related to ${type}!`);
                            if (type == 'playlist') {
                                triggerModify_playlist_new();
                            } else if (type == 'CC') {
                                triggerModify_CC_new();
                            }
                            return true;
                        }
                    }
                }
            }
        } catch (err) {
            Log_API_error(`Request for get_MediaConfig (type: ${type}) encountered some problems due to ${err}!`);
            Log_API_warn(`Wait for ${sleep_time / 1000} seconds until the next retry (current retry attempt: ${attempts})!`);
            sleep(sleep_time)
            if (attempts <= 0) {
                Log_API_error(`Request for get_MediaConfig (type: ${type}) failed due to ${err}!`);
                return undefined;
            }
            else {
                return await call(attempts - 1);
            }
        }
    }
    return await call(attempts)
}

/**
 * Author: Ken Lung, Time:  03/04/2024
 * 
 * If the response is a successful one, it will take 1 variables returned from the backend
 * And store it in the SecureStore with storeAs_LocalData
 * 
 * result is saved as address in SecureStore
 * Returning true at the end 
 * 
 * If an error is encountered, the API call will return undefined
 */

export async function get_Address(attempts = 3) {
    async function call(attempts) {
        try {
            const pmid = await getFrom_LocalData_LessLogging("pmID");
            const accessToken = await getFrom_LocalData_LessLogging("accessToken");
            const resource_location = `playerDB/playerinfos/pmId/${pmid}/get-address`;

            const response = await fetch(`${domain}/databaseAPI/${resource_location}`, {
                method: 'GET',
                headers: {
                    'Origin': 'androidApp',
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${accessToken}`
                },
            });

            if (response.status > 299) {
                Log_API_error(`Request for get_Address encountered abnormal status code of ${response.status}!`);

                return API_errorResponse(response);
            } else {
                if (response.status == 204) {
                    Log_API_info(`Did not receive new address, keeping the old one!`);
                    return true;
                }
                else {
                    Log_API_info(`Running get_Address without problem! (status: ${response.status})`);
                    const json = await response.json();

                    if (json != undefined) {
                        if (json != false) {
                            const address_content = json.result;
                            await storeAs_LocalData("address", address_content);
                            Log_API_info(`Saved new address!`);
                            return true;
                        }
                    }
                }
            }
        } catch (err) {
            Log_API_error(`Request for get_Address encountered some problems due to ${err}!`);
            Log_API_warn(`Wait for ${sleep_time / 1000} seconds until the next retry (current retry attempt: ${attempts})!`);
            sleep(sleep_time)
            if (attempts <= 0) {
                Log_API_error(`Request for get_Address failed due to ${err}!`);
                return undefined;
            }
            else {
                return await call(attempts - 1);
            }
        }
    }
    return await call(attempts)
}

/**
 * Author: Ken Lung, Time:  03/04/2024
 * 
 * If the response is a successful one, it will take 1 variables returned from the backend
 * And store it in the SecureStore with storeAs_LocalData
 * 
 * result is saved as CurMediaReport_Needed in SecureStore
 * Returning true at the end 
 * 
 * The API call will then trigger triggerReportMedia_new 
 * If the currently saved value is different to the received one
 * triggerReportMedia_new will change the setting concerning reporting media when it starts playing 
 * If the current value and the received value are the same,
 * Then no storing action will be taken
 * 
 * If an error is encountered, the API call will return undefined
 */

export async function get_isMediaReport_Needed(attempts = 3) {
    Log_API_debug("get_isMediaReport_Needed starts")
    async function call(attempts) {
        try {
            const pmid = await getFrom_LocalData_LessLogging("pmID");
            const accessToken = await getFrom_LocalData_LessLogging("accessToken");
            const resource_location = `playerDB/playerinfos/pmId/${pmid}/get-report-media-enabled`;

            const response = await fetch(`${domain}/databaseAPI/${resource_location}`, {
                method: 'GET',
                headers: {
                    'Origin': 'androidApp',
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${accessToken}`
                },
            });

            if (response.status > 299) {
                Log_API_error(`Request for get_isMediaReport_Needed encountered abnormal status code of ${response.status}!`);

                return API_errorResponse(response);
            } else {
                Log_API_info(`Running get_isMediaReport_Needed without problem! (status: ${response.status})`);
                const json = await response.json();

                if (json === null) {
                    Log_API_info(`Did not receive new media report setting! Keeping the old one!`);
                    return true;
                } else {
                    const require_report = json.result;
                    await storeAs_LocalData("CurMediaReport_Needed", require_report);
                    Log_API_info(`Saved new media report setting (current setting: ${require_report})!`);
                    triggerReportMedia_new();
                    return true;
                }
            }
        } catch (err) {
            Log_API_error(`Request for get_isMediaReport_Needed encountered some problems due to ${err}!`);
            Log_API_warn(`Wait for ${sleep_time / 1000} seconds until the next retry (current retry attempt: ${attempts})!`);
            sleep(sleep_time)
            if (attempts <= 0) {
                Log_API_error(`Request for get_isMediaReport_Needed failed due to ${err}!`);
                return undefined;
            }
            else {
                return await call(attempts - 1);
            }
        }
    }
    return await call(attempts)
}

/**
 * Author: Ken Lung, Time:  03/04/2024
 * 
 * If the response is a successful one, it will take 1 variables returned from the backend
 * And store it in the AsyncStore with storeAs_LocalData_AsyncStorage
 * 
 * result is saved as Routine in SecureStore
 * Returning true at the end 
 * 
 * The API call will then save the new Routine
 * 
 * If an error is encountered, the API call will return undefined
 */

export async function get_Routine(attempts = 3) {
    async function call(attempts) {


        try {
            const pmid = await getFrom_LocalData_LessLogging("pmID");
            const accessToken = await getFrom_LocalData_LessLogging("accessToken");
            const resource_location = `playerDB/routineJobs/pmId/${pmid}/get-routine-schedule`;

            const response = await fetch(`${domain}/databaseAPI/${resource_location}`, {
                method: 'GET',
                headers: {
                    'Origin': 'androidApp',
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${accessToken}`
                }
            });

            if (response.status > 299) {
                Log_API_error(`Request for post_MediaReport encountered abnormal status code of ${response.status}!`);

                return API_errorResponse(response);
            } else {
                Log_API_info(`Running post_MediaReport without problem! (status: ${response.status})`);
                const json = await response.json();

                if (json === null) {
                    Log_API_info(`Did not receive new routine schedule! Keeping the old one!`);
                    return true;
                } else if (json != undefined) {
                    const routine = json.result;
                    await storeAs_LocalData_AsyncStorage_LessLogging("Routine", routine);
                    trigger_updateRoutine()
                    Log_API_info(`Successfully saved routine schedule!`);
                    return true;
                }
            }
        } catch (err) {
            Log_API_error(`Request for get_Routine encountered some problems due to ${err}!`);
            Log_API_warn(`Wait for ${sleep_time / 1000} seconds until the next retry (current retry attempt: ${attempts})!`);
            sleep(sleep_time)
            if (attempts <= 0) {
                Log_API_error(`Request for get_Routine failed due to ${err}!`);
                return undefined;
            }
            else {
                return await call(attempts - 1);
            }
        }
    }
    return await call(attempts)
}

/**
 * Author: Ken Lung, Time:  03/04/2024
 * 
 * If the response is a successful one, it will take 1 variables returned from the backend
 * And store it in the AsyncStore with storeAs_LocalData_AsyncStorage
 * 
 * result is saved as NonRoutine in SecureStore
 * Returning true at the end 
 * 
 * The API call will then save the new NonRoutine and trigger call_NRoutine_manager
 * call_NRoutine_manager will check for the non-routine job to do 
 * 
 * If an error is encountered, the API call will return undefined
 * 
 * CAUTION: Supposedly, the non-routine jobs only start
 * After the system clock hits the time store in startAt
 * HOWEVER, it seems that it is not beneficial to start counting when should the tasks be done 
 * When it is supposed to be quite urgent when a non-routine job is made 
 */

export async function get_NonRoutine(attempts = 3) {
    async function call(attempts) {
        try {
            const pmid = await getFrom_LocalData_LessLogging("pmID");
            const accessToken = await getFrom_LocalData_LessLogging("accessToken");
            const resource_location = `playerDB/eventJobs/pmId/${pmid}/get-unprocessed-job`;
            //console.log(accessToken)
            const response = await fetch(`${domain}/databaseAPI/${resource_location}`, {
                method: 'GET',
                headers: {
                    'Origin': 'androidApp',
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${accessToken}`
                }
            });

            if (response.status > 299) {
                Log_API_error(`Request for get_NonRoutine encountered abnormal status code!`);

                return API_errorResponse(response);
            } else {
                Log_API_info(`Running get_NonRoutine without problem! (status: ${response.status})`);
                if (response.status == 204) {

                    Log_API_info(`Did not receive new non-routine schedule! Keeping the old one!`);
                    return true;

                } else {
                    const json = await response.json();
                    if (json != undefined) {
                        const download_job = json.result;
                        await storeAs_LocalData_AsyncStorage(`NonRoutine`, download_job);
                        Log_API_info(`Successfully saved non-routine schedule! Running required task(s) now!`);
                        await call_NRoutine_manager(download_job);
                        return true;
                    }
                }
            }
        } catch (err) {
            Log_API_error(`Request for get_NonRoutine encountered some problems due to ${err}!`);
            Log_API_warn(`Wait for ${sleep_time / 1000} seconds until the next retry (current retry attempt: ${attempts})!`);
            sleep(sleep_time)
            if (attempts <= 0) {
                Log_API_error(`Request for get_NonRoutine failed due to ${err}!`);
                return undefined;
            }
            else {
                return await call(attempts - 1);
            }
        }
    }
    return await call(attempts)
}

/**
 * Author: Ken Lung, Time:  03/04/2024
 * 
 * If the response is a successful one, it will take 1 variables returned from the backend
 * And store it in the AsyncStore with storeAs_LocalData_AsyncStorage
 * 
 * result is saved as NonRoutine in SecureStore
 * Returning true at the end 
 * 
 * The API call will then save the new display_config and call getFrom_LocalData to get the current setting 
 * It will trigger functions and tell responsible component(s) to change the related setting in the if-stateme, attempts = 3nts
 * If the currently saved value is different to the received one
 * 
 * If the current value and the received value are the same,
 * Then no storing action will be taken
 * 
 * If an error is encountered, the API call will return undefined
 * 
 */

export async function get_Display_Config(attempts = 3) {
    async function call(attempts) {
        try {
            const groupid = await getFrom_LocalData("groupID");
            const accessToken = await getFrom_LocalData_LessLogging("accessToken");
            const pmid = await getFrom_LocalData("pmID")
            const resource_location = `playerDB/playerinfos/groupInfo/pmId/${pmid}/groupId/${groupid}/get-display-info`;

            const response = await fetch(`${domain}/databaseAPI/${resource_location}`, {
                method: 'GET',
                headers: {
                    'Origin': 'androidApp',
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${accessToken}`
                }
            });

            if (response.status > 299) {
                Log_API_error(`Request for get_Display_Config encountered abnormal status code of ${response.status}!`);

                return API_errorResponse(response);
            } else {
                Log_API_info(`Running get_Display_Config without problem! (status: ${response.status})`);
                const json = await response.json();

                if (json === null) {
                    Log_API_info('Did not receive new display configuration! Keeping the old one!');
                    return true;
                } else if (json != undefined) {
                    const display_config = json.result;
                    await storeAs_LocalData(`display_config`, display_config);
                    Log_API_info('New display configuration is saved!');

                    const CC_display = await getFrom_LocalData('showRSS');
                    const time_display = await getFrom_LocalData('showTime');
                    const date_display = await getFrom_LocalData('showDate');

                    if (CC_display != display_config.displayCC) {
                        await storeAs_LocalData(`showRSS`, display_config.displayCC);
                        triggerNewDisplay_RSS();
                    }
                    if (time_display != display_config.displayTime) {
                        await storeAs_LocalData(`showTime`, display_config.displayTime);
                        triggerNewDisplay_time()
                    }
                    if (date_display != display_config.displayDate) {
                        await storeAs_LocalData(`showDate`, display_config.displayDate);
                        triggerNewDisplay_date()
                    }

                    return true;
                }
            }
        } catch (err) {
            Log_API_error(`Request for get_Display_Config encountered some problems due to ${err}!`);
            Log_API_warn(`Wait for ${sleep_time / 1000} seconds until the next retry (current retry attempt: ${attempts})!`);
            sleep(sleep_time)
            if (attempts <= 0) {
                Log_API_error(`Request for get_Display_Config failed due to ${err}!`);
                return undefined;
            }
            else {
                return await call(attempts - 1);
            }
        }
    }
    return await call(attempts)
}


export async function get_MediaUpdate(type, attempts = 3) {
    Log_API_debug("get_MediaUpdate "+type+" starts")
    async function call(attempts) {
        try {
            let new_playlist_Arr;
            const pmid = await getFrom_LocalData_LessLogging("pmID");
            const accessToken = await getFrom_LocalData_LessLogging("accessToken");
            const resource_location = `playerDB/eventJobs/mediaUpdate/pmId/${pmid}/get-unprocessed-job/${type}`;

            const response = await fetch(`${domain}/databaseAPI/${resource_location}`, {
                method: 'GET',
                headers: {
                    'Origin': 'androidApp',
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${accessToken}`
                }
            });

            if (response.status > 299) {
                Log_API_error(`Request for get_MediaUpdate encountered abnormal status code of ${response.status}!`);

                return API_errorResponse(response);
            } else {
                Log_API_info(`Running get_MediaUpdate (type: ${type}) without problem! (status: ${response.status})`);
                if (response.status == 204) {
                    Log_API_info(`Did not receive new playlist (type: ${type})! Keeping the old one!`);
                    return true;
                } else {
                    const json = await response.json();
                    if (json != undefined) {
                        const download_job = json.result;

                        Log_API_warn('Things to be downloaded: ' + JSON.stringify(download_job));

                        const cur_playlist_Arr = await getFrom_LocalData_AsyncStorage_LessLogging(`${type}_media_ArrList`)

                        if (type === 'playlist') {
                            const Tobe_DL_Jobs = process_2B_DLjobs(download_job, type)
                            const DLed_jobs = await decide_PlaylistStatus(Tobe_DL_Jobs, cur_playlist_Arr);
                            if (DLed_jobs != [] || DLed_jobs === Tobe_DL_Jobs) {
                                new_playlist_Arr = await process_DLedjobs(DLed_jobs, type)
                                await Housekeeping_UnusedMedia(new_playlist_Arr);
                                Log_API_info(`Storing the new playlist (type: ${type}) into the device!`);
                                triggerMedia_Reload_new()
                                return true;
                            } else {
                                Log_API_error(`NOT storing the new playlist (type: ${type}) into the device because downloading had some problems!`);
                                return false;
                            }
                        } else if (type === 'CC') {
                            new_playlist_Arr = await process_DLedjobs(download_job, type)
                            Log_API_info(`Storing the new playlist (type: ${type}) into the device!`);
                            triggerCC_Reload_new();
                            return true;
                        }
                    }
                }
            }
        } catch (err) {
            Log_API_error(`Request for get_MediaUpdate (type: ${type}) encountered some problems due to ${err}!`);
            Log_API_warn(`Wait for ${sleep_time / 1000} seconds until the next retry (current retry attempt: ${attempts})!`);
            sleep(sleep_time)
            if (attempts <= 0) {
                Log_API_error(`Request for get_MediaUpdate (type: ${type}) failed due to ${err}!`);
                
                return undefined;
            }
            else {
                return await call(attempts - 1);
            }
        }
    }
    return await call(attempts)
}

async function decide_PlaylistStatus(new_playlist, cur_playlist) {
    Log_API_debug("decide_PlaylistStatus starts")
    let res_forDL;
    let res_forDL_cnt = 0;
    let required_DL_list;
    try {
        required_DL_list = check_RequiredDL_Jobs(new_playlist);
        for (let new_playlist_idx = 0; new_playlist_idx != required_DL_list.length; new_playlist_idx++) {
            if ((required_DL_list[new_playlist_idx].status == "pending" || required_DL_list[new_playlist_idx].status == "read" || required_DL_list[new_playlist_idx].status == "processing")) {

                let resFrom_Processing_1 = await patch_DL_Processing_beforeDL(required_DL_list[new_playlist_idx].jobId, "Playlist");

                if (resFrom_Processing_1 == true) {

                    if (cur_playlist === null || cur_playlist === undefined) {
                        Log_API_info(`The previous job is ${cur_playlist}! Downloading a new playlist now!`)
                        res_forDL = await DownloadPlaylist(required_DL_list[new_playlist_idx])
                        if (res_forDL == true) {
                            res_forDL_cnt++
                        }
                    }
                    else {
                        let found = false
                        Log_API_info(`The previous job is not empty! Checking with the new job for idenetical job(s)!`)
                        for (let cur_playlist_idx = 0; cur_playlist_idx != cur_playlist.length; cur_playlist_idx++) {
                            if (required_DL_list[new_playlist_idx].jobId == cur_playlist[cur_playlist_idx].jobId) {
                                Log_API_info(`The previous playlist is idenetical! Posting Ready to the server!`)
                                await patch_DL_Ready_afterDL(required_DL_list[new_playlist_idx].jobId); //tbd
                                res_forDL_cnt++
                                break
                            }
                        }
                        if (found == false) {
                            Log_API_info(`The previous job is not idenetical! Downloading a new playlist now!`)
                            res_forDL = await DownloadPlaylist(required_DL_list[new_playlist_idx])
                            //console.log(`${JSON.stringify(new_playlist[new_playlist_idx])}: ${res_total}`)
                            if (res_forDL == true) {
                                res_forDL_cnt++
                            }
                        }
                    }
                }
                else {
                    continue
                }
            }
        }
        if (res_forDL_cnt == required_DL_list.length) {
            Log_API_info(`The new job has no problem in downloading the required playlist(s)!`)
        }
        else {
            Log_API_error(`The new job has some problems in downloading the required playlist(s)! (Ready-to-play No.: ${res_forDL_cnt}, total no.: ${required_DL_list.length})`)
        }
        return required_DL_list




    }
    catch (err) {
        Log_API_error(`Request for decide_MediaStatus encountered some problems due to ${err}!`)
        return undefined
    }
}

function check_RequiredDL_Jobs(new_playlist) {
    Log_API_debug("check_RequiredDL_Jobs starts")
    let required_DL_list = [];
    let current_time = moment().utc();
    let allowed_time_limit = 30; // hard-coded 30 mins of allowed time for processing media update job
    let isUpcomingJob_Present = false;
    Log_API_info(`The allowed limit for downloading the media from a media update job is ${allowed_time_limit} mins!`)


    for (let new_playlist_idx = 0; new_playlist_idx != new_playlist.length; new_playlist_idx++) {
        let time_to_Check = moment(new_playlist[new_playlist_idx].newPlaylistMedia.activatedAt).utc();
        let time_difference_inMin = time_to_Check.diff(current_time, 'minute', true);
        let time_difference_inDay = time_to_Check.diff(current_time, 'day', true);

        if((Math.abs(time_difference_inDay) <= 1)){
        if ((time_difference_inMin > -(allowed_time_limit)) || current_time.isSameOrAfter(new_playlist[new_playlist_idx].newPlaylistMedia.activatedAt )) {
            Log_API_info(`Putting playlist job (JobId: ${new_playlist[new_playlist_idx].jobId}) into a list for processing because it is not expired!`) 
            required_DL_list.push(new_playlist[new_playlist_idx])
            if (isUpcomingJob_Present == false) {
                isUpcomingJob_Present = true
            }
        }
        else {
            Log_API_info(`Checking playlist job (JobId: ${new_playlist[new_playlist_idx].jobId}) if it is the lastest expired job and there is no upcoming updates!`)

            if (isUpcomingJob_Present == true) {
                Log_API_warn(`NOT Putting playlist job (JobId: ${new_playlist[new_playlist_idx].jobId}) into a list for processing because it is expired and there are upcoming updates!`)
                continue;
            }
            else {
                if (new_playlist[new_playlist_idx].onceOnly == false) {
                    Log_API_info(`Putting playlist job (JobId: ${new_playlist[new_playlist_idx].jobId}) into a list for processing because there is no upcoming updates, taking the latest expired job!`)
                    required_DL_list.push(new_playlist[new_playlist_idx])
                    break;
                }


            }
            }
        }

    }
    Log_API_warn(`The list of media job(s) that requires processing is as follow: ${JSON.stringify(required_DL_list, null, 4)}!`)
    return required_DL_list

}