import { Text, View } from 'react-native';
import { useEffect, useState } from 'react';
import moment from 'moment'
import EventEmitter from 'events';
import { getFrom_LocalData } from '../util/stats/info/Secured_Machine_Info';
import { styles } from '../style/Date_timerCSS';

const time_emitter = new EventEmitter();
/**
 * Author: Ken Lung, Time: 03/04/2024
 * 
 * Event emitter to detect new changes
 * on the Date component sent by the backend server
 * 
 * More on what it does in later section with the related useEffect hook
 * 
 * triggerNewDisplay_time is a function for exporting to Fetch_API.js
 * So that it can use the function to tell Date_timer.js
 * that some visual setting related to the time needs to be changed
 * 
 * triggerNewDisplay_date is a function for exporting to Fetch_API.js
 * So that it can use the function to tell Date_timer.js
 * that some visual setting related to the date needs to be changed
 */
export function triggerNewDisplay_time() {
    time_emitter.emit('NewDisplayTime', true)
}

export function triggerNewDisplay_date() {
    time_emitter.emit('NewDisplayDate', true)
}


export default function Date_timer() {
    
    const [cur_Date, setCur_Date] = useState(''); //Recording the current date measured from system clock in local time
    const [cur_Time, setCur_Time] = useState(''); //Recording the current time measured from system clock in 12-hour clock
    const [showDate, setShowDate] = useState(true); //Determining whether the date has to be shown in the app
    const [showTime, setShowTime] = useState(true); //Determining whether the time has to be shown in the app


    /**
     * Author: Ken Lung, Time: 03/04/2024
     * useEffect hooks for fetching necessary data from SecureStore and AsyncStorage:
     * They run on start-up once only
     * Overall pattern: 
     * 1st: Get the data using specfic key as res (If you need to change the key name, CHANGE WITH CAUTION)
     * 2nd: Set the intended default value to the key if it is empty or undefined; else proceed 
     * 3rd: Set the useState variables as res 
     */

    useEffect(() => {
        async function fetchShowTime() {
            try {
                const res = await getFrom_LocalData("showTime");
                //console.log("fetchShowTime: "+ res)

                if (res === null || res === undefined) {
                    await storeAs_LocalData("showTime", true);
                    setShowTime(true);
                } else {
                    setShowTime(res);
                }
            } catch (error) {
                // Handle exceptions (e.g., log the error)
                Log_MACHINE_error(`Error fetching showTime:" ${error}`)
            }
        }
        async function fetchShowDate() {
            try {
                const res = await getFrom_LocalData("showDate");
                //console.log("fetchShowDate: "+ res)
                if (res === null || res === undefined) {
                    await storeAs_LocalData("showDate", true);
                    setShowDate(true);
                } else {
                    setShowDate(res);
                }
            } catch (error) {
                // Handle exceptions (e.g., log the error)
                Log_MACHINE_error(`Error fetching showDate:" ${error}`)
            }
        }
        fetchShowTime()
        fetchShowDate()
    }, [])

    /**
     * Author: Ken Lung, Time: 03/04/2024
     * useEffect hooks for updating the time and date:
     * They run indefinietely, and run on a 1 second interval with setInterval
     * Get the current time and date in a specfic format 
     * Set the relevant useState variables with the result obtained from the last step 
     * Return a clean-up function to clear the timer if the program closes 
     */

    useEffect(function () {
        const timer = setInterval(function () {
            //setTime(time => time = null)
            var date = moment().format('DD-MM-YYYY'); //Format the current date 
            var time = moment().format('hh:mm:ss a') //Format the current time in 12-hour format
        
            setCur_Date(cur_Date => cur_Date = date) //Set the current date
            setCur_Time(cur_Time => cur_Time = time) //Set the current time 
            
        }, 1000); //Run on a 1 second interval with setInterval indefeinitely 
        return (function () { clearInterval(timer) }) //Clean-up the function to clear the timer when the program closes 
    })

    /**
     * Author: Ken Lung, Time: 03/04/2024
     * useEffect hooks for updating the display property of time and date:
     * They run when the trigger has been triggerend (from Fetch_API.js)
     * They depend on showTime and showDate
     */

    useEffect(function () {
        async function handleNewDisplay_time() {
            let time_display = await getFrom_LocalData('showTime') //set showTime with the lastest boolean value 
            //To see if the component needs to show the time

            setShowTime(time_display)
        }
        async function handleNewDisplay_date() {
            let date_display = await getFrom_LocalData('showDate') //set showDate with the lastest boolean value 
            //To see if the component needs to show the date

            setShowDate(date_display)

        }

        /**
         * Author: Ken Lung, Time: 03/04/2024
         * timer_emitter basically tells the program to start listening for the events with the same header as the first parameter 
         * If it listens to have an event with such a header, it will run the function in the second parameter  
         */

        time_emitter.on('NewDisplayTime', handleNewDisplay_time)
        time_emitter.on('NewDisplayDate', handleNewDisplay_date)

        /**
         * Author: Ken Lung, Time: 03/04/2024
         * Clean up the event listener when the component unmounts
         */

        return () => {

            time_emitter.off('NewDisplayTime', handleNewDisplay_time)
            time_emitter.off('NewDisplayDate', handleNewDisplay_date)


        };
    }, [showDate, showTime]);
    /**
     * Author: Ken Lung, Time 03/04/2024
     * The following code is for returning the View necessary for showing the content 
     * Texts with the style of styles.abs are for making a greater constrast with the background with the shadowing 
     */
    return (
        <View style={styles.time_container}>
            <Text style={[styles.date, showDate ? { display: 'flex', marginRight: 'auto' } : { display: 'none' }]}>{cur_Date}</Text>
            <Text style={[styles.date, styles.abs, { textShadowOffset: { width: -2, height: -2 } }, showDate ? { display: 'flex', marginRight: 'auto' } : { display: 'none' }]}>{cur_Date}</Text>
            <Text style={[styles.date, styles.abs, { textShadowOffset: { width: -2, height: 2 } }, showDate ? { display: 'flex', marginRight: 'auto' } : { display: 'none' }]}>{cur_Date}</Text>
            <Text style={[styles.date, styles.abs, { textShadowOffset: { width: 2, height: -2 } }, showDate ? { display: 'flex', marginRight: 'auto' } : { display: 'none' }]}>{cur_Date}</Text>

            <Text style={[styles.time, showTime ? { display: 'flex', marginLeft: 'auto' } : { display: 'none' }]}>{cur_Time}</Text>
            <Text style={[styles.time, styles.abs, { textShadowOffset: { width: -2, height: -2 } }, showTime ? { display: 'flex', marginLeft: 'auto' } : { display: 'none' }]}>{cur_Time}</Text>
            <Text style={[styles.time, styles.abs, { textShadowOffset: { width: -2, height: 2 } }, showTime ? { display: 'flex', marginLeft: 'auto' } : { display: 'none' }]}>{cur_Time}</Text>
            <Text style={[styles.time, styles.abs, { textShadowOffset: { width: 2, height: -2 } }, showTime ? { display: 'flex', marginLeft: 'auto' } : { display: 'none' }]}>{cur_Time}</Text>
        </View>
    )
}
