import { EventEmitter } from 'events';
import { Video } from 'expo-av';
import * as FileSystem from 'expo-file-system';
import { Image } from 'expo-image';
import moment from 'moment';
import { useEffect, useRef, useState } from 'react';
import { Dimensions, View } from 'react-native';
import { showToast } from '../../background_jobs/notification/Toast_notification.js';

import Animated, { useAnimatedStyle, useSharedValue, withSpring } from 'react-native-reanimated';
import { MediaReport } from '../../API/MediaReport.js';
import { patch_DL_Completed_afterDL, patch_DL_Failed_afterDL, patch_DL_Incomplete_afterDL, patch_DL_PlayOnce_afterDL, patch_DL_Ready_afterDL } from '../../background_jobs/download/DownloadMedia_ReportStatus.js';
import { checkSubset } from '../../util/helper/Check_subset.js';
import { Log_MEDIA_debug, Log_MEDIA_error, Log_MEDIA_info, Log_MEDIA_warn } from '../../util/helper/Log_shortcut.js';

import { post_Abnormal_Alert } from '../../background_jobs/alert/Send_Alert.js';
import { Convert_playtime_toMs } from '../../util/helper/Convert_playtime_toMs.js';

import { styles } from '../../style/Media_playerCSS.js';
import { getFrom_LocalData_AsyncStorage, storeAs_LocalData_AsyncStorage } from '../../util/stats/info/Async_Machine_Info.js';
import { getFrom_LocalData, storeAs_LocalData } from '../../util/stats/info/Secured_Machine_Info.js';

import { Modify_playlist } from '../../background_jobs/modify_FromConfig/Modify_JSON.js';
import { getFrom_LocalData_LessLogging, storeAs_LocalData_LessLogging } from '../../util/stats/info/Secured_Machine_Info.js';

import { postCurCC } from '../../components/player/RSS_player_new.js';

/**
 * Author: Ken Lung, Time: 27/03/2024
 * Event emitter to detect new changes
 * on the media component sent by the backend server
 */

const playlist_emitter = new EventEmitter();

export function triggerMedia_Reload_new() {
    playlist_emitter.emit('Reload', true) //Exporting to Fetch_API.js for loading the new stack of media playlist job
}

export function triggerModify_playlist_new() {
    playlist_emitter.emit('Modify', true) //Exporting to Fetch_API.js for changing the configuration of the currently playing media playlist job

}

export function triggerReportMedia_new() {
    playlist_emitter.emit('MediaReport', true) //Exporting to Fetch_API.js for changing whether the component needs to report the media that is playing at the moment 
}
export function triggerViewCurMedia_new() {
    playlist_emitter.emit('ViewCurMedia', true) //Exporting to App.js for creating a pop-up to show the name and the Id of media playing at the moment 
}

export function triggerForcePopping_Deleted_Playlist() {


    playlist_emitter.emit('ForcePopping', true)
}
export default function Media_player_new() {


    /**
   * Author: Ken Lung, Time:  27/03/2024
   * useState variables for operation:
   
   * activationDates: an array to specfically record the activation time of the current and future media playing job(s)
   * to let the component to check whether any job have reached its activation time 
   * without resulting extensive search to mediaPlaylist
   * 
   * completeReported: an array to record the played media playing jobs' jobId
   * So that the component will not report "Completed" status to the backend server, avoiding excessive call
   * 
   * last_NonPlayOnce_playlist: an array for recording the latest constant media playlist 
   * So that the media player would play the media playing job of this jobId when there are no other playable job
   * (constant playlist means it is a playlist that is not a playOnce media playlist)
   */


    const [media_Stack, setMedia_Stack] = useState([]); //Stores a series of JSON of the current and future media playing job(s)
    const [stack_Index, setStack_Index] = useState({
        item_num: undefined //Indicating what is the current playing number of the media playing job list
    })

    const [activationDates_Stack, setActivationDates_Stack] = useState([]) //Specfically record the activation time of the current and future media playing job(s)

    const [completeReported, setCompleteReported] = useState([]) //Acting as a stack that stores the current and future playlist playing job(s)' activation time
    const [last_NonPlayOnce_playlist, setLast_NonPlayOnce_playlist] = useState([]) //Recording the latest constant media playlist for loading back to the stack if the last job is playOnce

    const [lastReported_playlist_media, setLastReported_playlist_media] = useState([]) //Housing the jobId of the last reported media to avoid excessive call to the backend 
    //const [last_playingPt, setLast_playingPt] = useState()
    const [last_playing_StackIndex, setLast_playingStackIndex] = useState()

    const [init_vid_pt, setInit_vid_pt] = useState(0); //starting point of the video
    const [end_vid_pt, setEnd_vid_pt] = useState(0); //ending point of the video, not properly implemented 

    const video = useRef(null); //reference for the video component 
    //const [status, setStatus] = useState({});

    const [need_playlist_report, setNeed_playlist_report] = useState(true); //checking whether the component needs to report the currently playing media to the backend

    const [isLoading, setIsLoading] = useState(true); //showing whether the component has properly loaded mediaPlaylist
    const opacity = useSharedValue(0);
    const flipX = useSharedValue(0);
    const flipY = useSharedValue(0);
    const spin = useSharedValue(0);
    const translateX = useSharedValue(200);
    const translateY = useSharedValue(200);
    const scaleX = useSharedValue(0);
    const scaleY = useSharedValue(0);

    const { height, width } = Dimensions.get('screen');
    const isPortrait = height > width;

    /**
  * Author: Ken Lung, Time: 27/03/2024
  * functions that are used in useEffect hooks for setting up the useState variables 
  * using getFrom_localData and similar functions with similar name
  * for getting persistent data from SecureStore
  * or getFrom_LocalData_AsyncStorage for getting persistent data from SecureStore
  * 
  * if the resulting promise from the above two functions return null
  * it means there have not been a value stored in the respective key 
  * then the hook will store the default values for the respective key 
  * 
  * finally, set the returning value or the default value for the useState variables 
  */

    async function fetchCompletePlaylist_Reported() {
        try {
            const res = await getFrom_LocalData_AsyncStorage("CompletePlaylist_Reported");
            //console.log("hello"+ res)
            if (res === null || res === undefined) {
                await storeAs_LocalData_AsyncStorage("CompletePlaylist_Reported", []);
                setCompleteReported([]);
            } else {
                setCompleteReported(res);
            }
            return res
        } catch (error) {
            // Handle exceptions (e.g., log the error)
            Log_MEDIA_error(`Error fetching CompletePlaylist_Reported:" ${error}`)
        }

    }



    async function fetch_Activation_Dates() {
        try {
            const res = await getFrom_LocalData_LessLogging("Activation_Dates");
            if (res === null || res === undefined) {
                await storeAs_LocalData_LessLogging("Activation_Dates", []);
                setActivationDates_Stack([]);
            } else {
                setActivationDates_Stack(res);
            }
            return res
        } catch (error) {
            // Handle exceptions (e.g., log the error)
            Log_MEDIA_error(`Error fetching Activation_Dates:" ${error}`)
        }

    }


    async function fetchCompletePlaylist_Reported() {
        try {
            const res = await getFrom_LocalData_AsyncStorage("CompletePlaylist_Reported");
            //console.log("hello"+ res)
            if (res === null || res === undefined) {
                await storeAs_LocalData_AsyncStorage("CompletePlaylist_Reported", []);
                setCompleteReported([]);
            } else {
                setCompleteReported(res);
            }
            return res
        } catch (error) {
            // Handle exceptions (e.g., log the error)
            Log_MEDIA_error(`Error fetching CompletePlaylist_Reported:" ${error}`)
        }

    }

    async function fetch_Last_NonPlayOnce_playlist() {
        try {
            const res = await getFrom_LocalData_AsyncStorage("Last_NonPlayOnce_playlist");
            if (res === null || res === undefined) {
                await storeAs_LocalData_AsyncStorage("Last_NonPlayOnce_playlist", []);
                setLast_NonPlayOnce_playlist([]);
            } else {
                setLast_NonPlayOnce_playlist(last_NonPlayOnce_playlist => last_NonPlayOnce_playlist = res);
            }
            return res
        } catch (error) {
            // Handle exceptions (e.g., log the error)
            Log_MEDIA_error(`Error fetching Last_NonPlayOnce_playlist:" ${error}`)
        }
    }

    async function fetch_LastReported_playlist_media() {
        try {
            const res = await getFrom_LocalData_LessLogging("Last_Reported_playlist_media");
            if (res === null || res === undefined) {
                await storeAs_LocalData_LessLogging("Last_Reported_playlist_media", -1);
                setLastReported_playlist_media(-1);
            } else {
                setLastReported_playlist_media(res);
            }
            return res
        } catch (error) {
            // Handle exceptions (e.g., log the error)
            Log_MEDIA_error(`Error fetching Last_Reported_playlist_media:" ${error}`)
        }
    }

    async function fetch_mediaReport_Needed() {
        try {
            const res = await getFrom_LocalData_LessLogging("CurMediaReport_Needed");
            if (res === null || res === undefined) {
                await storeAs_LocalData_LessLogging("CurMediaReport_Needed", false);
                setNeed_playlist_report(false);
            } else {
                setNeed_playlist_report(res);
            }
            return res
        } catch (error) {
            // Handle exceptions (e.g., log the error)
            Log_MEDIA_error(`Error fetching need_playlist_report:" ${error}`)
        }
    }


    useEffect(() => {
        fetch_Activation_Dates()
        fetchCompletePlaylist_Reported()
        fetch_Last_NonPlayOnce_playlist()
        fetch_LastReported_playlist_media()
        fetch_mediaReport_Needed()
        return (() => {
            setActivationDates_Stack([])
            setCompleteReported([])
            setLast_NonPlayOnce_playlist([])
            fetch_LastReported_playlist_media([])
        })
    }, [])

    useEffect(() => {
        //console.log("start fetch_Starting_media")
        fetch_Starting_media()
        return (() => {
            setMedia_Stack([]);
            Image.clearMemoryCache()

        })

    }, [])

/*
[{"newPlaylistMedia":
    {"_id":"66a8a04e2439319d928f6964","playlistName":"Unnamed","playlistId":296,"content":
        [
            {"_id":"66a8a04e2439319d928f6962","startPt":"0.0.0","stopPt":"0.0.0","duration":0.21778333333333333,"uploadedMedia":
            {"_id":"669a25ad6bceaf8176b6122e","fileName":"Y2meta.app-Chipi chipi chapa chapa cat-(1080p).mp4","mediaId":62,"mediaType":"video","__t":"video"},"enAnime":"None","__t":"video"}
        ],"playMode":"ORDER","activatedAt":"2024-07-30T08:12:00.671Z"
    }
}]

*/
async function fetch_Starting_media() {
    Log_MEDIA_debug('Fetch_Starting_media starts running')
    try {
        setIsLoading(true);
        const res = await getFrom_LocalData_AsyncStorage('playlist_media_ArrList');
        await Log_MEDIA_warn('The value of res was: ' + JSON.stringify(res));
        const newPlaylistMedia = res[0].newPlaylistMedia;
        if (newPlaylistMedia.content.length == 0 && moment().isSameOrAfter(moment(newPlaylistMedia.activatedAt) || res == []) ){
            setIsLoading(true);
            await Log_MEDIA_error('fetch_Starting_media : empty')
        } else
        // Check if res is an array and has at least one element
        if (Array.isArray(res) && res.length > 0) {

            // Check if newPlaylistMedia and its content array exist and have at least one element
            if (newPlaylistMedia && Array.isArray(newPlaylistMedia.content) && newPlaylistMedia.content.length > 0) {

                    let ok_to_play = await fetch_Indexes(res);
                    if (ok_to_play == true) {
                        setIsLoading(false);
                    }
                
            } else {
                throw new Error("newPlaylistMedia content is undefined or empty");
            }
        } else {
            throw new Error("res is not an array or is empty");
        }
    } catch (error) {
        if (error.message.includes("uploadedMedia is undefined") || error.message.includes("newPlaylistMedia content is undefined or empty") || error.message.includes("res is not an array or is empty") || error.message.includes("TypeError: Cannot read property 'uploadedMedia' of undefined")) {
            await storeAs_LocalData_AsyncStorage("playlist_media_ArrList", []);
            Log_MEDIA_error('fetch_Starting_Media : error catch empty')
            if (moment().isSameOrAfter(moment(newPlaylistMedia.activatedAt))) {
            setIsLoading(true);
        }
        }
        Log_MEDIA_error(`Error fetching playlist_media_ArrList: ${error}`);
        setIsLoading(true);
    }
}

    async function fetch_Indexes(response) {
        Log_MEDIA_debug("fetch_Indexes starts running")
        try {
            let latestNon_playOnce = await getFrom_LocalData_AsyncStorage("Last_NonPlayOnce_playlist")
            Log_MEDIA_warn('The value of last_NonPlayOnce_playlist was '+ JSON.stringify(last_NonPlayOnce_playlist))
            let nextList_activate_Date;
            let choosen_playlist = -1;
            let num = 0

            for (let legit_list = response.length - 1; legit_list != -1; legit_list--) {  //loop through the responsding array's json(s) to find a activatable playlist

                if (response[legit_list].newPlaylistMedia.content.length == 0) { //if the job has no item, instead of playing item, we will close the video player and display background
                    Log_MEDIA_warn('the list is empty, proceed to check completion');
                    //if (completeReported.includes(response[legit_list].jobId) == false) 
                    {   completeReported.push(response[legit_list].jobId)
                    
                        //setIsLoading(true)
                        await storeAs_LocalData_AsyncStorage("playlist_media_ArrList", response);
                        await storeAs_LocalData_AsyncStorage("CompletePlaylist_Reported", completeReported);
                        await patch_DL_Ready_afterDL(response[legit_list].jobId);
                        await put_ActivationDates(response);
                        choosen_playlist = 0;
                    }
                    continue
                }
                else {
                    /**
                     * Author: Ken Lung, Time: 15/04/2024
                     * if the job is a new job ready to activate (nextPlaylist_activate_Date is same or after last_NonPlayOnce)
                     * or it is the last constant playlist playing job, 
                     * it is the first index in the responsding array's json(s) to be choosen for playing
                     */
                    nextList_activate_Date = response[legit_list].newPlaylistMedia.activatedAt


                    if (moment().isSameOrAfter(moment(nextList_activate_Date))) {

                        let downloaded_media = await find_AllMedia_inPlaylist(legit_list, response)
                        let parent_arr = response[legit_list].newPlaylistMedia.content
                        //console.log(JSON.stringify(parent_arr))


                        if (checkSubset(parent_arr, downloaded_media) || response[legit_list].incompleteButPlay == true) { //check if the playlist has all the media needed
                            choosen_playlist = legit_list
                            break;
                        }
                        else {
                            Log_MEDIA_error(`Playlist (JobId: ${response[legit_list].jobId}) is incomplete but it is NOT allowed to play!`)
                            continue
                        }
                    }
                }
            }

            if (choosen_playlist !== -1) {

                num = getFirst_Itemindex(choosen_playlist, response)

                setStack_Index(state => ({ item_num: num })) //set the starting index in the useState variable 

                //console.log(response[choosen_playlist].onceOnly)

                if (response[choosen_playlist].onceOnly == false) {
                    if (latestNon_playOnce === null || latestNon_playOnce == []) {
                        setLast_NonPlayOnce_playlist(last_NonPlayOnce_playlist => last_NonPlayOnce_playlist = response[choosen_playlist]);
                        await storeAs_LocalData_AsyncStorage("Last_NonPlayOnce_playlist", response[choosen_playlist]);
                    }
                    else if (latestNon_playOnce.jobId != response[choosen_playlist].jobId) {
                        setLast_NonPlayOnce_playlist(last_NonPlayOnce_playlist => last_NonPlayOnce_playlist = response[choosen_playlist]);
                        await storeAs_LocalData_AsyncStorage("Last_NonPlayOnce_playlist", response[choosen_playlist]);
                    }
                }
                new_response = [...response]; // Create a copy
                //let new_response = await Tidyup_Emptylist(response, 'playlist'); //removing all empty playlist 
                setMedia_Stack(media_Stack => media_Stack = new_response);
                await storeAs_LocalData_AsyncStorage("playlist_media_ArrList", new_response);
                await put_ActivationDates(new_response);
                return true
            }
            else {

                Log_MEDIA_warn(`There is no valid playlist to play from due to they are either played PlayOnce playlist or incomplete playlist!`)
                Log_MEDIA_error('fetch_Indexes : invalid playlist')
                setIsLoading(true) //Do not play anything when there is nothing to play 
            }
        }
        catch (error) {
            Log_MEDIA_error(`Some unexpected error happened in fetch_Indexes due to ${error}!`)
        }

    }



    async function put_ActivationDates(response) {
        Log_MEDIA_debug("put_ActivationDates starts running")
        let new_activationDates = []

        for (let remaining_num = response.length - 1; remaining_num != -1; remaining_num--) {
            new_activationDates.push(response[remaining_num].newPlaylistMedia.activatedAt); //push all the job's activation date
        }
        new_activationDates.reverse() //reverse the order to make it ordering from the oldest on top to the youngest at the bottom 
        setActivationDates_Stack(activationDates_Stack => activationDates_Stack = new_activationDates) //store it to the useState variable 

        await storeAs_LocalData("Activation_Dates", new_activationDates); //store in securestore

    }

    function getFirst_Itemindex(target, source) {
        Log_MEDIA_debug("getFirst_Itemindex starts running")
        //Return the starting index of the playlist for playing 

        switch (source[target].newPlaylistMedia.playMode) {
            case 'REVERSE':
                if (last_playing_StackIndex == undefined && (source[target].onceOnly == false || source[target].onceOnly == undefined)) {

                    setLast_playingStackIndex(source[target].newPlaylistMedia.content.length - 1)

                }
                return source[target].newPlaylistMedia.content.length - 1;
            case 'SHUFFLE':
                if (last_playing_StackIndex == undefined && (source[target].onceOnly == false || source[target].onceOnly == undefined)) {

                    setLast_playingStackIndex(Math.floor(Math.random() * source[target].newPlaylistMedia.content.length))

                }
                return Math.floor(Math.random() * source[target].newPlaylistMedia.content.length);
            case 'ORDER':
                if (last_playing_StackIndex == undefined && (source[target].onceOnly == false || source[target].onceOnly == undefined)) {

                    setLast_playingStackIndex(0)

                }
                return 0;

        }
    }

    function nextItem_Index_accordingTO_playMethod() {
        Log_MEDIA_debug("nextItem_Index_accordingTO_playMethod starts running")
        if (media_Stack[media_Stack.length - 1].newPlaylistMedia.content.length != 1 || media_Stack[media_Stack.length - 1].newPlaylistMedia.content.length != 0) {

            setIsLoading(true)

            switch (media_Stack[media_Stack.length - 1].newPlaylistMedia.playMode) {
                case 'ORDER':
                    nextIndex_inORDER();
                    break;

                case 'REVERSE':
                    nextIndex_inREVERSE();
                    break;

                case 'SHUFFLE':
                    nextIndex_inSHUFFLE();
                    break;

                default:
                    break;
            }
            setIsLoading(false)

        }
        else if (media_Stack[media_Stack.length - 1].newPlaylistMedia.content.length == 1) {
            let new_num = stack_Index.item_num
            let type = media_Stack[media_Stack.length - 1].newPlaylistMedia.content[new_num].uploadedMedia.mediaType

            checkAccidental_Repeated(stack_Index.item_num, new_num, type);

        }
        else {
            Log_MEDIA_warn(`There is no media in the playlist!`)
            Log_MEDIA_error('nextItem_Index_accordingTo_playMethod : catch empty')
            setIsLoading(true)
        }


    }



    function nextIndex_inORDER() {
        //setItem_Index(item_index => item_index = (item_index + 1) % received_CurMedia[playlist_index].newPlaylistMedia.content.length)
        if (!isLoading) {

            let new_num = (stack_Index.item_num + 1) % media_Stack[media_Stack.length - 1].newPlaylistMedia.content.length

            let type = media_Stack[media_Stack.length - 1].newPlaylistMedia.content[new_num].uploadedMedia.mediaType


            setStack_Index(({ item_num: new_num }))
            if (media_Stack[media_Stack.length - 1].onceOnly == false) {
                setLast_playingStackIndex(new_num)

            }

            checkAccidental_Repeated(stack_Index.item_num, new_num, type);


        }
    }



    async function nextIndex_inREVERSE() {
        if (!isLoading) {
            let new_num = -1


            if (stack_Index.item_num == 0) {
                new_num = media_Stack[media_Stack.length - 1].newPlaylistMedia.content.length - 1
            }
            else {
                new_num = stack_Index.item_num - 1
            }

            let type = media_Stack[media_Stack.length - 1].newPlaylistMedia.content[new_num].uploadedMedia.mediaType


            setStack_Index(({ item_num: new_num }))
            if (media_Stack[media_Stack.length - 1].onceOnly == false) {
                setLast_playingStackIndex(new_num)

            }

            checkAccidental_Repeated(stack_Index.item_num, new_num, type);


        }
    }



    function nextIndex_inSHUFFLE() {
        let new_num = -1;
        //console.log('shuffle')
        if (!isLoading) {


            do {

                new_num = Math.floor(Math.random() * (media_Stack[media_Stack.length - 1].newPlaylistMedia.content.length));
            } while (new_num == stack_Index.item_num)


            let type = media_Stack[media_Stack.length - 1].newPlaylistMedia.content[new_num].uploadedMedia.mediaType


            setStack_Index(({ item_num: new_num }))
            if (media_Stack[media_Stack.length - 1].onceOnly == false) {
                setLast_playingStackIndex(new_num)

            }

            checkAccidental_Repeated(stack_Index.item_num, new_num, type);




        }
    }


    /**
     * Author: Ken Lung, Time: 15/04/2024
     * Handle the case when the next video is using the same source as the last one 
     * "replay" the source from the starting point of the next video 
     **/
    function checkAccidental_Repeated(play_num, item_num, type) {
        Log_MEDIA_debug("checkAccidental_Repeated starts running")
         {
            if (play_num.mediaId=== item_num.mediaId) {
                if (type === "video") {
                    call_parseTime(media_Stack[media_Stack.length - 1].newPlaylistMedia.content[stack_Index.item_num].duration * 60000, media_Stack[media_Stack.length - 1])
                    play_animation();
                    video.current.playFromPositionAsync(init_vid_pt);
                }
                else if (type === "slide") {
                    play_animation()
                }
            }

        }
    }



    async function nextPlaylist_Index_accordingTO_ActivateDate() {
        Log_MEDIA_debug("nextPlaylist_Index_accordingTO_ActivateDate starts running")
        let coming_num;
        let new_mediaPlaylist_Stack
        let latestNon_playOnce = await getFrom_LocalData_AsyncStorage("Last_NonPlayOnce_playlist")
        setIsLoading(true)
        if (media_Stack.length > 1) { //If there are more than 1 playlist job in the stack 
            let target = (media_Stack.length - 1) - 1 //Shift down the index of the stack by one 
            let nextList_activate_Date = media_Stack[target].newPlaylistMedia.activatedAt

            if (moment().isSameOrAfter(moment(nextList_activate_Date)) == true) { // needs to activate the next playlist
                //console.log("moment().isSameOrAfter(moment(nextList_activate_Date)) == true")
                if (latestNon_playOnce.jobId != media_Stack[media_Stack.length - 1].jobId && media_Stack[media_Stack.length - 1].onceOnly == false) {
                    setLast_NonPlayOnce_playlist(last_NonPlayOnce_playlist => last_NonPlayOnce_playlist = media_Stack[media_Stack.length - 1]); //save the playlist as a temp in the asyncstorage if it is constant 
                    await storeAs_LocalData_AsyncStorage("Last_NonPlayOnce_playlist", media_Stack[media_Stack.length - 1]);
                }
                /** tbd on recording the time
                 *  if (media_Stack[target].onceOnly == true && media_Stack[media_Stack.length - 1].onceOnly == false) { // if the next playlist is onceOnly, need to record the 
                     //console.log("media_Stack[target].onceOnly == true")
 
                     /**
                      * if (media_Stack[media_Stack.length - 1].newPlaylistMedia.content[stack_Index.item_num].uploadedMedia.mediaType == 'video') {
                         console.log("media_Stack[media_Stack.length - 1].newPlaylistMedia.content[stack_Index.item_num].uploadedMedia.mediaType == 'video'")
                         console.log("video.current.positionMillis" + status.positionMillis)
                         setLast_playingPt(status.positionMillis)
                     }
                      
                 } tbd
                 */ 

                if (media_Stack[media_Stack.length - 1].newPlaylistMedia.content.length !== 0) {
                    coming_num = getFirst_Itemindex(target, media_Stack) //get the first index for playing the next playlist 
                    setStack_Index(state => ({ item_num: coming_num })) //updating the index for playling items in the playlist 

                    new_mediaPlaylist_Stack = [...media_Stack]; // Create a copy
                    new_mediaPlaylist_Stack.pop() //pop away the oldest job 

                    setMedia_Stack(media_Stack => media_Stack = new_mediaPlaylist_Stack); //update the stack 
                    await storeAs_LocalData_AsyncStorage("playlist_media_ArrList", new_mediaPlaylist_Stack);//save the updated stack in asyncstorage 

                    activationDates_Stack.pop() //pop away the oldest job in activation time stack 
                    setActivationDates_Stack(activationDates_Stack => activationDates_Stack = activationDates_Stack) //update the activation time stack 
                    await storeAs_LocalData("Activation_Dates", activationDates_Stack); //save the new time stack into asyncstorage

                    setIsLoading(false)
                }
                else {
                    new_mediaPlaylist_Stack = [...media_Stack]; // Create a copy
                    new_mediaPlaylist_Stack.pop() //pop away the oldest job

                    setMedia_Stack(media_Stack => media_Stack = new_mediaPlaylist_Stack); //update the stack 
                    await storeAs_LocalData_AsyncStorage("playlist_media_ArrList", new_mediaPlaylist_Stack);//save the updated stack in asyncstorage 

                    activationDates_Stack.pop() //pop away the oldest job in activation time stack 
                    setActivationDates_Stack(activationDates_Stack => activationDates_Stack = activationDates_Stack) //update the activation time stack 
                    await storeAs_LocalData("Activation_Dates", activationDates_Stack); //save the new time stack into asyncstorage
    
                    Log_MEDIA_warn(`The next playlist is empty!`)
                    Log_MEDIA_error('nextPlaylist_Index_accordingTo_ActivateDate : error catch empty')
                    setIsLoading(true)
                }


            }
            //tbd on commenting out the code
            else if (moment().isSameOrAfter(moment(nextList_activate_Date)) == false) { // activate the old one 
                //console.log("moment().isSameOrAfter(moment(nextList_activate_Date)) == false")

                coming_num = last_playing_StackIndex //get the first index for playing the next playlist 
                setStack_Index(state => ({ item_num: coming_num })) //updating the index for playling items in the playlist 

                new_mediaPlaylist_Stack = [...media_Stack]; // Create a copy

                new_mediaPlaylist_Stack.pop()
                new_mediaPlaylist_Stack.push(latestNon_playOnce) //push back the last constant job in 

                setMedia_Stack(media_Stack => media_Stack = new_mediaPlaylist_Stack);
                await storeAs_LocalData_AsyncStorage("playlist_media_ArrList", new_mediaPlaylist_Stack); //save the updated stack in asyncstorage 

                activationDates_Stack.pop()
                activationDates_Stack.push(latestNon_playOnce.newPlaylistMedia.activatedAt) //push back the last constant job's activation time 

                setActivationDates_Stack(activationDates_Stack => activationDates_Stack = activationDates_Stack) //update the usestate variable
                await storeAs_LocalData("Activation_Dates", activationDates_Stack); //save the updated version in to async storage 

                if (latestNon_playOnce.newPlaylistMedia.content[coming_num].uploadedMedia && latestNon_playOnce.newPlaylistMedia.content[coming_num].uploadedMedia.mediaType == 'video') {
                    call_parseTime(latestNon_playOnce.newPlaylistMedia.content[coming_num].duration * 60000, latestNon_playOnce)
                    setIsLoading(false)
                    video.current.playFromPositionAsync(init_vid_pt)
                }
                else {
                    setIsLoading(false)
                }

            }
        }
        else if (media_Stack.length <= 1) { //when there is less than or equal to 1 job left 

            if (media_Stack[media_Stack.length - 1].onceOnly == false) { //pop away none the less but this should not happen

                setIsLoading(false)
                return true
            }
            else if (media_Stack[media_Stack.length - 1].onceOnly == true) {

                let new_mediaPlaylist_Stack = [...media_Stack]; // Create a copy

                new_mediaPlaylist_Stack.pop() //pop away the oldest job 
                new_mediaPlaylist_Stack.push(latestNon_playOnce) //push back the last constant job in 
                setMedia_Stack(media_Stack => media_Stack = new_mediaPlaylist_Stack);
                await storeAs_LocalData_AsyncStorage("playlist_media_ArrList", new_mediaPlaylist_Stack); //save the updated stack in asyncstorage 

                activationDates_Stack.pop() //pop away the activation time for the last job 
                activationDates_Stack.push(latestNon_playOnce.newPlaylistMedia.activatedAt) //push back the last constant job's activation time 
                setActivationDates_Stack(activationDates_Stack => activationDates_Stack = activationDates_Stack) //update the usestate variable
                await storeAs_LocalData("Activation_Dates", activationDates_Stack); //save the updated version in to async storage 

                let coming_num = last_playing_StackIndex //get the first index for playing the next playlist 
                setStack_Index(state => ({ item_num: coming_num })) //updating the index for playling items in the playlist 

                if (latestNon_playOnce.newPlaylistMedia.content[coming_num].uploadedMedia && latestNon_playOnce.newPlaylistMedia.content[coming_num].uploadedMedia.mediaType == 'video') {
                    call_parseTime(latestNon_playOnce.newPlaylistMedia.content[coming_num].duration * 60000, latestNon_playOnce)
                    setIsLoading(false)
                    video.current.playFromPositionAsync(init_vid_pt)
                }
                else {
                    setIsLoading(false)
                }
            }
        }
        else {
            Log_MEDIA_warn(`There is no valid playlist to play from due to they are either played PlayOnce playlist or incomplete playlist!`)
            Log_MEDIA_error('nextPlaylist_Index_accordingTo_ActivateDate : invalid playlist')
            setIsLoading(true)
        }
    }

    async function findFileURL_forPlaying(fileName) {
        Log_MEDIA_debug("findFileURL_forPlaying starts running")
        //const fileUri = dir_DCIM + fileName

        const fileInfo = await FileSystem.getInfoAsync(fileName);
        //console.log(fileInfo)
        if (!isLoading) {
            if (!fileInfo.exists) { //if the file does not exists for some reasons 

                Log_MEDIA_warn(`Cannot find the required media for playing! (media: ${fileName})`);
                Log_MEDIA_warn(`The current playlist has missing media! (playlist: ${media_Stack[media_Stack.length - 1].newPlaylistMedia.playlistName})`);
                
                if (media_Stack[media_Stack.length - 1].incompleteButPlay == true) { //continue playing the playlist but skip the current item because it has not been downloaded
                    
                    Log_MEDIA_info(`The current playlist is allowed to play even if not all media have not downloaded! (playlist: ${media_Stack[media_Stack.length - 1].newPlaylistMedia.playlistName})`);
                    Log_MEDIA_info(`Continue playing the current playlist! (playlist: ${media_Stack[media_Stack.length - 1].newPlaylistMedia.playlistName})`)
                    
                    let downloaded_media = await find_AllMedia_inPlaylist(media_Stack.length - 1, media_Stack)
                    
                    if (downloaded_media.length > 0) { //Report "Incomplete" when there is at least one media downladed, so just download the missing item 
                        patch_DL_Incomplete_afterDL(media_Stack[media_Stack.length - 1].jobId, downloaded_media, media_Stack[media_Stack.length - 1].newPlaylistMedia.activatedAt)
                        //Log_MEDIA_info(`Downloading missing media from the current playlist now! (playlist: ${media_Stack[media_Stack.length - 1].newPlaylistMedia.playlistName})`);

                    }
                    else { //Just redownload the entire playlist if no media of the playlist can be found 
                        patch_DL_Failed_afterDL(media_Stack[media_Stack.length - 1].jobId, "No media downloaded!")
                        //Log_MEDIA_info(`Downloading all media from the current playlist now because it has no media to play! (playlist: ${media_Stack[media_Stack.length - 1].newPlaylistMedia.playlistName})`);

                    }
                    nextItem_Index_accordingTO_playMethod(); //play the next item 

                }
                else { //Redownload the entire playlist if it is not incomplete but play 
                    Log_MEDIA_warn(`The current playlist is NOT allowed to play when not all media have not downloaded! (playlist: ${media_Stack[media_Stack.length - 1].newPlaylistMedia.playlistName})`);
                    patch_DL_Failed_afterDL(media_Stack[media_Stack.length - 1].jobId, "Not all media have been downloaded but the playlist is not incompleteButPlay!")
                    nextPlaylist_Index_accordingTO_ActivateDate()


                }

            }
            else {
                await check_PlaylistReported()
            }

        }

    };

    /**
     * Author: Ken Lung, Time: 15/04/2024
     * Get all the file(s) needed for the current playlist
     **/

    async function find_AllMedia_inPlaylist(playlist_num, source) {
        Log_MEDIA_debug("find_AllMedia_inPlaylist starts running")
        try {
            let downloaded_media = [];
            let fileInfo = null;
            if (source[playlist_num].newPlaylistMedia.content.length == 0) {
                return downloaded_media
            }
            for (let media_idx = 0; media_idx != source[playlist_num].newPlaylistMedia.content.length; media_idx++) {
                let file_local_name = source[playlist_num].newPlaylistMedia.content[media_idx].uploadedMedia.mediaId +
                    '.' + source[playlist_num].newPlaylistMedia.content[media_idx].uploadedMedia.fileName.split('.').pop()
                let fileUri = `${FileSystem.documentDirectory}/${file_local_name}`
                fileInfo = await FileSystem.getInfoAsync(fileUri);
                if (fileInfo.exists == true) {
                    downloaded_media.push(source[playlist_num].newPlaylistMedia.content[media_idx])

                }
                //console.log("downloaded_media " + JSON.stringify(downloaded_media))

                return downloaded_media;
            }

        }
        catch (error) {
            Log_MEDIA_error(`Unexpected error happened in find_AllMedia_inPlaylist due to ${error}!`)
        }

    }



    /**
     * Author: Ken Lung, Time: 15/04/2024
     * Get the starting time and the ending time of the video as in ms, not HH:MM:SS
     **/
    function call_parseTime(original_duration, stackPlaylist) {
        if (!isLoading) {
            let startTime_inStr = stackPlaylist.newPlaylistMedia.content[stack_Index.item_num].startPt
            let endTime_inStr = stackPlaylist.newPlaylistMedia.content[stack_Index.item_num].stopPt

            const parsed_time = Convert_playtime_toMs(startTime_inStr, endTime_inStr, original_duration)

            setInit_vid_pt(init_vid_pt => init_vid_pt = parsed_time[0])

            setEnd_vid_pt(end_vid_pt => end_vid_pt = parsed_time[1])


        }
    }

    /**
    * Author: Ken Lung, Time: 15/04/2024
    * Push the last constant job to the stack by first making a deep copy of the stack and save the new stack in the device
    * The activation time will be saved in a stack as wellas well 
    **/
    async function push_LastConstant_Playlist() {
        Log_MEDIA_debug("push_LastConstant_Playlist starts running")
        let new_mediaPlaylist_Stack = [...media_Stack]; // Create a copy
        new_mediaPlaylist_Stack.push(last_NonPlayOnce_playlist) //push back the last constant job in 

        setMedia_Stack(media_Stack => media_Stack = new_mediaPlaylist_Stack);
        await storeAs_LocalData_AsyncStorage("playlist_media_ArrList", new_mediaPlaylist_Stack); //save the updated stack in asyncstorage 

        activationDates_Stack.push(last_NonPlayOnce_playlist.newPlaylistMedia.activatedAt) //push back the last constant job's activation time 
        setActivationDates_Stack(activationDates_Stack => activationDates_Stack = activationDates_Stack) //update the usestate variable
        await storeAs_LocalData("Activation_Dates", activationDates_Stack); //save the updated version in to async storage 

        let coming_num = getFirst_Itemindex(new_mediaPlaylist_Stack.length - 1, new_mediaPlaylist_Stack) //get the first index for playing the next playlist 
        setStack_Index(state => ({ item_num: coming_num })) //updating the index for playling items in the playlist 

    }
    async function postCurMedia() {
       /* if (need_playlist_report == true) */ { //only report when the backend require to report currently playing media
        let media_id
        if (!isLoading) {
            media_id = media_Stack[media_Stack.length - 1].newPlaylistMedia.content[stack_Index.item_num].uploadedMedia.mediaId;
            await MediaReport("playlist", media_id, moment().utc());
        }
            else {
            media_id ="No media playing"
            await MediaReport("playlist", "No media playing", moment().utc())
        }
            if (lastReported_playlist_media.length == -1 || lastReported_playlist_media != media_id ) {
                //const time = moment().format('DD-MM-YYYY hh:mm:ss a')
                
                //Log_MEDIA_info(`The ID of the currently playing media is ${media_id} and it starts playing at ${time})`);
                setLastReported_playlist_media(lastReported_playlist_media => lastReported_playlist_media = media_id)
                await storeAs_LocalData_LessLogging("Last_Reported_playlist_media", media_id)
            }
        }

    }


    function play_animation() {
        const animation = media_Stack[media_Stack.length - 1].newPlaylistMedia.content[stack_Index.item_num].enAnime;
        handleAnimation(animation);
    }

    function reset_Animation() {
        opacity.value = 0;
        flipX.value = 0;
        flipY.value = 0;
        spin.value = 0;
        translateX.value = 200;
        translateY.value = 200;
        scaleX.value = 0;
        scaleY.value = 0;
    }

    function handleAnimation(animation) {

        reset_Animation()
        switch (animation) {
            case 'FadeIn':

                opacity.value = withSpring(1);
                break;
            case 'FlipInEasyX':

                flipY.value = withSpring(180, { damping: 10 });
                break;
            case 'FlipInEasyY':

                flipX.value = withSpring(180, { damping: 10 });
                break;
            case 'PinwheelIn':

                spin.value = withSpring(720);
                break;
            case 'SlideInRight':

                translateX.value = withSpring(1);
                break;
            case 'SlideInUp':

                translateY.value = withSpring(1);
                break;
            case 'StretchInX':

                scaleX.value = withSpring(1);
                break;
            case 'StretchInY':

                scaleY.value = withSpring(1);
                break;
            case 'ZoomIn':

                scaleX.value = withSpring(1);
                scaleY.value = withSpring(1);
                break;
            default:

                break;
        }
    };

    const animatedStyle = useAnimatedStyle(() => {
        if (!isLoading) {
            const animation = media_Stack[media_Stack.length - 1].newPlaylistMedia.content[stack_Index.item_num].enAnime;
            switch (animation) {
                case 'FadeIn':

                    return { opacity: opacity.value };
                case "FlipInEasyX":
                    return { transform: [{ rotateY: '180deg' }, { rotateY: `${flipY.value}deg` }], };
                case "FlipInEasyY":
                    return { transform: [{ rotateX: '180deg' }, { rotateX: `${flipX.value}deg` }], };
                case 'PinwheelIn':
                    return { transform: [{ rotate: `${spin.value}deg` }] };
                case 'SlideInRight':
                    return { transform: [{ translateX: translateX.value }] };
                case 'SlideInUp':
                    return { transform: [{ translateY: translateY.value }] };
                case 'StretchInX':
                    return { transform: [{ scaleX: scaleX.value }] };
                case 'StretchInY':
                    return { transform: [{ scaleY: scaleY.value }] };
                case 'ZoomIn':
                    return { transform: [{ scaleX: scaleX.value }, { scaleY: scaleY.value }] };
                case "None":
                    return { opacity: 1 };
                default:
                    return {};
            }
        }
        return {};
    });

    useEffect(() => {
        async function findURL() {
            if (!isLoading) {
                let media_srcUri = FileSystem.documentDirectory +
                    media_Stack[media_Stack.length - 1].newPlaylistMedia.content[stack_Index.item_num].uploadedMedia.mediaId
                    + '.' + media_Stack[media_Stack.length - 1].newPlaylistMedia.content[stack_Index.item_num].uploadedMedia.fileName.split('.').pop();
                await findFileURL_forPlaying(media_srcUri);
            }
        }
        findURL()
    }, [isLoading, media_Stack, stack_Index.item_num]);

    useEffect(() => {
        if (!isLoading) {
            const animation = media_Stack[media_Stack.length - 1].newPlaylistMedia.content[stack_Index.item_num].enAnime;
            handleAnimation(animation);
        }
    }, [isLoading, media_Stack, stack_Index.item_num])



    useEffect(() => {
        let check_timer

        check_timer = setInterval(() => {
            //Log_MEDIA_debug("Checking the activation time for the next playlist")
            if ((activationDates_Stack.includes(moment().utc()) || moment().isSameOrAfter(activationDates_Stack[(activationDates_Stack.length - 1) - 1]))
                && activationDates_Stack.length > 1) {
                nextPlaylist_Index_accordingTO_ActivateDate();
                triggerMedia_Reload_new();
            }

        }, 1000);
        return function () {
            clearInterval(check_timer);
        }

    },);




    useEffect(function () {
        if (!isLoading) {

            if (media_Stack[media_Stack.length - 1].newPlaylistMedia.content[stack_Index.item_num].uploadedMedia.mediaType == 'slide') {
                const slide_timer = setTimeout(async function () { //For changing the playing slide to the next media after being displayed for its displaytime
                    if (media_Stack[media_Stack.length - 1].onceOnly == true
                        && media_Stack[media_Stack.length - 1].newPlaylistMedia.content.length - 1 == stack_Index.item_num) {
                        await patch_DL_PlayOnce_afterDL(media_Stack[media_Stack.length - 1].jobId, moment().utc(), 'playlist') //Report "Finished" to the backend as the current playOnce job has been finished 
                        nextPlaylist_Index_accordingTO_ActivateDate()
                    }
                    nextItem_Index_accordingTO_playMethod();


                }, media_Stack[media_Stack.length - 1].newPlaylistMedia.content[stack_Index.item_num].displayTime * 1000);
                return function () {
                    clearTimeout(slide_timer);
                }
            }
            else if (media_Stack[media_Stack.length - 1].newPlaylistMedia.content[stack_Index.item_num].uploadedMedia.mediaType == 'video') {
                setInit_vid_pt(0)
                setEnd_vid_pt(0)
                call_parseTime(media_Stack[media_Stack.length - 1].newPlaylistMedia.content[stack_Index.item_num].duration * 60000, media_Stack[media_Stack.length - 1])
            }
        }
    }, [isLoading, media_Stack, stack_Index.item_num])





    // Function to handle the event
    // Listen for the event
    async function HandleReload() {


        try {
            if (!isLoading) {
                if (media_Stack[media_Stack.length - 1].onceOnly == true) {
                    await patch_DL_PlayOnce_afterDL(media_Stack[media_Stack.length - 1].jobId, moment().utc(), 'playlist') //Report "finished" to the backend if the playing job is playOnce but the component needs to change 
                }
                else {
                    await storeAs_LocalData_AsyncStorage("Last_NonPlayOnce_playlist", media_Stack[media_Stack.length - 1])
                }
            }
            Log_MEDIA_error('HandleReload : idle')
            setIsLoading(true)
            //console.log("reload fetch_Starting_media")
            await fetch_Starting_media()
        }
        catch (error) {
            Log_MEDIA_error(`Something wrong happened while reloading the latest job list for playing playlist due to ${error}!`)
        }
    }



    async function HandleMediaReport() {
        const res = await getFrom_LocalData("CurMediaReport_Needed");
        setNeed_playlist_report(res) //change the setting for reporting currently playing media 
    }

    function HandleViewCurMedia() {

        if (!isLoading) {
            let fileID = media_Stack[media_Stack.length - 1].newPlaylistMedia.content[stack_Index.item_num].uploadedMedia.mediaId
            let file_local_name = fileID + '.' + media_Stack[media_Stack.length - 1].newPlaylistMedia.content[stack_Index.item_num].uploadedMedia.fileName.split('.').pop()
            showToast(`Currently playing media: ${file_local_name}`) //show a pop-message on the screen about the currently playing media 
        }

    }

    async function HandleModify() {
        if (!isLoading) {
            if (media_Stack[media_Stack.length - 1].onceOnly == false) {
                await Modify_playlist(media_Stack, false)
            }
            else if (media_Stack[media_Stack.length - 1].onceOnly == true) {

                await Modify_playlist(last_NonPlayOnce_playlist, false)

            }
        }
        else if (media_Stack.length == 0) {

            await Modify_playlist([], true)
        }

    }

    async function HandleForcePopping() {

        Log_MEDIA_debug("HandleForcePopping starts running")
        let new_mediaPlaylist_Stack = [...media_Stack]; // Create a copy

        new_mediaPlaylist_Stack.pop() //pop away the oldest job 
        setMedia_Stack(media_Stack => media_Stack = new_mediaPlaylist_Stack); //update the stack 

        await storeAs_LocalData_AsyncStorage("playlist_media_ArrList", new_mediaPlaylist_Stack);//save the updated stack in asyncstorage 
        activationDates_Stack.pop() //pop away the oldest job in activation time stack 
        setActivationDates_Stack(activationDates_Stack => activationDates_Stack = activationDates_Stack) //update the activation time stack 
        await storeAs_LocalData("Activation_Dates", activationDates_Stack); //save the new time stack into asyncstorage 
    }

    useEffect(() => {

        //Activate the eventListeners 
        playlist_emitter.on('Reload', HandleReload)
        playlist_emitter.on('MediaReport', HandleMediaReport);
        playlist_emitter.on('ViewCurMedia', HandleViewCurMedia);
        playlist_emitter.on("Modify", HandleModify)
        playlist_emitter.on("ForcePopping", HandleForcePopping)

        // Clean up the event listener when the component unmounts
        return () => {

            playlist_emitter.off('Reload', HandleReload)
            playlist_emitter.off('MediaReport', HandleMediaReport);
            playlist_emitter.off('ViewCurMedia', HandleViewCurMedia);
            playlist_emitter.off("Modify", HandleModify)
            playlist_emitter.off("ForcePopping", HandleForcePopping)

        };
    }, [isLoading]);

    async function check_PlaylistReported() {
        if (completeReported != undefined) {


            if (completeReported.includes(media_Stack[media_Stack.length - 1].jobId) == false) {
                let incompleteButPlay_Media = media_Stack[media_Stack.length - 1].incompleteButPlay
                let res = await patch_DL_Completed_afterDL(media_Stack[media_Stack.length - 1].jobId, moment().utc(), 'playlist', incompleteButPlay_Media);
                completeReported.push(media_Stack[media_Stack.length - 1].jobId)
                await storeAs_LocalData_AsyncStorage('CompletePlaylist_Reported', completeReported)
                if (res === null) {
                    Log_MEDIA_error('check_PlaylistReported : error catch empty')
                    setIsLoading(true)
                    await push_LastConstant_Playlist()
                    setIsLoading(false)

                }
            }
        }
    }


    function switch_on_MediaType(type) {
        switch (type) {
            case 'slide':
                return (

                    <Animated.View
                        style={[styles.slide_container, animatedStyle]}
                    >
                        <Image
                            source={{
                                uri: FileSystem.documentDirectory +
                                    media_Stack[media_Stack.length - 1].newPlaylistMedia.content[stack_Index.item_num].uploadedMedia.mediaId
                                    + '.' + media_Stack[media_Stack.length - 1].newPlaylistMedia.content[stack_Index.item_num].uploadedMedia.fileName.split('.').pop()
                            }}

                            style={styles.slide_pic_active}
                            //check if the media is missing or not 
                            //if the media is not missing but the component cannot play, usually it is because the file is corrupted or the player cannot initialize 
                            onError={async (error) => {
                                let file_local_name = FileSystem.documentDirectory
                                    + media_Stack[media_Stack.length - 1].newPlaylistMedia.content[stack_Index.item_num].uploadedMedia.mediaId
                                    + '.' + media_Stack[media_Stack.length - 1].newPlaylistMedia.content[stack_Index.item_num].uploadedMedia.fileName.split('.').pop()

                                if (media_Stack[media_Stack.length - 1].newPlaylistMedia.content[stack_Index.item_num].uploadedMedia.mediaType != "video") {
                                    await post_Abnormal_Alert(`Cannot load slide (mediaID: ${media_Stack[media_Stack.length - 1].newPlaylistMedia.content[stack_Index.item_num].uploadedMedia.mediaId}) due to ${JSON.stringify(error)}!`, "APP_STATE")
                                    let res = await FileSystem.getInfoAsync(file_local_name)

                                    if (res.exists == true) {

                                        Log_MEDIA_error(`Slide (mediaID: ${media_Stack[media_Stack.length - 1].newPlaylistMedia.content[stack_Index.item_num].uploadedMedia.mediaId}) was corrupted!`)

                                    }
                                    else {
                                        Log_MEDIA_error(`Slide (mediaID: ${media_Stack[media_Stack.length - 1].newPlaylistMedia.content[stack_Index.item_num].uploadedMedia.mediaId}) is missing!`)

                                    }
                                    nextItem_Index_accordingTO_playMethod()

                                }
                            }}
                            onLoad={async function () {

                                await postCurMedia();
                                await postCurCC();
                            }}
                        />
                    </Animated.View>


                )

            case 'video':
                //nextItem_Index_accordingTO_playMethod()
                return (
                    <Animated.View
                        style={[styles.video_container, animatedStyle]}
                    >
                        <Video
                            ref={video}
                            source={{
                                uri: FileSystem.documentDirectory
                                    + media_Stack[media_Stack.length - 1].newPlaylistMedia.content[stack_Index.item_num].uploadedMedia.mediaId
                                    + '.' + media_Stack[media_Stack.length - 1].newPlaylistMedia.content[stack_Index.item_num].uploadedMedia.fileName.split('.').pop()
                            }}
                            style={styles.video_clip_active} resizeMode='contain'
                            positionMillis={init_vid_pt}

                            shouldPlay={true}
                            useNativeControls={true}
                            progressUpdateIntervalMillis={1000}
                            //check if the media is missing or not 
                            //if the media is not missing but the component cannot play, usually it is because the file is corrupted or the player cannot initialize 
                            onError={async (error) => {
                                let file_local_name = FileSystem.documentDirectory
                                    + media_Stack[media_Stack.length - 1].newPlaylistMedia.content[stack_Index.item_num].uploadedMedia.mediaId
                                    + '.' + media_Stack[media_Stack.length - 1].newPlaylistMedia.content[stack_Index.item_num].uploadedMedia.fileName.split('.').pop()

                                if (media_Stack[media_Stack.length - 1].newPlaylistMedia.content[stack_Index.item_num].uploadedMedia.mediaType != "slide") {
                                    await post_Abnormal_Alert(`Cannot load video (mediaID: ${media_Stack[media_Stack.length - 1].newPlaylistMedia.content[stack_Index.item_num].uploadedMedia.mediaId}) due to ${JSON.stringify(error)}!`, "APP_STATE")
                                    let res = await FileSystem.getInfoAsync(file_local_name)

                                    if (res.exists == true) {
                                        Log_MEDIA_error(`Video (mediaID: ${media_Stack[media_Stack.length - 1].newPlaylistMedia.content[stack_Index.item_num].uploadedMedia.mediaId}) was corrupted!`)
                                    }
                                    else {
                                        Log_MEDIA_error(`Video (mediaID: ${media_Stack[media_Stack.length - 1].newPlaylistMedia.content[stack_Index.item_num].uploadedMedia.mediaId}) is missing!`)
                                    }
                                    //await reDownloadItem(media_Stack[media_Stack.length - 1].newPlaylistMedia.content[stack_Index.item_num])
                                    nextItem_Index_accordingTO_playMethod()

                                }

                            }}

                            onPlaybackStatusUpdate={async video_status => {
                                //load the next media after finishing the video 

                                //setStatus(video_status);
                                //console.log('Current position (ms):', video_status.positionMillis);

                                if (video_status.didJustFinish == true
                                    || video_status.positionMillis >= end_vid_pt) {
                                    //setLast_playingPt(video_status.positionMillis)

                                    if (media_Stack[media_Stack.length - 1].onceOnly == true
                                        && media_Stack[media_Stack.length - 1].newPlaylistMedia.content.length - 1 == stack_Index.item_num) {
                                        await patch_DL_PlayOnce_afterDL(media_Stack[media_Stack.length - 1].jobId, moment().utc(), 'playlist')
                                        nextPlaylist_Index_accordingTO_ActivateDate()

                                    }

                                    nextItem_Index_accordingTO_playMethod();
                                }

                            }}
                            onLoad={async function () {
                                await video.current.setProgressUpdateIntervalAsync(1000)
                                await postCurMedia();
                                await postCurCC();
                                call_parseTime(media_Stack[media_Stack.length - 1].newPlaylistMedia.content[stack_Index.item_num].duration * 60000, media_Stack[media_Stack.length - 1])

                                video.current.playFromPositionAsync(init_vid_pt)



                            }}
                        />
                    </Animated.View>

                )
            default:
                break;
        }

    }


    if (!isLoading) {
        return (
            <View style={styles.container}>
                {switch_on_MediaType(media_Stack[media_Stack.length - 1].newPlaylistMedia.content[stack_Index.item_num].uploadedMedia.mediaType)}
            </View>
        )

    }
    else {
        return (
            <View style={styles.container}>
                <Image
                    source={isPortrait ? require('../../../assets/LiWanJi_edited.png') : require('../../../assets/LiWanJi.jpg')}
                    style={[styles.slide_pic_dummy, { contentFit: 'contain' }]}
                />

            </View>
        )

    }


}
