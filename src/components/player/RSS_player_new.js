import { EventEmitter } from "events";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { Text, View } from 'react-native';
import MarqueeView from 'react-native-marquee-view';
import { patch_DL_Completed_afterDL, patch_DL_Failed_afterDL, patch_DL_PlayOnce_afterDL } from '../../background_jobs/download/DownloadMedia_ReportStatus.js';
import { Log_RSS_error, Log_RSS_info, Log_RSS_warn } from "../../util/helper/Log_shortcut.js";
import { getFrom_LocalData_AsyncStorage, storeAs_LocalData_AsyncStorage } from '../../util/stats/info/Async_Machine_Info.js';

import { MediaReport } from "../../API/MediaReport.js";
import { Modify_CC } from "../../background_jobs/modify_FromConfig/Modify_JSON.js";
import { styles } from '../../style/RSS_playerCSS.js';
import { Tidyup_Emptylist } from "../../util/helper/Tidyup_Emptylist.js";
import { getFrom_LocalData, getFrom_LocalData_LessLogging, storeAs_LocalData, storeAs_LocalData_LessLogging } from "../../util/stats/info/Secured_Machine_Info.js";
/**
 * Author: Ken Lung, Time: 18/03/2024
 * renew_RSS_time is a hardcoded global variable
 * for setting up the time needed before moving to the next CC job 
 * (measured in MINUTE)
 */
const renew_RSS_time = 30

/**
 * Author: Ken Lung, Time: 18/03/2024
 * Event emitter to detect new changes
 * on the RSS component sent by the backend server
 * 
 */
const CC_emitter = new EventEmitter();


/* Author Pedro Wong, 7/18/2024
hard coded filtera and filterb for handling cases where RSS feed don't have <title> tags within Item
if this function works in general cases of RSS feed, then we can just use this instead of the XMLParser
*/

function sleep(milliseconds) {
    return new Promise(resolve => setTimeout(resolve, milliseconds));
  }



function filtera(x, y, z) {
    let result = "";
    let i = 0;
    while (i < x.length) {
        // Check if the current substring matches 'y'
        if (x.substring(i, i + y.length) === y) {
            i += y.length; // Move past 'y'
            let temp = "";
            // Keep accumulating characters until 'z' is found
            while (x.substring(i, i + z.length) !== z && i < x.length) {
                temp += x[i];
                i++;
            }
            i += z.length; // Move past 'z'
            result += temp + "   "; // Add the found text to result, adjust separator as needed
        } else {
            i++;
        }
    }
    return result.trim(); // Trim the final result to remove any trailing spaces
}

function filterb(x) {
    let result = "";
    let i = 0;
    x += " ";
    while (i < x.length-1) {
        // Check if the current substring matches 'y'
        if (x[i] === "<") {
            while (x[i] !== ">") {
                i++;}
            result += " ";}
            if (x[i] !== ">")
            {
            result += x[i];
            }        
            i++;
        }
        
    result = result.replace(/&amp;/g, '&')
                    .replace(/&quot;/g, '"')
                    .replace(/&#39;/g, "'")
                    .replace(/&lt;/g, '<')
                    .replace(/&gt;/g, '>')
                    .replace(/&cent;/g, '¢')
                    .replace(/&pound;/g, '£')
                    .replace(/&yen;/g, '¥')
                    .replace(/&euro;/g, '€');
                    
    return result;
}

function filterHtmlSpaces(htmlString) {
    // Regular expression to find spaces between HTML tags
    const regex = />(\s+)</g;
    // Replace found spaces with just the closing and opening tags
    // const filteredHtml = htmlString.replace(regex, '><');
    const filteredHtml = htmlString.replace(/>(\s+)</g, '><')
        .replace(/&nbsp;|&ensp;|&emsp;|&thinsp;|&zwnj;|&zwj;/g, '');
    return filteredHtml;
}

function stringToMapConvertor(string) {
    let map = new Map();
    let array = string.split("   ");
    for (let i = 0; i < array.length; i++) {
        map.set(i, array[i]);
    }
    return map;
}

export function triggerCC_Reload_new() {
    CC_emitter.emit('Reload', true) //Exporting to Fetch_API.js for loading new RSS job stack
}

export function triggerNewDisplay_RSS() {
    CC_emitter.emit('NewDisplayRSS', true) //Exporting to Fetch_API.js for loading new visual setting for the RSS componenet 
}

export function triggerModify_CC_new() {
    CC_emitter.emit('Modify', true) //Exporting to Fetch_API.js for changing the currently playing RSS job's configuration
}

export function triggerForcePopping_Deleted_CC() {
    CC_emitter.emit('ForcePopping', true) //Exporting to Fetch_API.js for loading the new stack of media playlist job
}

export function triggerReportMedia_new_CC() {
    CC_emitter.emit('MediaReport', true) //Exporting to Fetch_API.js for changing whether the component needs to report the media that is playing at the moment 
}

export default function RSS_player_new() {

    /**
     * Author: Ken Lung, Time:  18/03/2024
     * useState variables for operation:
     * 
     * activationDates_CC: an array to specfically record the activation time of the current and future CC playing job(s)
     * to let the component to check whether any job have reached its activation time 
     * without resulting extensive search to CCPlaylist
     */
    const [CC_Stack, setCC_Stack] = useState([]); //An array acting as a stack that stores the current and future RSS playing job(s)
  

    const [content_series, setContent_series] = useState(null); //An array of all the <title> tags in the RSS URL as string
    const cur_RssList_Content_series = list_rss_title_series(); //A map of <Text> tag based on content_series, displayed in the <MarqueeView> element

    const [speed_multiplyer, setSpeed_multiplyer] = useState(1); //An integer for determining how fast should the <Text> tags in the <MarqueeView> element should go
    const [need_CC_report, setNeed_CC_report] = useState(true); //checking whether the component needs to report the currently playing media to the backend

    const [activationDates_CC_Stack, setActivationDates_CC_Stack] = useState([]) //An array acting as a stack that stores the current and future RSS playing job(s)' activation time
    const [last_NonPlayOnce_CC, setLast_NonPlayOnce_CC] = useState([]) //An JSON for recording the latest constant RSS playlist 
    const [completeReported_CC, setCompleteReported_CC] = useState([]) //An JSON for recording the played RSS playing jobs' jobId

    const [isLoading, setIsLoading] = useState(true); //A boolean to shows whether the component has properly loaded Media_CC
    const [showRSS, setShowRSS] = useState(true) //A boolean check if the <MarqueeView> needs to be shown
    const space = "      " //A long string of space for separating the <Text> tags in cur_RssList_Content_series


    /**
   * Author: Ken Lung, Time: 18/03/2024
   * Functions that are used in useEffect hooks for setting up the useState variables 
   * using getFrom_localData for getting persistent data from SecureStore
   * or getFrom_LocalData_AsyncStorage for getting persistent data from SecureStore
   * 
   * If the resulting promise from the above two functions return null
   * It means there have not been a value stored in the respective key 
   * then the hook will store the default values for the respective key 
   * 
   * Finally, set the returning value or the default value for the useState variables 
   */
    async function fetchShowRSS() {
        try {
            const res = await getFrom_LocalData("showRSS");
            if (res === null) {
                await storeAs_LocalData("showRSS", true);
                setShowRSS(true);
            } else {
                setShowRSS(res);
            }
        } catch (error) {
            // Handle exceptions (e.g., log the error)
            Log_RSS_error(`Error fetching showRSS:" ${error}`)
        }

    }
    async function fetchCompleteCC_Reported() {
        try {
            const res = await getFrom_LocalData_AsyncStorage("CompleteCC_Reported");
            //console.log("hello"+ res)
            if (res === null || res === undefined) {
                await storeAs_LocalData_AsyncStorage("CompleteCC_Reported", []);
                setCompleteReported_CC([]);
            } else {
                setCompleteReported_CC(res);
            }
            return res
        } catch (error) {
            // Handle exceptions (e.g., log the error)
            Log_RSS_error(`Error fetching CompleteCC_Reported:" ${error}`)
        }

    }


    async function fetch_Activation_Dates() {
        try {
            const res = await getFrom_LocalData_LessLogging("Activation_Dates_CC");
            if (res === null || res === undefined) {
                await storeAs_LocalData_LessLogging("Activation_Dates_CC", []);
                setActivationDates_CC_Stack([]);
            } else {
                setActivationDates_CC_Stack(res);
            }
            return res
        } catch (error) {
            // Handle exceptions (e.g., log the error)
            Log_RSS_error(`Error fetching Activation_Dates_CC:" ${error}`)
        }

    }


    async function fetch_Last_NonPlayOnce_CC() {
        try {
            const res = await getFrom_LocalData_AsyncStorage("Last_NonPlayOnce_CC");
            if (res === null || res === undefined) {
                await storeAs_LocalData_AsyncStorage("Last_NonPlayOnce_CC", []);
                setLast_NonPlayOnce_CC([]);
            } else {
                setLast_NonPlayOnce_CC(last_NonPlayOnce_CC => last_NonPlayOnce_CC = res);
            }
            return res
        } catch (error) {
            // Handle exceptions (e.g., log the error)
            Log_RSS_error(`Error fetching Last_NonPlayOnce_CC:" ${error}`)
        }
    }

    async function fetch_Starting_CC() {
        try {
            setIsLoading(true)
            let res = await getFrom_LocalData_AsyncStorage('CC_media_ArrList');
            if (res != undefined && res != null) {
                let ok_to_play = await fetchIndexes(res)
                if (ok_to_play == true) {
                    setIsLoading(false);
                }
                getRss_ContentSeries(res[res.length - 1].newCCMedia.content[0].url, res)
            }
        } catch (error) {
            Log_RSS_error(`Error fetching CC_media:" ${error}`)

        }
    }

    async function fetch_mediaReport_Needed() {
        try {
            const res = await getFrom_LocalData_LessLogging("CurMediaReport_Needed");
            if (res === null || res === undefined) {
                await storeAs_LocalData_LessLogging("CurMediaReport_Needed", false);
                setNeed_CC_report(false);
            } else {
                setNeed_CC_report(res);
            }
            return res
        } catch (error) {
            // Handle exceptions (e.g., log the error)
            Log_MEDIA_error(`Error fetching need_playlist_report:" ${error}`)
        }
    }

    function fetchSpeed(multi) {
        setSpeed_multiplyer(speed_multiplyer => speed_multiplyer = multi)
    }


    /**
       * Author: Ken Lung, Time: 18/03/2024
       * fetchIndexes is a function for determining the latest activatable and playable RSS playlist
       * It first loops through the received RSS playlist(s) to check which one is a valid RSS playlist
       * if there is one, it will start playing it and storing the index to cur_CC
       * else send an error respose to the backend server
       * 
       * This function runs on response, but it is basically media_CC read from getFrom_LocalData_AsyncStorage on initial rendering 
       * 
       * detailed operation go-through will be shown below 
       */

    async function fetchIndexes(response) {

        let latestNonPlayOnce_CC = await getFrom_LocalData_AsyncStorage("Last_NonPlayOnce_CC") //Read Last_NonPlayOnce_CC to select that job 
        //if there is no RSS playing job that is playable and is after the latest activation date
        let nextCC_activate_Date;
        let choosen_CC = -1;

        /**
         * Author: Ken Lung, Time: 18/03/2024
         * loop through the responsding array's json(s) to find a legit playlist
         * by checking the activation time of the job
         */
        for (let legit_CC = response.length - 1; legit_CC != -1; legit_CC--) { //loop through the responsding array's json(s) to find a activatable playlist



            if (response[legit_CC].newCCMedia.content.length == 0) { //if the job has no item, skip that job

                if (completeReported_CC.includes(response[legit_CC].jobId) == false) {
                    completeReported_CC.push(response[legit_CC].jobId)
                    await storeAs_LocalData_AsyncStorage('CompleteCC_Reported', completeReported_CC)
                }
                continue
            }


            /**
            * Author: Ken Lung, Time: 18/03/2024
            * if the job is a new job ready to activate (nextCC_activate_Date is same or after last_NonPlayOnce_CC)
            * or it is the last constant RSS playing job, 
            * it is the first index in the responsding array's json(s) to be choosen for playing
            */

            else {
                //console.log(response[legit_CC].newCCMedia.content)
                nextCC_activate_Date = response[legit_CC].newCCMedia.activatedAt

                if (moment().isSameOrAfter(moment(nextCC_activate_Date))) { //If the job is a new job ready to activate (nextCC_activate_Date is same or after last_NonPlayOnce_CC)
                    choosen_CC = legit_CC
                    break;

                }
            }
        }



        if (choosen_CC != -1) {

            if (response[choosen_CC].onceOnly == false) {
                if (latestNonPlayOnce_CC === null || latestNonPlayOnce_CC == []) {
                    setLast_NonPlayOnce_CC(last_NonPlayOnce_CC => last_NonPlayOnce_CC = response[choosen_CC]);
                    await storeAs_LocalData_AsyncStorage("Last_NonPlayOnce_CC", response[choosen_CC]);
                }
                else if (latestNonPlayOnce_CC.jobId != response[choosen_CC].jobId) {
                    setLast_NonPlayOnce_CC(last_NonPlayOnce_CC => last_NonPlayOnce_CC = response[choosen_CC]);
                    await storeAs_LocalData_AsyncStorage("Last_NonPlayOnce_CC", response[choosen_CC]);
                }
            }

            fetchSpeed(response[choosen_CC].newCCMedia.speed) //Change the speed of the <MarqueeView> element by changing speed_multipler

            let new_response = await Tidyup_Emptylist(response, "CC") //Clear out any job with empty content 
            setCC_Stack(CC_Stack => CC_Stack = new_response); //Change the stack that the component is using for playing RSS
            await storeAs_LocalData_AsyncStorage("CC_media_ArrList", new_response); //Save the new stack for playing RSS in the AsyncStorage
            await put_ActivationDates(new_response);
            await get_All_RssTitle(new_response)
            return true
        }
        else {
            Log_RSS_error(`There is no valid RSS to play from due to they are either played PlayOnce playlist or incomplete playlist!`)
            setIsLoading(true)
            return false
        }
    }

    async function put_ActivationDates(response) {
        let new_activationDates = []
        for (let remaining_num = response.length - 1; remaining_num != -1; remaining_num--) {
            new_activationDates.push(response[remaining_num].newCCMedia.activatedAt); //Push the activation time from the RSS job stack into this array
        }
        new_activationDates.reverse()//Reverse the order of the array to make it save from the earliest on top and latest at the bottom
        setActivationDates_CC_Stack(activationDates_CC_Stack => activationDates_CC_Stack = new_activationDates) //Change the stack that the component using to check the activation time of the jobs 

        await storeAs_LocalData("Activation_Dates_CC", new_activationDates); //Save the new stack for RSS jobs' activation time in the AsyncStorage

    }

    /**
     * Author: Ken Lung, Time: 18/03/2024
     * useEffect hook for setting up the useState variables on initial rendering
     * return a clean-up function to empty the content for the useState variable 
     * to release memory
     */

    useEffect(() => {
        fetchCompleteCC_Reported()
        fetch_Last_NonPlayOnce_CC()
        fetchCompleteCC_Reported()
        fetch_Activation_Dates()
        fetch_mediaReport_Needed()
        return (() => {
            setContent_series([])
            setCompleteReported_CC([])
            setActivationDates_CC_Stack([])

        })
    }, [])

    useEffect(() => {
        fetchShowRSS()
        fetch_Starting_CC()
        return (() => {
            setCC_Stack([]);
        })
    }, [])

    /**
     * Author: Ken Lung, Time: 18/03/2024
     * an async function to get the content of the RSS URL sent by the backend server 
     * it extracts all the <title> tags of the RSS URL if the response status is normal (typically starts with 20X)
     * else it will record an error message based on the status code
     * 
     * XML parser will parse the content and get all the <title> tags in the RSS URL
     * and log the RSS URL has been extracted 
     * 
     */

    async function getRss_ContentSeries(link, new_CC ,counter = 0) {
        try {
            const response = await fetch(link, {
                method: 'GET',
                headers: {
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3'
                },
            });
            if (response.status > 299) {
                if (response.status >= 400 && response.status <= 499) {
                    switch (response.status) {
                    case 400:
                        Log_RSS_error('Request denied from the website code : 400');
                        break;
                    default:
                        Log_RSS_error(`Client error with ${response.status} status while requesting RSS content! (Supporting details: The targeted website ${link} does not allow access)`);
                        break;
                    }} else if (response.status >= 500 && response.status <= 599) {
                    switch (response.status) {
                        case 500:
                            Log_RSS_error(`Server error with ${response.status} status! (Supporting details: The server is currently facing some internal difficulties and not available)`);
                            break;
                        default:
                            Log_RSS_error(`Server error with ${response.status} status! (Supporting details: The server is currently facing some difficulties and not available)`);
                            break;
                    }
                }
                return '';
            } else {
  
                let text = await response.text();
                text = text.replace(/\s+/g, ' ').trim();
                text = filterHtmlSpaces(text);

                /*
                const parser = new XMLParser();let obj = parser.parse(text);
                let rtn_rss_map = new Map();

                if (obj.rss && obj.rss.channel) {   //archived for refactoring purpose 7/19/2024
                    rtn_rss_map = obj.rss.channel.item.reduce((map, item, index) => {
                        map.set(index, item.title);
                        return map;
                    }, new Map());
                    rtn_rss_string = Array.from(rtn_rss_map.values()).join(space);
                } else */
                let x = "";
                let y = "";
                x = filterb(filtera(text, "<item><title><![CDATA[" ,"]]></title>"));
                y = filterb(filtera(text, "<description><![CDATA[" ,"]]></description>"));
                if (x === "") {
                    rtn_rss_string = y.trimStart();
                } else {
                    rtn_rss_string = x.trimStart();
                }
                const MAX_SIZE = 5000;
                if (rtn_rss_string.length > MAX_SIZE) {
                    Log_RSS_info('The size of the RSS content is too large, only the first '+ MAX_SIZE +' characters will be displayed');
                    rtn_rss_string = rtn_rss_string.substring(0, MAX_SIZE);
                    //filter the rtn_rss_string that ends with a complete sentence
                    let lastIndexOf = rtn_rss_string.lastIndexOf(' ');
                    if (lastIndexOf !== -1) {
                        rtn_rss_string = rtn_rss_string.substring(0, lastIndexOf + 1);
                    }
                }


                let rtn_rss_map = new Map();
                rtn_rss_map = stringToMapConvertor(rtn_rss_string);
                rtn_rss_string = Array.from(rtn_rss_map.values()).join(space);
                

                Log_RSS_info(`Succeed in requesting titles from RSS the targeted website (${link})!`);
                let CC_id = new_CC[new_CC.length - 1].newCCMedia.content[0].rssId; 
                Log_RSS_warn('The current RSS job is playing CCid: ' + CC_id);
                //assume the source will be the CC_Stack
                 //await MediaReport("CC", CC_id , moment().utc());
                return rtn_rss_string;
            }
        } catch (err) {
            if (err === 'AbortError') {
                Log_RSS_error(`The request to the targeted website ${link} has been aborted due to timeout!`);}
            else if (err === 'TypeError') {
                Log_RSS_warn('Detected type error '+ count +' times, proceed to retry...')
                if (counter < 300000 ){
                return await getRss_ContentSeries(link, new_CC, counter + 1);}
                else{ 
                    Log_RSS_error('RSS request failed after '+counter +' tries due to TypeError!');
                    await check_CCReported(false, new_CC);
                    return undefined;}

            }
            Log_RSS_error(`An unexpected error happened while requesting content from the targeted website ${link} due to ${err}!`);
            await check_CCReported(false, new_CC);
            return undefined
        }
    }

    async function get_All_RssTitle(new_CC) {

        let arrangement;
        let result;

        if (!isLoading) {
            arrangement = new_CC[new_CC.length - 1].newCCMedia.playMode
            switch (arrangement) { //Switch statement to detect the playing method of the job 
                case 'REVERSE':
                    result = await arranging_InREVERSE(new_CC) //Get the title(s) as a string, arranged in reverse
                    break;
                case 'SHUFFLE':
                    result = await arranging_InSHUFFLE(new_CC) //Get the title(s) as a string, arranged in shuffle
                    break;
                case 'ORDER':
                    result = await arranging_InORDER(new_CC) //Get the title(s) as a string, arranged in order
                    break;

            }
            await check_CCReported(true, new_CC) //Check was the current job repeat at least once already 
            setContent_series(result) //Set the title(s) string in the content_series 
            postCurCC();
        }
    }
    async function arranging_InORDER(new_CC) {
        let result = "";
        for (let rss_cnt = 0; rss_cnt != new_CC[new_CC.length - 1].newCCMedia.content.length; rss_cnt++) {

            let response = await getRss_ContentSeries(new_CC[new_CC.length - 1].newCCMedia.content[rss_cnt].url, new_CC);
            
            if (response === undefined) {
                return '';}
            result += response;

        }
        
        return result
    }
    async function arranging_InREVERSE(new_CC) {
        let result = "";
        for (let rss_cnt = new_CC[new_CC.length - 1].newCCMedia.content.length - 1; rss_cnt != -1; rss_cnt--) {

            let response = await getRss_ContentSeries(new_CC[new_CC.length - 1].newCCMedia.content[rss_cnt].url, new_CC);
            if (response === undefined) {
                return '';}
            result += response;

        }
        return result
    }
    async function arranging_InSHUFFLE(new_CC) {
        let responses = [];
        try {
            for (let rss_cnt = 0; rss_cnt != new_CC[new_CC.length - 1].newCCMedia.content.length; rss_cnt++) {

                let response = await getRss_ContentSeries(new_CC[new_CC.length - 1].newCCMedia.content[rss_cnt].url, new_CC);
                if (response === undefined) {
                    return '';}
                responses.push(response);
    
            }            

            for (let i = responses.length - 1; i > 0; i--) {
                let j = Math.floor(Math.random() * (i + 1));
                [responses[i], responses[j]] = [responses[j], responses[i]];
            }
            return responses.join(space);
        } catch (error) {
            Log_RSS_error("Failed to shuffle RSS content:", error);
            return arranging_InORDER(new_CC); // or handle the error as appropriate
        }
    }




    async function check_CCReported(result, CC_report) {
        let res;
        if (completeReported_CC != undefined) {

            if (completeReported_CC.includes(CC_report[CC_report.length - 1].jobId) == false) { //If the RSS job have not been reported once  

                if (result == true) { 
                    let incompleteButPlay_CC = CC_report[CC_report.length-1].incompleteButPlay;
                    res = await patch_DL_Completed_afterDL(CC_report[CC_report.length - 1].jobId, moment().utc(), 'CC', incompleteButPlay_CC); //Report "Complete" to the backend for loading RSS job
                }
                else {
                    res = await patch_DL_Failed_afterDL(CC_report[CC_report.length - 1].jobId, "Cannot load the content from the targeted URL!", 'CC'); //Report "Failed" to the backend for loading RSS job
                }
                completeReported_CC.push(CC_report[CC_report.length - 1].jobId) //Push the jobId of the currently playing RSS job 
                await storeAs_LocalData_AsyncStorage('CompleteCC_Reported', completeReported_CC) //Store the jobId array of RSS job(s) in the AsyncStorage

                if (res === null) {
                    await push_LastConstant_CC()

                }

            }
        }
    }

    async function push_LastConstant_CC() {
        let new_mediaCC = [...CC_Stack]; // Create a copy
        new_mediaCC.push(last_NonPlayOnce_CC) //push back the last constant job in 

        setCC_Stack(CC_Stack => CC_Stack = new_mediaCC);
        await storeAs_LocalData_AsyncStorage("CC_media_ArrList", new_mediaCC); //save the updated stack in asyncstorage 

        activationDates_CC_Stack.push(last_NonPlayOnce_CC.newCCMedia.activatedAt) //push back the last constant job's activation time 
        setActivationDates_CC_Stack(setActivationDates_CC_Stack => setActivationDates_CC_Stack = setActivationDates_CC_Stack) //update the usestate variable
        await storeAs_LocalData("Activation_Dates_CC", activationDates_CC_Stack); //save the updated version in to async storage 



    }


    async function nextCC_Index_accordingTO_ActivateDate() {
        let new_media_CC_Stack = [...CC_Stack]; // Create a copy
        let latestNon_playOnce = await getFrom_LocalData_AsyncStorage("Last_NonPlayOnce_CC")

        if (CC_Stack.length > 1) { //If there are more than 1 RSS job in the stack 

            let target = (CC_Stack.length - 1) - 1 //Shift down the index of the stack by one 

            let nextList_activate_Date = CC_Stack[target].newCCMedia.activatedAt
            if (moment().isSameOrAfter(moment(nextList_activate_Date)) == true) { // needs to activate the next playlist

                if (latestNon_playOnce.jobId != CC_Stack[CC_Stack.length - 1].jobId && CC_Stack[CC_Stack.length - 1].onceOnly == false) {
                    setLast_NonPlayOnce_CC(last_NonPlayOnce_CC => last_NonPlayOnce_CC = CC_Stack[CC_Stack.length - 1]); //Set the last_NonPlayOnce_CC to be the current job 
                    await storeAs_LocalData_AsyncStorage("Last_NonPlayOnce_CC", CC_Stack[CC_Stack.length - 1]); //Store the new last_NonPlayOnce_CC to the device 
                }

                new_media_CC_Stack = [...CC_Stack]; // Create a copy


                new_media_CC_Stack.pop() //pop away the oldest job 

                setCC_Stack(CC_Stack => CC_Stack = new_media_CC_Stack); //Update the RSS job stack 
                await storeAs_LocalData_AsyncStorage("CC_media_ArrList", new_media_CC_Stack); //Save the new RSS job stack

                activationDates_CC_Stack.pop() //Pop out the oldest RSS job's activation time  
                setActivationDates_CC_Stack(activationDates_CC_Stack => activationDates_CC_Stack = activationDates_CC_Stack) //Update the RSS job activation time stack 
                await storeAs_LocalData("Activation_Dates_CC", activationDates_CC_Stack); //Save the new RSS job activation time stack
                await get_All_RssTitle(new_media_CC_Stack)

            }
            else if (moment().isSameOrAfter(moment(nextList_activate_Date)) == false) { // activate the old one 

                new_media_CC_Stack = [...CC_Stack]; // Create a copy

                new_media_CC_Stack.pop()
                new_media_CC_Stack.push(latestNon_playOnce) //push back the last constant job in 
                setCC_Stack(CC_Stack => CC_Stack = new_media_CC_Stack); //Update the RSS job stack 
                await storeAs_LocalData_AsyncStorage("CC_media_ArrList", new_media_CC_Stack); //Save the new RSS job stack

                activationDates_CC_Stack.pop() //Pop out the oldest RSS job's activation time  
                activationDates_CC_Stack.push(latestNon_playOnce.newCCMedia.activatedAt)
                setActivationDates_CC_Stack(activationDates_CC_Stack => activationDates_CC_Stack = activationDates_CC_Stack) //Update the RSS job activation time stack 
                await storeAs_LocalData("Activation_Dates_CC", activationDates_CC_Stack); //Save the new RSS job activation time stack

                await get_All_RssTitle(new_media_CC_Stack)


            }


        }

        else if (CC_Stack.length <= 1) { //If there is only 1 or fewer RSS job in the stack
            if (CC_Stack[CC_Stack.length - 1].onceOnly == false) { //Keep playing the last job in stack if it is not a playOnce RSS job
          
                setIsLoading(false)
                return true
            }
            else if (CC_Stack[CC_Stack.length - 1].onceOnly == true) { //Pop the last job out and push back the latest non-playOnce job back into the stacks, if it exists

                new_media_CC_Stack = [...CC_Stack]; // Create a copy
                new_media_CC_Stack.pop()
                new_media_CC_Stack.push(latestNon_playOnce)
                setCC_Stack(CC_Stack => CC_Stack = new_media_CC_Stack);
                await storeAs_LocalData_AsyncStorage("CC_media_ArrList", new_media_CC_Stack);
            
                activationDates_CC_Stack.pop()
                activationDates_CC_Stack.push(latestNon_playOnce.newCCMedia.activatedAt)
                setActivationDates_CC_Stack(activationDates_CC_Stack => activationDates_CC_Stack = activationDates_CC_Stack)
                await storeAs_LocalData("Activation_Dates_CC", activationDates_CC_Stack);
                await get_All_RssTitle(new_media_CC_Stack)

                setIsLoading(false)

            }
        }
        else {

            Log_RSS_warn(`There is no valid CC to play from due to they are either played PlayOnce CC or incomplete CC!`) //Log message that shows the player has no playable RSS job
            setIsLoading(true) //Set IsLoading to true to show an empty blue bar for the RSS player 

        }
    }

    async function postCurCC() {
        //a random value for the RSS job's id
        val = Math.floor(Math.random() * new_CC[new_CC.length - 1].newCCMedia.content.length);
         /*if (need_CC_report == true) */ { //only report when the backend require to report currently playing media
            let CC_id = new_CC[new_CC.length - 1].newCCMedia.content[val].rssId;
            Log_RSS_warn('The current RSS job is playing CCid: ' + CC_id);
            
            //assume the source will be the CC_Stack
            await MediaReport("CC", CC_id , moment().utc());

            setLast_NonPlayOnce_CC(last_NonPlayOnce_CC => last_NonPlayOnce_CC = CC_Stack[CC_Stack.length - 1]);
            await storeAs_LocalData_LessLogging("Last_NonPlayOnce_CC", CC_Stack[CC_Stack.length - 1]);
            
            
        }
    }

    async function HandleMediaReport() {
        const res = await getFrom_LocalData("CurMediaReport_Needed");
        setNeed_CC_report(res) //change the setting for reporting currently playing media 
    }


    async function HandleReload() {
        if (!isLoading) {
            if (CC_Stack[CC_Stack.length - 1].onceOnly == true) {
                //Report "Finished" when the current job is a playOnce RSS job but the component needs to load a new stack of RSS job
                await patch_DL_PlayOnce_afterDL(CC_Stack[CC_Stack.length - 1].jobId, moment().format(), 'CC')
            }
        }
        setIsLoading(true)
        // Fetch the first playing RSS job from the new RSS stack 
        await fetch_Starting_CC()
    }

    async function HandleNewDisplay() {
        let CC_display = await getFrom_LocalData('showRSS') //Change whether the RSS component should be seen or not 
        setShowRSS(CC_display)
    }

    async function HandleModify() {

        const strange_mediaCC = await getFrom_LocalData_AsyncStorage("CC_media_ArrList")
        if (strange_mediaCC === null || strange_mediaCC.length == 0) {


            await Modify_CC([], true)
        }

        else if (strange_mediaCC[strange_mediaCC.length - 1].onceOnly == false) {

            Modify_CC(strange_mediaCC, false)
        }
        else if (strange_mediaCC[strange_mediaCC.length - 1].onceOnly == true) {

            await Modify_CC(last_NonPlayOnce_CC, false)

        }



    }
    async function HandleForcePopping_CC() {
        let new_CC_Stack = [...CC_Stack]; // Create a copy

        new_CC_Stack.pop() //pop away the oldest job 
        setCC_Stack(CC_Stack => CC_Stack = new_CC_Stack); //update the stack 

        await storeAs_LocalData_AsyncStorage("CC_media_ArrList", new_mediaPlaylist_Stack);//save the updated stack in asyncstorage 
        activationDates_CC_Stack.pop() //pop away the oldest job in activation time stack 
        setActivationDates_CC_Stack(activationDates_CC_Stack => activationDates_CC_Stack = activationDates_CC_Stack) //update the activation time stack 
        await storeAs_LocalData("Activation_Dates_CC", activationDates_CC_Stack); //save the new time stack into asyncstorage 
    }


    useEffect(() => {
        if (!isLoading) {
            if (CC_Stack[CC_Stack.length - 1].onceOnly == true) {
                const timer = setTimeout(async () => {

                    await patch_DL_PlayOnce_afterDL(CC_Stack[CC_Stack.length - 1].jobId, moment().format(), 'CC');
                    nextCC_Index_accordingTO_ActivateDate()

                }, Math.round(CC_Stack[CC_Stack.length - 1].newCCMedia.content.length * 5 * 60 * 1000 / (speed_multiplyer * 4))); 

                return function () {
                    clearTimeout(timer);
                }
            }
        }
    },);

    useEffect(() => {
        let check_timer
        if (!isLoading) {

            check_timer = setInterval(async () => {

                if ((activationDates_CC_Stack.includes(moment().utc()) || moment().isSameOrAfter(activationDates_CC_Stack[(activationDates_CC_Stack.length - 1) - 1]))
                    && activationDates_CC_Stack.length > 1) {
                    nextCC_Index_accordingTO_ActivateDate();
                }

            }, 1000);
            return function () {
                clearInterval(check_timer);
            }
        }
    },);


    useEffect(() => {
        list_rss_title_series()

    }, [content_series])

    useEffect(() => {
        let refresh_timer
        if (!isLoading) {
            get_All_RssTitle(CC_Stack) //Load the content of the RSS first, or the compoenet will only show something after the refreshing 
            postCurCC();
            refresh_timer = setInterval(async () => {
                await get_All_RssTitle(CC_Stack) //Refreshing the RSS content after an hour 
                postCurCC();
            }, renew_RSS_time * 60 * 1000);
            return function () {
                clearInterval(refresh_timer);
            }
        }
    }, [isLoading]);


    useEffect(() => {
        // Function to handle the event
        // Listen for the event

        //Activate the eventListeners 
        CC_emitter.on('Reload', HandleReload);
        CC_emitter.on('MediaReport',HandleMediaReport);
        CC_emitter.on('NewDisplayRSS', HandleNewDisplay);
        CC_emitter.on('Modify', HandleModify);
        CC_emitter.on('ForcePopping', HandleForcePopping_CC)

        // Clean up the event listener when the component unmounts
        return () => {
            CC_emitter.off('Reload', HandleReload);
            CC_emitter.off('MediaReport',HandleMediaReport);

            CC_emitter.off('NewDisplayRSS', HandleNewDisplay);
            CC_emitter.off('Modify', HandleModify);
            CC_emitter.off('ForcePopping', HandleForcePopping_CC)


        };

    }, []);


    function list_rss_title_series() {

        if (content_series != null) {

            let string_components = content_series.split(space) //Split the string by their long space in between each of them in to an array
            return (string_components.map(element => { //Map the content of array into separated <Text> tags 
                return (<Text key={element}>{element}</Text>)

            }))
        }
    }
    return (
        <View style={[styles.container, showRSS ? { display: 'flex' } : { display: 'none' }]}>

            <MarqueeView

                speed={0.175 * speed_multiplyer} // The speed of the scrolling
                marqueeOnStart // Start scrolling when the component mounts
                // Loop the scrolling indefinitely
                useNativeDriver // Use the native driver for better performance

            >
                <Text style={styles.rss}>{cur_RssList_Content_series}</Text>
            </MarqueeView>

        </View>
    );
}



