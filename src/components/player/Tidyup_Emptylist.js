export async function Tidyup_Emptylist(response, type) {
    let cnt = response.length - 1;
    if (type == "playlist") {
        while (cnt != 0) {

            if (response[cnt].newPlaylistMedia.content.length == 0) {
                response.splice(cnt, 1);
            }
            cnt--;
        }
    }
    else if (type == "CC") {
        while (cnt != 0) {

            if (response[cnt].newCCMedia.content.length == 0) {
                response.splice(cnt, 1);
            }
            cnt--;
        }
    }
    return response;
}
