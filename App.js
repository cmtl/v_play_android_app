import * as Application from 'expo-application';
import Constants from 'expo-constants';
import { StatusBar } from 'expo-status-bar';
import * as Updates from 'expo-updates';
import { useEffect, useRef, useState } from 'react';
import { AppState, Button, Image, LogBox, Text, TextInput, TouchableOpacity, View, useWindowDimensions } from 'react-native';
//import * as Updates from 'expo-updates'
import { useAssets } from 'expo-asset';
import * as KeepAwake from 'expo-keep-awake';
import * as ScreenOrientation from 'expo-screen-orientation';

import { patch_Activation, patch_Login } from './src/API/Fetch_Login.js';
import Date_timer from './src/components/Date_timer.js';
import { WifiReport, get_SignalStrength } from './src/util/stats/Check_Connection.js';
import { DeviceReport } from './src/util/stats/Check_MachineStatus.js';
import { Deleting_Info, Saving_Activated_Info, Saving_Login_Info } from './src/util/stats/info/Info_Bundle.js';
import { deletetFrom_LocalData, getFrom_LocalData, storeAs_LocalData } from './src/util/stats/info/Secured_Machine_Info.js';
//import { getDCIM_dir } from './src/util/helper/get_Directory.js';
import { post_Abnormal_Alert } from './src/background_jobs/alert/Send_Alert.js';
import { Housekeeping_Cache, } from './src/background_jobs/housekeeping/Housekeeping_Cache.js';
import { Housekeeping_Log, } from './src/background_jobs/housekeeping/Housekeeping_Log.js';
import { call_Housekeeping_Manager_onPeriod, call_Housekeeping_Manager_onStartUp } from './src/background_jobs/housekeeping/Housekeeping_manager.js';
import { observe_CacheDirectory, observe_DocumentDirectory } from './src/util/helper/File_observer.js';
import { Log_MACHINE_error, Log_MACHINE_info } from './src/util/helper/Log_shortcut.js';


import { default_routine } from './src/background_jobs/routine/Default_routine.js';

import { horizontal_styles, inverted_style, normal_style, vertical_styles } from './src/style/AppCSS.js';



import { EventEmitter } from 'events';
import * as API_Job from './src/API/Fetch_API.js';
import { ALERT_FileTransport_Emergency } from './src/background_jobs/log/ALERT_FileTransport.js';
import { API_FileTransport_Emergency } from './src/background_jobs/log/API_FileTransport.js';
import { MACHINE_FileTransport_Emergency } from './src/background_jobs/log/MACHINE_FileTransport.js';
import { MEDIA_FileTransport_Emergency } from './src/background_jobs/log/MEDIA_FileTransport.js';
import { NETWORK_FileTransport_Emergency } from './src/background_jobs/log/NETWORK_FileTransport.js';
import { ROUTINE_FileTransport_Emergency } from './src/background_jobs/log/ROUTINE_FileTransport.js';
import { RSS_FileTransport_Emergency } from './src/background_jobs/log/RSS_FileTransport.js';
import { Routine_PMinfo } from './src/background_jobs/routine/Routine_PMinfo.js';
import { Routine_checkNonRoutine } from './src/background_jobs/routine/Routine_checkNonRoutine.js';
import { Routine_checkRoutine } from './src/background_jobs/routine/Routine_checkRoutine.js';
import { Routine_heartbeat } from './src/background_jobs/routine/Routine_heartbeat.js';
import { Routine_mediaUpdate } from './src/background_jobs/routine/Routine_mediaUpdate.js';
import Media_player_new, { triggerMedia_Reload_new, triggerViewCurMedia_new } from './src/components/player/Media_player_new.js';
import RSS_player_new, { triggerCC_Reload_new } from './src/components/player/RSS_player_new.js';
import { Delete_CacheDirectory, Delete_FileDirectory } from './src/util/helper/Delete_all_File.js';
import { deletetFrom_LocalData_AsyncStorage, getFrom_LocalData_AsyncStorage, storeAs_LocalData_AsyncStorage } from './src/util/stats/info/Async_Machine_Info.js';
//import * as ScreenOrientation from 'expo-screen-orientation'

/**
 * Author: Ken Lung, Time:  12/03/2024
 * Ignoring warning and errors sent by Expo
 * which are unnecessary to production
 */

LogBox.ignoreAllLogs()

const App_emitter = new EventEmitter();
let resetState;
export function trigger_updateRoutine() {
  App_emitter.emit("UpdateRoutine", true)
}

export default function App() {
  // const screen_height = calcHeight();
  // const screen_width = calcWidth();

  /**
   * Author: Ken Lung, Time:  12/03/2024
   * useState variables for operation:
   * key_val: content in login's input bar (str)
   * activated: check if the device is confirmed to be activated from the backend (bool)
   * usingToken: check if the device received a usable token since login from the backend (bool)
   * 
   * inverted: a boolean to show whether the screen should be inverted 
   * showAdmin: a boolean to shows whether the component has properly loaded Media_CC
   * 
   * height, width: two integers for measuring the screen height and width 
   * used for detecting whether the device is on portrait or landscape
   * 
   * routine: a preset routine for the app to run certain API calls (JSON)
   * routineRef: a reference variable to routine
   * so that the app will use the latest version of routine 
   * 
   * appState: check the current appstate of the app (str)
   * can be active, inactive, or background 
   */

  const [default_logo] = useAssets(require('./assets/LiWanJi.jpg'));

  const [key_val, setKey_val] = useState('');
  const [activated, setActivated] = useState(false);
  const [usingToken, setUsingToken] = useState(false);
  const [inverted, setInverted] = useState(false);

  const [showAdmin, setShowAdmin] = useState(false)

  const { height, width, scale, fontScale } = useWindowDimensions();
  const [routine, setRoutine] = useState([]);
  const routineRef = useRef(routine);

  const [appState, setAppState] = useState(AppState.currentState);

  const rid1_intervalRef = useRef()
  const rid2_intervalRef = useRef()
  const rid3_intervalRef = useRef()
  const rid4_intervalRef = useRef()
  const rid5_intervalRef = useRef()

  /**
   *  
   const [rid1,setRid1] = useState()
   const [rid2, setRid2] = useState()
   const [rid3, setRid3] = useState()
   const [rid4, setRid4] = useState()
   const [rid5, setRid5] = useState()
   */

  /**
   * Author: Ken Lung, Time: 12/03/2024
   * useEffect for keeping the app awake and not falling into sleep mode
   * return a clean-up function to deactivate this function once the app terminates 
   */
  resetState = () => {

    clearInterval(rid1_intervalRef);
    clearInterval(rid2_intervalRef);
    clearInterval(rid3_intervalRef);
    clearInterval(rid4_intervalRef);
    clearInterval(rid5_intervalRef);

   
    triggerMedia_Reload_new()
    triggerCC_Reload_new()
    
    routineJob1()
    routineJob2()
    routineJob4()
    routineJob5()





    };





  useEffect(() => {
    KeepAwake.activateKeepAwakeAsync()

    return () => {
      KeepAwake.deactivateKeepAwake()
    }
  }, [])

  /**
   * Author: Ken Lung, Time: 12/03/2024
   * useEffect for checking app state
   * if the app enters a different app state such as inactive or background
   * send an alert to the backend and write the remaining buffered log strings to the log files
   * return a clean-up function to detach the listener when the app terminates 
   */

  useEffect(() => {
    async function handleAppStateChange(nextAppState) { //background here
      if (appState !== nextAppState) {
        // AppState has changed
        Log_MACHINE_error(`The app has entered ${nextAppState} unexpectedly!`)
        await post_Abnormal_Alert(`Entering ${nextAppState} unexpectedly during normal operation!`, "APP_STATE")
        if (nextAppState != 'active') {

          await API_FileTransport_Emergency()
          //await APP_FileTransport_Emergency()
          await ALERT_FileTransport_Emergency()
          await MEDIA_FileTransport_Emergency()
          await RSS_FileTransport_Emergency()
          await NETWORK_FileTransport_Emergency()
          await MACHINE_FileTransport_Emergency()
          await ROUTINE_FileTransport_Emergency()
        }


      }
      setAppState(nextAppState);
    };

    let subscripter = AppState.addEventListener('change', handleAppStateChange);
    return () => {
      subscripter.remove()
    };
  }, []);

  useEffect(() => {

    //Activate the eventListeners 
    App_emitter.on('UpdateRoutine', HandleUpdateRoutine)

    // Clean up the event listener when the component unmounts
    return () => {

      App_emitter.off('UpdateRoutine', HandleUpdateRoutine)

    };
  }, []);

  useEffect(() => {

    async function fetchInverted() {
      try {
        const res = await getFrom_LocalData("inverted");
        if (res === null) {
          await storeAs_LocalData("inverted", false);
          setInverted(false);
        } else {
          setInverted(res);
        }
      } catch (error) {
        // Handle exceptions (e.g., log the error)
        Log_MACHINE_error(`Error fetching inverted:" ${error}`)
      }

    }



    async function fetchActivated() {
      try {
        const res = await getFrom_LocalData("activated");
        if (res === null) {
          await storeAs_LocalData("activated", false);
          setActivated(false);
        } else {
          setActivated(res);
        }
      } catch (error) {
        // Handle exceptions (e.g., log the error)
        Log_MACHINE_error(`Error fetching activated:" ${error}`)
      }
    }


    async function fetchUsingToken() {
      try {
        const res = await getFrom_LocalData("usingToken");
        if (res === null) {
          await storeAs_LocalData("usingToken", false);
          setUsingToken(false);
        } else {
          setUsingToken(res);
        }
      } catch (error) {
        // Handle exceptions (e.g., log the error)
        Log_MACHINE_error(`Error fetching usingToken:" ${error}`)
      }
    }

    async function fetchRoutine() {
      try {
        const res = await getFrom_LocalData_AsyncStorage("Routine");
        if (res === null) {
          await storeAs_LocalData_AsyncStorage("Routine", default_routine);
          setRoutine(default_routine)
          routineRef.current = default_routine;
        } else {
          setRoutine(res);
          routineRef.current = res;

        }
      } catch (error) {
        // Handle exceptions (e.g., log the error)
        Log_MACHINE_error(`Error fetching Routine:" ${error}`)
      }
    }

    fetchActivated()
    fetchInverted()
    fetchUsingToken()
    fetchRoutine()

  }, []);


  useEffect(function housekeepingJobs() {
    call_Housekeeping_Manager_onStartUp()
    let periodic_houseKeeping = setInterval(call_Housekeeping_Manager_onPeriod, 60 * 60 * 1000)
    return (() => {
      clearInterval(periodic_houseKeeping)
    })
  }, [])


  useEffect(() => {
    routineRef.current = routine;

  }, []);


  async function routineJob1() {
    await Routine_heartbeat(routineRef.current[0])
  }
  async function routineJob2() {
    await Routine_PMinfo(routineRef.current[1])
  }
  async function routineJob3() {
    await Routine_checkRoutine(routineRef.current[2])

  }
  async function routineJob4() {
    await Routine_mediaUpdate(routineRef.current[3])
  }
  async function routineJob5() {
    await Routine_checkNonRoutine(routineRef.current[4])
  }


  function setup_Routine(new_routine) {


    clearInterval(rid1_intervalRef.current)
    clearInterval(rid2_intervalRef.current)
    clearInterval(rid3_intervalRef.current)
    clearInterval(rid4_intervalRef.current)
    clearInterval(rid5_intervalRef.current)

    rid1_intervalRef.current = setInterval(() => {
      // Your periodic job logic here
      routineJob1()
    }, new_routine[0].interval * 60 * 1000)
    rid2_intervalRef.current = setInterval(() => {
      // Your periodic job logic here
      routineJob2()
    }, new_routine[1].interval * 60 * 1000 - 2000)
    rid3_intervalRef.current = setInterval(() => {
      // Your periodic job logic here
      routineJob3()
    }, new_routine[2].interval * 60 * 1000 - 2000)
    rid4_intervalRef.current = setInterval(() => {
      // Your periodic job logic here
      routineJob4()
    }, new_routine[3].interval * 60 * 1000 - 2000)
    rid5_intervalRef.current = setInterval(() => {
      // Your periodic job logic here
      routineJob5()
    }, new_routine[4].interval * 60 * 1000 - 2000)
  }

  useEffect(() => {
    if (routineRef && usingToken) {

      routineJob1()
      routineJob2()
      routineJob3()
      routineJob4()
      routineJob5()

      rid1_intervalRef.current = setInterval(() => {
        // Your periodic job logic here
        routineJob1()
      }, routineRef.current[0].interval * 60 * 1000 - 2000)
      rid2_intervalRef.current = setInterval(() => {
        // Your periodic job logic here
        routineJob2()
      }, routineRef.current[1].interval * 60 * 1000 - 2000)
      rid3_intervalRef.current = setInterval(() => {
        // Your periodic job logic here
        routineJob3()
      }, routineRef.current[2].interval * 60 * 1000 - 2000)
      rid4_intervalRef.current = setInterval(() => {
        // Your periodic job logic here
        routineJob4()
      }, routineRef.current[3].interval * 60 * 1000 - 2000)
      rid5_intervalRef.current = setInterval(() => {
        // Your periodic job logic here
        routineJob5()
      }, routineRef.current[4].interval * 60 * 1000 - 2000)


      /**
       * setRid1(setInterval(routineJob1, routineRef.current[0].interval * 60 * 1000));
      setRid2(setInterval(routineJob2, routineRef.current[1].interval * 60 * 1000));
      setRid3(setInterval(routineJob3, routineRef.current[2].interval * 60 * 1000));
      setRid4(setInterval(routineJob4, routineRef.current[3].interval * 60 * 1000));
      setRid5(setInterval(routineJob5, routineRef.current[4].interval * 60 * 1000));
      */
    }

    return () => {
      /**
       * clearInterval(rid1);
      clearInterval(rid2);
      clearInterval(rid3);
      clearInterval(rid4);
      clearInterval(rid5);
       */
      clearInterval(rid1_intervalRef);
      clearInterval(rid2_intervalRef);
      clearInterval(rid3_intervalRef);
      clearInterval(rid4_intervalRef);
      clearInterval(rid5_intervalRef);
    };

  }, [usingToken])


  async function HandleUpdateRoutine() {
    let new_routine = await getFrom_LocalData_AsyncStorage("Routine")
    setRoutine(new_routine)
    setup_Routine(new_routine)
  }

  async function pop_AdminUI_functions(text) {
    switch (text) {

      case "print":
        test_all_del()
        break;
      case 'Rot':
        setInverted(inverted => inverted = !inverted);
        storeAs_LocalData("inverted", !inverted);
        break;
      case 'Land':
        ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.LANDSCAPE)
        break;
      case 'Port':
        ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.PORTRAIT)
        break;
      case '881930':
        backTo_Login()
        Deleting_Info();
        await Delete_FileDirectory()
        await Delete_CacheDirectory()
        await observe_DocumentDirectory()
        await observe_CacheDirectory()
        break;
      case 'Rou':
        await API_Job.get_Routine();
        break;
      case 'Getp':
        await API_Job.get_MediaUpdate('playlist')
        break;
      case 'Getc':
        await API_Job.get_MediaUpdate('CC')
        break;
      case 'S':
        WifiReport();
        break;
      case 'C':
        DeviceReport();
        break;
      case 'Delm':
        await deletetFrom_LocalData_AsyncStorage('playlist_media_ArrList')
        await deletetFrom_LocalData_AsyncStorage('CC_media_ArrList')
        triggerCC_Reload_new()
        triggerMedia_Reload_new()
        //Updates.reloadAsync()
        break;
      case 'Delc':
        Housekeeping_Cache()
        break;
      case 'Dell':
        Housekeeping_Log()
        break;
      case 'Viewdoc':
        observe_DocumentDirectory()
        break;
      case 'Viewcache':
        observe_CacheDirectory()
        break;
      case 'Delnr':
        deletetFrom_LocalData("NonRoutine")
        break;
      case 'Delr':
        deletetFrom_LocalData('Routine')
        break;
      case 'Login':
        setActivated(false)
        break;
      case 'Forcea1':
        await Saving_Activated_Info("1", "1", "e6bf29d2a5d94cd1", 'a55fc4a7f1058a1a')
        setActivated(activated => activated = true)
        break;
      case "Forcel1":
        const accessToken = await getFrom_LocalData_LessLogging("accessToken");
        await Saving_Login_Info("1", "1", accessToken)
        setUsingToken(usingToken => usingToken = true)
        break;
      case "Viewmedia":
        triggerViewCurMedia_new()
        break;
      case 'Configp':
        await API_Job.get_MediaConfig("playlist")
        break
      case "Configc":
        await API_Job.get_MediaConfig("CC");
        break;
      case "Configd":
        await API_Job.get_Display_Config();
        break;

      case "Man":
        let activationKey = await getFrom_LocalData("activationKey");
        let androidID = await getFrom_LocalData("androidID");
        let signal_strength = await get_SignalStrength();
        let app_ver = Constants.expoConfig.version;
        await API_Job.patch_Login_for_Heartbeat(activationKey, androidID, app_ver, signal_strength)
        break;
      case "Poll":
        await deletetFrom_LocalData("Last_Reported_playlist_media")
        break;
      
    }
  }

  function backTo_Login() {
    deletetFrom_LocalData("activated")
    deletetFrom_LocalData("usingToken")
    setActivated(activated => activated = null)
    setUsingToken(usingToken => usingToken = null)
    Updates.reloadAsync()
  }

  function toggle_Admin() {
    setShowAdmin(!showAdmin);
    if (!showAdmin == true) {
      Log_MACHINE_info(`Frontline admin bar can be seen seen now!`)
    }
    else if (!showAdmin == false) {
      Log_MACHINE_info(`Frontline admin bar cannot be seen now!`)
    }
  }

  /**
  Optional botton for extra function
   * if (!showAdmin == true ) {
    return (<View style={height < width ? [horizontal_styles.container, inverted ? inverted_style.container : normal_style.container]
      : [vertical_styles.container, inverted ? inverted_style.container : normal_style.container]}>
      <StatusBar style="light" hidden={true} />
      <TouchableOpacity style={height < width ? horizontal_styles.invisibleButton : vertical_styles.invisibleButton} onPress={() => { toggle_Admin() }}>
      </TouchableOpacity>
      <View style={height < width ? horizontal_styles.input_container : vertical_styles.input_container}>
      <View style={height < width ? horizontal_styles.buttons : vertical_styles.buttons}>
          <Button title="Get New Media Update" style={height < width ? horizontal_styles.button : vertical_styles.button}
            onPress={async function () {
              await API_Job.get_MediaUpdate("playlist")
            }} />
            <Button title="Get New RSS Update" style={height < width ? horizontal_styles.button : vertical_styles.button}
            onPress={async function () {
              await API_Job.get_MediaUpdate("CC")
            }} />
            <Button title="Get New Media Config" style={height < width ? horizontal_styles.button : vertical_styles.button}
            onPress={async function () {
              API_Job.get_MediaConfig("playlist")
  
            }} />
            <Button title="Get New RSS Config" style={height < width ? horizontal_styles.button : vertical_styles.button}
            onPress={async function () {
              API_Job.get_MediaConfig("CC")
  
            }} />
        
        </View>
      </View>
      </View>
      )
  }
   */
  if (activated !== true || usingToken !== true) {
    return (<View style={height < width ? [horizontal_styles.container, inverted ? inverted_style.container : normal_style.container]
      : [vertical_styles.container, inverted ? inverted_style.container : normal_style.container]}>
      <StatusBar style="light" hidden={true} />
      <TouchableOpacity style={height < width ? horizontal_styles.invisibleButton : vertical_styles.invisibleButton} onPress={() => { toggle_Admin(); }}>
      </TouchableOpacity>
      <Image source={default_logo}
        style={height < width ? horizontal_styles.logo : vertical_styles.logo} />
      <View style={height < width ? horizontal_styles.input_container : vertical_styles.input_container}>
        <View style={height < width ? horizontal_styles.input_element : vertical_styles.input_element}>
          <Text>
            Activation Key:
          </Text>
          <TextInput style={height < width ? horizontal_styles.input : vertical_styles.input}
            value={key_val}
            onChangeText={(value) => setKey_val(value)} />
        </View>

        <View style={height < width ? horizontal_styles.buttons : vertical_styles.buttons}>
        
          <Button title="ACTIVATE" style={height < width ? horizontal_styles.button : vertical_styles.button}
            onPress={async function () {
              let activationKey = key_val;
              let androidID = Application.getAndroidId()

              let activation_res = await patch_Activation(activationKey, androidID)
              //console.log(activation_res)
              if (activation_res == true) {
                setActivated(activated => activated = true)
                let signal_strength = await get_SignalStrength();
                let app_ver = Constants.expoConfig.version;
                let login_res = await patch_Login(activationKey, androidID, app_ver, signal_strength)
                //console.log(login_res)
                if (login_res == true) {
                  setActivated(activated => activated = true)
                  setUsingToken(usingToken => usingToken = true)
                }

              }

              setKey_val('')
            }} />
        </View>        
        <View style={{ height:5,width: 5 }} />
        <Button title="DELETE" style={height < width ? horizontal_styles.button : vertical_styles.button} onPress={async function () {
          Deleting_Info()
          //choosing_disabledState(true);
          backTo_Login();
          setKey_val('')
        }} />

      </View>


    </View >)
  }
  else if (activated == true && usingToken == true) {

    return (

      <View style={height < width ? [horizontal_styles.container, inverted ? inverted_style.container : normal_style.container]
        : [vertical_styles.container, inverted ? inverted_style.container : normal_style.container]} >

        <StatusBar style="light" hidden={true} />
        <TouchableOpacity style={height < width ? horizontal_styles.invisibleButton : vertical_styles.invisibleButton} onPress={() => { toggle_Admin() }}>
        </TouchableOpacity>


        <View style={height < width ? [horizontal_styles.header, inverted ? inverted_style.header : normal_style.header]
          : [vertical_styles.header, inverted ? inverted_style.header : normal_style.header]}>

          <Date_timer />
          {/*
            <Text style={height < width ? {display:'flex'} : {display:'none'}}> hori</Text>
            <Text style={height < width ? {display:'none'}: {display:'flex'} }> vert</Text>
            */}

        </View>

        < View style={height < width ? horizontal_styles.media_container : vertical_styles.media_container} >


          <Media_player_new />
          {/*<Media_player_new />*/}
          {/*<Media_player_useConfig/>*/}
          <RSS_player_new />
          {/**<View style={[height < width ? horizontal_styles.rss : vertical_styles.rss,]}>
          <RSS_player_new />
          </View> */}


        </View >

        <View style={[height < width ? horizontal_styles.footer : vertical_styles.footer, showAdmin ? { display: 'flex' } : { display: 'none' }]}>

          <TextInput onChangeText={function (text) {
            pop_AdminUI_functions(text);
          }}
            returnKeyType='next'
            placeholder="-------------------------------------------------------------------------------------------------"
          />
          {/**
           *  <Text>ViPlay - Provided by Clever Motion Technology Limited</Text>
          <Text>All Right Reserved</Text>
           */}

        </View>

      </View>
    )
  }
};


export { resetState };

