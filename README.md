# V_play_Android_app

## Name
V-Play - Private Video Channel Platform

## Description
V-Play is a media playing advertisement application under Expo managed workflow, designed to refer to the changes made by the user on the playlist(s) playing on the device automatically. User can choose what kind of media file to be played and the relevant media playing setting on an Android device remotely.

The application also supports RSS reel feature, where users can import RSS links for the program to read the content and play it in a manner that is similar to Marquee text. The links can be pre-existing RSS link or self-made RSS link, as long as the content needed to be shown is within the "title" tag in the RSS link.

# Compiling the Program
- Download the source code as a ZIP file from GitLab 
- Decompose it and extract the content
- Run "npm install" in the console to install the required libraries
- "npm install -g eas-cli" and "eas login" if you are using it for the first time
- Run "eas build" in the console to start building the project
- Choose “Android”
- If the console asks to make a new keystore, choose “Yes”
- Wait for the APK file to be compiled and ready to be downloaded from the Expo Cloud Server by clicking the URL to the build website

# Installing the Program
- Compile the APK file 
- Download the APK file from the computer 
- Export the APK file to the device 
- Install the application from the APK file
- If the device prompts you not to install, ignore the warning 

## Support
Priority of ways to request support is listed from top to bottom:
- Face-to-face inquiries: Ask Mr. Alan Kan or Raymond Kan for getting development history, requirements, development direction, and backend's operation
- Whatspp: Ask questions in the company's Whatsapp group
- Discord: Post the questions to the v-play chatroom under the TECH header in the discord server owned by CMT (named CMT server)
- Email to the developer (Ken Lung): Send an email to kjrgunpla@gmail.com when the options above cannot help you

## Contributing
# Testing on an Android Device with Google Play
- Install Expo GO from Google Play/ App Store
- Run "npm start" in the console to start up Metro, the app's bundler
- Choose “Using Expo Go”, and press “S” if the console is not showing this option
- Connect to the same network that the computer is connected to
- Scan the QR code in the console with the camera in your mobile device or scanner in Expo Go

# Testing on an Android Device without Google Play
- Compile the APK file 
- Download the APK file from the computer 
- Export the APK file to the device 
- Install the application from the APK file
- If the device prompts you not to install, ignore the warning 
- Run the app

## Authors and acknowledgment
This project is primarily made by Ken Lung, entered the company from 03/06/2024 to 28/06/2024. 
This project would not be possible without Jun Li's contribution as she closely cooperate with the front end with her back end support. 
Valuable opinions have been gathered throughout the development from Mr. Raymond Kan and Mr. Alan Kan by pointing out new requirements and adjusting the development direction to fit future customers' needs.

# To-Do (High Priority):

- 1st: Handle onceOnly playlist logic better (returning to original playing position for video after it ends, not just the original playing index)

# To-Do (Low Priority):

- 1st: Cooperate proper navigation to different pages within the app (Current: admin functions are done with a switch statement and are represented by a text-input bar, login page is a part of App.js which is too clumsy)

- 2nd: Further improvement on the documentation
